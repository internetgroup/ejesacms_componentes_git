/*global document: false */
/*global window: false */
/*global DOMParser: false */
/*global ActiveXObject: false */
/*global console*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.BRASILEIRAO2013.Jogos'); /** Jogos do campeonato **/


/** implementando módulo **/
/** Jogos do campeonato **/
EJESA.BRASILEIRAO2013.Jogos = (function () {
	'use strict';
	var start,
		xml = 'http://localhost/javascript/brasileirao/listajogosdecampeonato.xml',
		renderiza,
		games = [],
		mountGame,
		mountHTML,
		filterRJ,
		gamesRJ = [],
		parseGame,
		getScore,
		parseString;



	mountGame = function (game) {
		var obj;
		//console.log(game.getElementsByTagName('partidas')[0].childNodes[0]);
		obj = {
			'data' : game.getElementsByTagName('data')[0].childNodes[0],
			'id' : game.getElementsByTagName('id')[0].childNodes[0],
			'rodada' : game.getElementsByTagName('rodada')[0].childNodes[0],
			'partida' : game.getElementsByTagName('partidas')[0].childNodes[0],
			'partidaId' : game.getElementsByTagName('partidasId')[0].childNodes[0],
			'situacao' : game.getElementsByTagName('situacao')[0].childNodes[0]
		};
		games.push(obj);

	};

	filterRJ = function () {
		var i,
			jogo,
			times;

		for (i = 0; i < games.length; i += 1) {
			jogo = games[i];
			times = jogo.partida.nodeValue;
			if (times.match(/Flamengo/)
					|| times.match(/Vasco/)
					|| times.match(/Botafogo/)
					|| times.match(/Fluminense/)
					) {
				gamesRJ.push(jogo);
			}
		}
		gamesRJ = EJESA.BRASILEIRAO2013.Util.arraySort(gamesRJ);

		//console.log(gamesRJ);
	};

	parseGame = function (partida, situacao) {
		var times = [],
			score = [],
			gols,
			parts,
			i,
			aux;
		parts = partida.split('X');
		for (i = 0; i < parts.length; i += 1) {
			aux = EJESA.BRASILEIRAO2013.Util.trim(parts[i]);
			if (situacao === "Concluída") {
				gols = getScore(aux, i);
				if (i === 0) {
					aux = aux.substring(0, aux.indexOf(gols));
				} else {
					aux = aux.substring(2);
				}
				aux = EJESA.BRASILEIRAO2013.Util.trim(aux);
				aux = parseString(aux);
				aux = aux.replace(' ', '');
				times.push(aux.toLowerCase());
				times.push(gols);
			} else {
				aux = EJESA.BRASILEIRAO2013.Util.trim(aux);
				aux = parseString(aux);
				aux = aux.replace(' ', '');
				times.push(aux.toLowerCase());
				times.push('0');
			}
		}
		return times;
	};

	getScore = function (time, pos) {
		var score,
			parts,
			i;
		parts = time.split(' ');
		if (pos === 0) {
			score = parts[parts.length - 1];
		} else {
			score = parts[0];
		}
		return score;
	};

	renderiza = function (data) {

		var html = '',
			parser,
			xmlDoc,
			futeb,
			jogos,
			jogo,
			i,
			campeonato;

		if (window.DOMParser) {
			parser = new DOMParser();
			xmlDoc = parser.parseFromString(data, "text/xml");
		} else {
			xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async = false;
			xmlDoc.loadXML(data);
		}

		futeb = xmlDoc.getElementsByTagName('FUTEB');
		jogos = futeb[0].getElementsByTagName('lista');
		//console.log(jogos);
		for (i = 0; i < jogos.length; i = i + 1) {
			jogo = jogos[i];
			//console.log(jogo);
			mountGame(jogo);
		}
		filterRJ();
		//console.log(games);
		html = mountHTML();
	};

	start = function () {
		//pegar o xml
		var i, xhr;

		xhr = EJESA.BRASILEIRAO2013.Util.getXHR();

	    xhr.onreadystatechange = function () {
	        if (xhr.readyState !== 4) {
	            return false;
	        }
	        if (xhr.status !== 200) {
	            console.log("Error, status code: " + xhr.status);
	            return false;
	        }
	        renderiza(xhr.responseText);
	    };

	    xhr.open("GET", xml, true);
	    xhr.send("");
	};


	mountHTML = function () {

		var i,
			html = '',
			jogo,
			lista,
			partida,
			rodada,
			situacao,
			data,
			teams,
			atual;


		lista = document.getElementById("lista-de-rodadas");

		for (i = 0; i < gamesRJ.length; i += 1) {
			jogo = gamesRJ[i];
			partida = jogo.partida.nodeValue;
			rodada = jogo.rodada.nodeValue;
			//console.log(rodada);
			situacao = jogo.situacao.nodeValue;
			data = jogo.data.nodeValue;
			teams = parseGame(partida, situacao);

			if (i === 0) {
				html += "<li>";
				atual = rodada;
			}

			/**
			console.log("===");
			console.log(rodada);
			console.log(atual);
			console.log("===");
			**/

			if (atual !== rodada) {
				html += "</li>";
				html += "<li>";
				atual = rodada;
			}

			html += "<div class='jogo'>";
			html += "<div class='escudo'>";
			html += "<img src='imgs/" + teams[0] +  "_41h.png' alt='" + teams[0] + "' title='" + teams[0] + "'/>";
			html += "</div>";
			html += "<div class='time'>";
			html +=  teams[0].substring(0, 3).toUpperCase();
			html += "</div>";
			html += "<div class='data-local'>";
			html +=  data;
			html += "<span>" + teams[1] + "<span class='x'>x</span>" + teams[3] + "</span>";
			html +=  rodada;
			html += "</div>";
			html += "<div class='time'>";
			html +=  teams[2].substring(0, 3).toUpperCase();
			html += "</div>";
			html += "<div class='escudo'>";
			html += "<img src='imgs/" + teams[2] +  "_41h.png' alt='" + teams[2] +  "' title='" + teams[2] +  "' />";
			html += "</div>";
			html += "<div class='clear'></div>";
			html += "</div>";

			if ((i + 1) === gamesRJ.length) {
				html += "</li>";
			}

		}

		document.getElementById('lista-de-rodadas').innerHTML = html;


		return html;

	};

	parseString = function (string) {
		var caracter,
			caracteresAcento = {
	            "Á" : "A",
	            "á" : "a",
	            "É" : "E",
	            "é" : "e",
	            "Í" : "I",
	            "í" : "i",
	            "Ó" : "O",
	            "ó" : "o",
	            "Ú" : "U",
	            "ú" : "u",
	            "À" : "A",
	            "à" : "a",
	            "È" : "E",
	            "è" : "e",
	            "Ì" : "I",
	            "ì" : "i",
	            "Ò" : "O",
	            "ò" : "o",
	            "Ù" : "U",
	            "ù" : "u",
	            "Â" : "A",
	            "â" : "a",
	            "Ê" : "E",
	            "ê" : "e",
	            "Î" : "I",
	            "î" : "i",
	            "Ô" : "O",
	            "ô" : "o",
	            "Û" : "U",
	            "û" : "u",
	            "Ä" : "A",
	            "ä" : "a",
	            "Ë" : "E",
	            "ë" : "e",
	            "Ï" : "I",
	            "ï" : "i",
	            "Ö" : "O",
	            "ö" : "o",
	            "Ü" : "U",
	            "ü" : "u",
	            "Ã" : "A",
	            "ã" : "a",
	            "Õ" : "O",
	            "õ" : "o",
	            "Ç" : "C",
	            "ç" : "c"
			};

		for (caracter in caracteresAcento) {
			string = string.replace(caracter, caracteresAcento[caracter]);
		}

		return string;
	};




	//revelando API pública
	return {
		start: start
	};

}());