/*global document: false */
/*global console*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.EJELIA00106'); /** Lista de últimas do Canal **/

/** implementando módulo **/
/** Lista de últimas do Canal **/
EJESA.EJELIA00106 = (function () {
	'use strict';
	var QTDNEWS = 6,
		SITE = 'odia',
		SOLR = 'http://odia.ig.com.br/_indice/noticias/select?',
		FORMAT = 'json',
		HEADLINES = 'EJELIA00106HL',
		SECTIONFORM = 'EJELIA00106FORM',
		NOTICIA_ID = '512788b109f9c44c46000020',
		DIVERSAO_ID = '51278b0656efd04740000051',
		ESPORTE_ID = '51278b7e09f9c44c46000029',
		FLAMENGO_ID = '51278b9509f9c44c4600002b',
		BOTAFOGO_ID = '51278c0a56efd04740000059',
		FLUMINENSE_ID = '51278bdb09f9c44c4600002d',
		VASCO_ID = '51278bb956efd04740000057',
		MMA_ID = '54e729c62a4aa32776000fcb',
		CARIOCA_ID = '52d84c50923c1e6e3b001483',
		OLIMPIADA_ID = '563904d82f2df0ce4400265c',
		CARNAVAL_ID = '51278b2709f9c44c46000027',
		CELEBRIDADES_ID = '51278b4256efd04740000053',
		TELEVISAO_ID = '51278b5f56efd04740000055',
		RIO_ID = '5127897256efd04740000043',
		ECONOMIA_ID = '512789c456efd04740000046',
		BRASIL_ID = '512789e656efd04740000048',
		MUNDO_ID = '51278a0609f9c44c46000023',
		EDUCACAO_ID = '51278a7f56efd0474000004a',
		OPINIAO_ID = '51278a9409f9c44c46000025',
		AUTOMANIA_ID = '51278abd56efd0474000004c',
		IMOVEIS_ID = '51278aec56efd0474000004f',
		getIDS,
		sectionIdentify,
		pageIdentify,
		page = 1,
		makeAddress,
		displaySearch,
		start,
		renderiza,
		executeSearch,
		displayForm,
		validateChecked,
		sumChecked,
		removeChecked,
		contentFilter,
		nextPage,
		prevPage,
		resetPagination,
		pageMarked;

	getIDS = function (section) {
		var sections = '';

		if (section === 'noticia') {
			sections += NOTICIA_ID;
			sections += '%20';
			sections += RIO_ID;
			sections += '%20';
			sections += ECONOMIA_ID;
			sections += '%20';
			sections += BRASIL_ID;
			sections += '%20';
			sections += MUNDO_ID;
			sections += '%20';
			sections += EDUCACAO_ID;
			sections += '%20';
			sections += OPINIAO_ID;
			sections += '%20';
			sections += AUTOMANIA_ID;
			sections += '%20';
			sections += IMOVEIS_ID;
		}

		if (section === 'diversao') {
			sections += DIVERSAO_ID;
			sections += '%20';
			sections += CARNAVAL_ID;
			sections += '%20';
			sections += CELEBRIDADES_ID;
			sections += '%20';
			sections += TELEVISAO_ID;
		}

		if (section === 'esporte') {
			sections += ESPORTE_ID;
			sections += '%20';
			sections += FLAMENGO_ID;
			sections += '%20';
			sections += FLUMINENSE_ID;
			sections += '%20';
			sections += BOTAFOGO_ID;
			sections += '%20';
			sections += VASCO_ID;
			sections += '%20';
			sections += MMA_ID;
			sections += '%20';
			sections += CARIOCA_ID;
			sections += '%20';
			sections += OLIMPIADA_ID;

		}

		if (section === 'flamengo') {
			sections += FLAMENGO_ID;
		}

		if (section === 'fluminense') {
			sections += FLUMINENSE_ID;
		}

		if (section === 'botafogo') {
			sections += BOTAFOGO_ID;
		}

		if (section === 'vasco') {
			sections += VASCO_ID;
		}

		if (section === 'carnaval') {
			sections += CARNAVAL_ID;
		}

		if (section === 'celebridades') {
			sections += CELEBRIDADES_ID;
		}

		if (section === 'televisao') {
			sections += TELEVISAO_ID;
		}

		if (section === 'rio') {
			sections += RIO_ID;
		}

		if (section === 'economia') {
			sections += ECONOMIA_ID;
		}

		if (section === 'brasil') {
			sections += BRASIL_ID;
		}

		if (section === 'mundoeciencia') {
			sections += MUNDO_ID;
		}

		if (section === 'educacao') {
			sections += EDUCACAO_ID;
		}

		if (section === 'opiniao') {
			sections += OPINIAO_ID;
		}

		if (section === 'automania') {
			sections += AUTOMANIA_ID;
		}

		if (section === 'imoveis') {
			sections += IMOVEIS_ID;
		}

		if (section === 'carioca') {
			sections += CARIOCA_ID;
		}

		if (section === 'MMA') {
			sections += MMA_ID;
		}

		if (section === 'olimpiada') {
			sections += OLIMPIADA_ID;
		}

		return sections;
	};

	sectionIdentify = function (metaurl) {

		var section = '';

		if (metaurl.match(/\/rio-de-janeiro\//)) {
			section += 'noticia';
		}

		if (metaurl.match(/\/brasil\//)) {
			section += 'noticia';
		}

		if (metaurl.match(/\/economia\//)) {
			section += 'noticia';
		}

		if (metaurl.match(/\/educacao\//)) {
			section += 'noticia';
		}

		if (metaurl.match(/\/automania\//)) {
			section += 'noticia';
		}

		if (metaurl.match(/\/opiniao\//)) {
			section += 'noticia';
		}

		if (metaurl.match(/\/imoveis\//)) {
			section += 'noticia';
		}

		if (metaurl.match(/\/diversao\//)) {
			section += 'diversao';
		}

		if (metaurl.match(/\/esporte\//)) {
			section += 'esporte';
		}

		return section;
	};

	pageIdentify = function () {
		var start = (page - 1) * QTDNEWS;
		return start;
	};

	makeAddress = function (start, section) {

		var address = SOLR;

		address += 'start=';
		address += start;
		address += '&size=';
		address += QTDNEWS;
		address += '&site=';
		address += SITE;
		address += '&secoes_EH=';
		address += section;
		address += '&wt=';
		address += FORMAT;

		return address;
	};

	displaySearch = function (search) {
		document.getElementById(HEADLINES).innerHTML = search;
	};

	//métodos privados
	executeSearch = function (address) {

        var xmlhttp;
        
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState !== 4) {
                return false;
            }
            if (xmlhttp.status !== 200) {
                console.log("Error, status code: " + xmlhttp.status);
                return false;
            }
            renderiza(xmlhttp.responseText);
                
        }
        xmlhttp.open("GET",address,true);
        xmlhttp.send("");

	};

	renderiza = function (data) {
		var html = '',
			obj = JSON.parse(data),
			i,
			y,
			sections,
			mysection,
			tit,
			eye,
			date_news,
			docs;

		docs = obj.response.docs;

		for (y = 1; y <= docs.length; y = y + 1) {

			i = y - 1;
			html += "<li>";
			if (y % 3 === 0) {
				html += "<div class='headline smr'>";
			} else {
				html += "<div class='headline cmr'>";
			}
			html += "<span class='headline-span'></span>";
			html += "<div class='headline-filter'>";

			sections = docs[i].secaoBreadcrumb.split('›');
			mysection = sections[sections.length - 1];
			mysection = EJESA.Util.trim(mysection);

			html += mysection;
			html += "</div>";

			if (docs[i].urlImgEmp_idCorteImagem !== undefined) {

				html += "<div class='headline-img'>";
				html += "<span class='span-border-hover'>";
				html += "<span class='active'></span>";
				html += "<a href='" + docs[i].url + "'>";
				html += "<img src='" + docs[i].urlImgEmp_300x120 + "'>";
				html += "</a>";
				html += "</span>";
				html += "</div>";

			} else {

				html += "<div class='headline-block'></div>";

			}

			tit = '';
			if (docs[i].titulo.length > 60) {
				tit = docs[i].titulo.substring(0, 60) + "...";
			} else {
				tit = docs[i].titulo;
			}


			date_news = EJESA.Util.dateSolr(docs[i].startDate, '$3/$2/$1 - $4:$5');

			html += "<div class='headline-date'>";
			html += date_news;
			html += "</div>";
			html += "<div class='headline-title'>";
			html += "<a href='" + docs[i].url + "'>";
			html += tit;
			html += "</a>";
			html += "</div>";

			if (docs[i].urlImgEmp_idCorteImagem === undefined) {

				eye = '';
				if (docs[i].olho.length > 80) {
					eye = docs[i].olho.substring(0, 80) + "...";
				} else {
					eye = docs[i].olho;
				}

				html += "<div class='headline-eye'>";
				html += "<a href='" + docs[i].url + "'>";
				html += eye;
				html += "</a>";
				html += "</div>";

			}

			html += "</li>";
		}
		displaySearch(html);
	};

	displayForm = function (section) {

		var inputform = "<span>filtrar:</span>";

		if (section === 'noticia') {

			inputform += "<form name='secOptions'>";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='noticia' checked />";
			inputform += " Tudo";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='rio' />";
			inputform += " Rio";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='brasil' />";
			inputform += " Brasil";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='economia' />";
			inputform += " Economia";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='mundoeciencia' />";
			inputform += " Mundo e Ciência";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='educacao' />";
			inputform += " Educação";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='opiniao' />";
			inputform += " Opinião";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='automania' />";
			inputform += " Automania";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='imoveis' />";
			inputform += " Imóveis";
			inputform += "</form>";

		}

		if (section === 'diversao') {

			inputform += "<form name='secOptions'>";
			inputform += "<input type='checkbox' name='dops' class='snews' ";
			inputform += "onClick='javascript:EJESA.EJELIA00106.contentFilter()' ";
			inputform += "value='diversao' checked/>";
			inputform += " Tudo";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='celebridades' />";
			inputform += " Celebridades";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='televisao' />";
			inputform += " Televisão";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='carnaval' />";
			inputform += " O Dia na Folia";
			inputform += "</form>";

		}

		if (section === 'esporte') {

			inputform += "<form name='secOptions'>";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='esporte' checked/>";
			inputform += " Tudo";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='botafogo' />";
			inputform += " Botafogo";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='flamengo' />";
			inputform += " Flamengo";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='fluminense' />";
			inputform += " Fluminense";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='vasco' />";
			inputform += " Vasco";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='MMA' />";
			inputform += " MMA";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='carioca' />";
			inputform += " Carioca";
			inputform += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA00106.contentFilter()' value='olimpiada' />";
			inputform += " Olimpíada";
			inputform += "</form>";

		}

		document.getElementById(SECTIONFORM).innerHTML = inputform;
	};

	removeChecked = function (fields) {
		var i;
		for (i = 0; i < fields.length; i = i + 1) {
			fields[i].checked = false;
		}
	};

	validateChecked = function () {
		var fields = document.secOptions.dops,
			//sections = EJESA.Util.fieldsMarked(fields),
			i,
			sections = [];

		for (i = 0; i < fields.length; i = i + 1) {

			if (fields[i].checked === true) {
				sections.push(fields[i].value);
			}

			if (fields[i].value === 'diversao' ||
					fields[i].value === 'esporte' ||
					fields[i].value === 'diversao') {

				if (fields[i].checked === true) {
					removeChecked(fields);
					fields[i].checked = true;
				}
			}
		}

		return sections;

	};

	sumChecked = function (sections) {
		var param = '',
			i;

		for (i = 0; i < sections.length; i = i + 1) {
			param += getIDS(sections[i]);
			if (i !== (sections.length - 1)) {
				param += '%20';
			}
		}

		return param;
	};


	start = function () {

		var section = sectionIdentify(EJESA.Util.getMetaProperty("og:url")),
			secoes_EH,
			start,
			address,
			search,
			options_checked,
			snews;


		displayForm(section);

		secoes_EH = getIDS(section);

		start = pageIdentify(1);

		address = makeAddress(start, secoes_EH);

		search = executeSearch(address);

	};

	pageMarked = function (page) {
		var mp,
			range,
			pos0 = 0,
			pos1 = 0,
			pos2 = 0,
			pos3 = 0,
			pos4 = 0,
			pos5 = 0,
			posid;

			range = parseInt(page/5);

		if (page > 5) {
			if(page %5 == 1){
				pos1 = page;
				pos2 = page + 1;
				pos3 = page + 2;
				pos4 = page + 3;
				pos5 = page + 4;	

				posid = page % 5;
	
			}
			else{
				if(page %5 != 0){
					pos1 = (range*5) + 1;
					pos2 = (range*5) + 2;
					pos3 = (range*5) + 3;
					pos4 = (range*5) + 4;
					pos5 = (range*5) + 5;	

					posid = page % 5;

				}
				else{
					pos1 = (range*5) - 4;
					pos2 = (range*5) - 3;
					pos3 = (range*5) - 2;
					pos4 = (range*5) - 1;
					pos5 = (range*5) ;	

					posid = 5;

				}
				
			}

			mp = 'hn_pos_' + posid;

		}
		else{
		
			pos1 = 1;
			pos2 = pos1 + 1;
			pos3 = pos1 + 2;
			pos4 = pos1 + 3;
			pos5 = pos1 + 4;

			mp = 'hn_pos_' + page;
		}
		document.getElementById('hn_pos_1').innerHTML = pos1;
		document.getElementById('hn_pos_2').innerHTML = pos2;
		document.getElementById('hn_pos_3').innerHTML = pos3;
		document.getElementById('hn_pos_4').innerHTML = pos4;
		document.getElementById('hn_pos_5').innerHTML = pos5;

		EJESA.Util.removeClassByID(mp, 'headline_nav_off');
		EJESA.Util.addClassByID(mp, 'headline_nav_on', true);


	};

	resetPagination = function () {
		EJESA.Util.removeClassByID('hn_pos_1', 'headline_nav_on');
		EJESA.Util.removeClassByID('hn_pos_2', 'headline_nav_on');
		EJESA.Util.removeClassByID('hn_pos_3', 'headline_nav_on');
		EJESA.Util.removeClassByID('hn_pos_4', 'headline_nav_on');
		EJESA.Util.removeClassByID('hn_pos_5', 'headline_nav_on');

		EJESA.Util.addClassByID('hn_pos_1', 'headline_nav_off');
		EJESA.Util.addClassByID('hn_pos_2', 'headline_nav_off');
		EJESA.Util.addClassByID('hn_pos_3', 'headline_nav_off');
		EJESA.Util.addClassByID('hn_pos_4', 'headline_nav_off');
		EJESA.Util.addClassByID('hn_pos_5', 'headline_nav_off');
	};


	contentFilter = function () {
		//snews = document.form.input.name('.snews');
		var snews = document.secOptions.dops,
			sections,
			secoes_EH,
			start,
			address,
			search;


		sections = validateChecked();
		if (sections.length > 0) {

			secoes_EH = sumChecked(sections);

			start = pageIdentify();

			resetPagination();

			pageMarked(page);

			address = makeAddress(start, secoes_EH);

			search = executeSearch(address);

		}

	};


	nextPage = function () {

		page = page + 1;
		contentFilter();

	};

	prevPage = function () {

		page = page - 1;
		contentFilter();

	};

	window.onload = function () {
		resetPagination();
		contentFilter();
	};

	//revelando API pública
	return {
		start: start,
		contentFilter: contentFilter,
		nextPage: nextPage,
		prevPage: prevPage
	};

}());