/*
    Dependências
    - - - - - - -
    A código da página precisa ter no <head> o meta pubs-web-spectator e no seu content o valor das posições Webspectator previamente cadastradas no admin do Webspectator
    <meta name="pubs-web-spectator" content="lb:10397,mrec1:10398,mrec2:10399,dhtml:10400,intervencao:10401" />
    content > label identificador : ID WS

    # No <head> inserir as chamadas dos scripts, biblioteca WS para o site O Dia e o carregamento inicial do WS, script local
    <script type="text/javascript" src="//wfpscripts.webspectator.com/bootstrap/ws-WS-00DIA.js"></script>
    <script>PUBLICIDADES.Webspectator.posicoes();</script>

    # No container de cada publicidade, inserir o script abaixo
    <script type="text/javascript">
        var ws_unit = {
            "id": PUBLICIDADES.Webspectator.vincula("lb")
        };
        document.write('<scr' + 'ipt type="text/javascript" src="//wfpscripts.webspectator.com/ws-ad.js"></scr' + 'ipt>');
    </script>

    # Observação
    O mais importante do processo é que o vínculo da posição com seu ID é feito manualmente.
    Exemplo: Se no <meta> pubs-web-spectator coloquei lb:10397, então preciso ter uma chamada de script no container com este label
    PUBLICIDADES.Webspectator.vincula("lb")

    Ou seja, o label no cadastro do Webspectator serve para se guiar. No código, temos que coloca-lo de forma resumida, sem espaços ou acentos/caracteres. 
    Outro exemplo: No WS existe um cadastro "Half Page"
        . no <meta> ficará: <meta name="pubs-web-spectator" content="halfpage:xxxxx" />
        . no script do container ficará: [...] PUBLICIDADES.Webspectator.vincula("halfpage") [...]
*/

var PUBLICIDADES = PUBLICIDADES || {};

PUBLICIDADES.namespace = function (ns_string) {
    'use strict';
    var parts = ns_string.split('.'),
        parent = PUBLICIDADES,
        i;

    if (parts[0] === "PUBLICIDADES") {
        parts = parts.slice(1);
    }

    for (i = 0; i < parts.lenght; i += 1) {
        if (parent[parts[i]] === 'undefined') {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }
    return parent;
};

PUBLICIDADES.namespace('PUBLICIDADES.Webspectator');

PUBLICIDADES.Webspectator = (function () {
    ws_pecas = {};

    vincula = function (pub_position) {
        if (ws_pecas[pub_position]) {
            return ws_pecas[pub_position].replace(/\s+/g, '');
        }
        return vincula;
    }

    posicoes = function (){

        var webspectator_pubs = document.getElementsByName("pubs-web-spectator")[0].getAttribute("content") || "",
            webspectator_pubs = webspectator_pubs.toLowerCase();

        if (webspectator_pubs.length > 0) {
           var ws_pubs = webspectator_pubs.split(",");
           for (p in ws_pubs) {
               if (typeof ws_pubs[p] === "string") {
                    var peca = ws_pubs[p].split(":"),
                        peca_key = peca[0],
                        peca_val = peca[1];
                        ws_pecas[peca_key] = peca_val;
               }
           }
        }

        return posicoes;

    }

    return {
        posicoes: posicoes,
        vincula: vincula
    };
}());

/* PUBLICIDADES.Webspectator.posicoes(); */