/*global document: false */
/*global window */
/*global XMLHttpRequest*/
/*global ActiveXObject*/
/*global console*/

var EJESA = EJESA || {};
var JS_ENVIROMENT = 'js';
var SOLR_NOTICIAS = 'http://odia.ig.com.br/_indice/noticias/',
    BELIA00102 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/be/EJESA_BELIA00102.js?v=0.02',
    BENEA00404 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/be/EJESA_BENEA00404.js?v=0.31',
    BELIA00206 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/be/EJESA_BELIA00206.js?v=0.46',
    BELIA00306 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/be/EJESA_BELIA00306.js?v=1.27',

    /* WEBSPECTATOR = 'http://services.webspectator.com/init/WS-00DIA/' + (+new Date), */
    REALTIME_CORE = '', /* http://clients.realtime.co/odia/rt_core.js */
    IBTX = ''; /* http://www.apps.realtime.co/IBTX/9s3Lrr/IBTX.js */

var SITE_ENVIROMENT;
var JQUERY;


EJESA.namespace = function (ns_string) {
    'use strict';
    var parts = ns_string.split('.'),
        parent = EJESA,
        i;

    if (parts[0] === "EJESA") {
        parts = parts.slice(1);
    }

    for (i = 0; i < parts.lenght; i += 1) {
        if (parent[parts[i]] === 'undefined') {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }
    return parent;
};

/** declarando os módulos **/
EJESA.namespace('EJESA.Util');
EJESA.namespace('EJESA.CALL_BELIA00102'); /** Lista de últimas Home  **/
EJESA.namespace('EJESA.CALL_BENEA00404'); /** Noticias Relacionadas **/
EJESA.namespace('EJESA.CALL_BELIA00206'); /** Noticias Relacionadas **/
/* EJESA.namespace('EJESA.LOAD_REALTIME_CORE'); */ /** Carrega Biblioteca IBT **/
/* EJESA.namespace('EJESA.WEBSPECTATOR'); */ /** Carrega Biblioteca WEBSPECTATOR **/
/* EJESA.namespace('EJESA.LOAD_IBTX'); */ /** Carrega Biblioteca APP IBT **/
/* EJESA.namespace('EJESA.WSPublicidades'); */

/** implementando módulo **/
EJESA.Util = (function () {
    'use strict';
    //propriedades privadas
    //métodos privados
    var zeroPad = function (num) {
        if (num < 10) {
            num = '0' + num;
        }
        return num;
    },

        getEnviroment = function () {
            var site_enviroment,
                strUrl = ' ' + window.location + ' ',
                edition_prod = /cms\.ejesa\.igcorp\.com\.br/,
                prod_preview = /cms\.ejesa\.igcorp\.com\.br\/edicaoHomes\/preview/,
                edition_qa = /cms\-ejesa\-app\-1\.dev\.infra/,
                qa_preview = /cms\-ejesa\-app\-1\.dev\.infra\/edicaoHomes\/preview/;

            site_enviroment = 'production';

            if (strUrl.match(edition_prod)) {
                site_enviroment = 'edition_prod';
            }

            if (strUrl.match(prod_preview)) {
                site_enviroment = 'prod_preview';
            }

            if (strUrl.match(edition_qa)) {
                site_enviroment = 'edition_qa';
            }

            if (strUrl.match(qa_preview)) {
                site_enviroment = 'qa_preview';
            }

            SITE_ENVIROMENT = site_enviroment;

            return site_enviroment;
        },

        dateSolr = function (data, formato) {
            var ext = /^(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)Z$/.exec(data),
                utc = new Date(),
                mon,
                day,
                hour,
                min,
                local;

            utc.setUTCFullYear(ext[1]);
            utc.setUTCMonth(parseInt(ext[2], 10) - 1);
            utc.setUTCDate(ext[3]);
            utc.setUTCHours(ext[4]);
            utc.setUTCMinutes(ext[5]);
            mon = zeroPad(parseInt(utc.getMonth(), 10) + 1);
            day = zeroPad(utc.getDate());
            hour = zeroPad(utc.getHours());
            min = zeroPad(utc.getMinutes());
            local = utc.getFullYear() + '-' + mon + '-' + day + '-' + hour + '-' + min;
            return local.replace(/^(\d+)-(\d+)-(\d+)-(\d+)-(\d+)$/, formato);
        },

        getXHR = function () {
            var i,
                xhr,
                activexIds = [
                    'MSXML2.XMLHTTP.3.0',
                    'MSXML2.XMLHTTP',
                    'Microsoft.XMLHTTP'
                ];
            if (typeof XMLHttpRequest === "function") { // native XHR
                xhr =  new XMLHttpRequest();
            } else { // IE before 7
                for (i = 0; i < activexIds.length; i += 1) {
                    try {
                        xhr = new ActiveXObject(activexIds[i]);
                        break;
                    } catch (e) {}
                }
            }
            return xhr;
        },

        trim = function (str) {
            return str.replace(/^\s+|\s+$/g, "");
        },

        getMetaProperty = function (name) {
            var i, x, y, att, m = document.getElementsByTagName('meta');
            for (i = 0; i < m.length; i = i + 1) {
                x = m[i].attributes;
                for (y = 0; y < x.length; y = y + 1) {
                    att = x[y];
                    if (att.name === 'property' && att.value === name) {
                        return m[i].content;
                    }
                }
            }
        },

        getMetaName = function (name) {
            var i, x, y, att, m = document.getElementsByTagName('meta');
            for (i = 0; i < m.length; i = i + 1) {
                x = m[i].attributes;
                for (y = 0; y < x.length; y = y + 1) {
                    att = x[y];
                    if (att.name === 'name' && att.value === name) {
                        return m[i].content;
                    }
                }
            }
        },

        setClickNodeList = function (nodes, clique) {
            var i, x;
            for (i = 0; i < nodes.length; i = i + 1) {
                x = nodes[i];
                x.click(clique);
            }
        },

        addClassName = function (objElement, strClass, blnMayAlreadyExist) {

            var arrList,
                strClassUpper,
                i;

            if (objElement.className) {
                arrList = objElement.className.split(' ');
                if (blnMayAlreadyExist) {
                    strClassUpper = strClass.toUpperCase();
                    for (i = 0; i < arrList.length; i = i + 1) {
                        if (arrList[i].toUpperCase() === strClassUpper) {
                            arrList.splice(i, 1);
                            i = i - 1;
                        }
                    }
                }
                arrList[arrList.length] = strClass;
                objElement.className = arrList.join(' ');
            } else {
                objElement.className = strClass;
            }
        },

        addClassByID = function (id, strClass, blnMayAlreadyExist) {

            var arrList = [],
                strClassUpper,
                i,
                y,
                objElement,
                classes = '';

            objElement = document.getElementById(id);
            if (objElement.className) {
                arrList = objElement.className.split(' ');
                if (blnMayAlreadyExist) {
                    strClassUpper = strClass.toUpperCase();
                    for (i = 0; i < arrList.length; i = i + 1) {
                        if (arrList[i].toUpperCase() === strClassUpper) {
                            arrList.splice(i, 1);
                            i = i - 1;
                        }
                    }
                }
                arrList[arrList.length] = strClass;
                objElement.className = arrList.join(' ');
            } else {
                arrList[0] = strClass;
                objElement.className = strClass;
            }
            for (y = 0; y < arrList.length; y = y + 1) {
                if (arrList[y] !== '') {
                    classes += arrList[y];
                    classes += ' ';
                }
            }
            objElement.setAttribute('class', classes);
        },

        addClassNodeList = function (nodes, classe) {
            var i, x;
            for (i = 0; i < nodes.length; i = i + 1) {
                x = nodes[i];
                addClassName(x, classe, true);
            }
        },

        hasClassName = function (objElement, strClass) {

            var arrList,
                strClassUpper,
                i;

            if (objElement.className) {
                arrList = objElement.className.split(' ');
                strClassUpper = strClass.toUpperCase();
                for (i = 0; i < arrList.length; i = i + 1) {
                    if (arrList[i].toUpperCase() === strClassUpper) {
                        return true;
                    }
                }
            }

            return false;
        },

        removeClassName = function (objElement, strClass) {

            var arrList,
                strClassUpper,
                i;

            if (objElement.className) {
                arrList = objElement.className.split(' ');
                strClassUpper = strClass.toUpperCase();
                for (i = 0; i < arrList.length; i = i + 1) {
                    if (arrList[i].toUpperCase() === strClassUpper) {
                        arrList.splice(i, 1);
                        i = i - 1;
                    }
                }
            }
        },

        removeClassByID = function (id, strClass) {

            var arrList,
                strClassUpper,
                i,
                y,
                obj,
                classes = '';

            obj = document.getElementById(id);

            if(obj){
                if (obj.className) {
                    arrList = obj.className.split(' ');
                    strClassUpper = strClass.toUpperCase();
                    for (i = 0; i < arrList.length; i = i + 1) {
                        if (arrList[i].toUpperCase() === strClassUpper) {
                            arrList.splice(i, 1);
                            i = i - 1;
                        }
                    }
                    for (y = 0; y < arrList.length; y = y + 1) {
                        if (arrList[y] !== '') {
                            classes += arrList[y];
                            classes += ' ';
                        }
                    }
                }
                obj.setAttribute('class', classes);

            }

        },

        loadScript = function (url, callback) {

            var head, script;
            head = document.getElementsByTagName('head')[0];

            script = document.createElement('script');
            script.type = "text/javascript";
            if (script.readyState) { // IE
                script.onreadystatechange = function () {
                    if (script.readyState === "loaded" || script.readyState === "complete") {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else { // Outros
                script.onload = function () {
                    callback();
                };
            }
            script.src = url;

            head.appendChild(script);
        },

        loadJS = function (url) {

            var head, script;
            head = document.getElementsByTagName('head')[0];

            script = document.createElement('script');
            script.type = "text/javascript";
            script.async = true;
            script.src = url;

            head.appendChild(script);
        },

        fieldsMarked = function (field) {
            var itens = field,
                checks = [],
                i;
            for (i = 0; i < itens.length; i = i + 1) {
                if (itens[i].checked === true) {
                    checks.push(itens[i].value);
                }
            }
            return checks;
        };

    //fim da declaracao var

    //revelando API pública
    return {
        dateSolr: dateSolr,
        getXHR: getXHR,
        trim: trim,
        getMetaProperty: getMetaProperty,
        getMetaName: getMetaName,
        setClickNodeList: setClickNodeList,
        hasClassName : hasClassName,
        addClassName : addClassName,
        removeClassName : removeClassName,
        addClassNodeList : addClassNodeList,
        loadScript : loadScript,
        fieldsMarked : fieldsMarked,
        removeClassByID : removeClassByID,
        addClassByID : addClassByID,
        getEnviroment : getEnviroment,
        loadJS : loadJS
    };
}());

/*
EJESA.WSPublicidades = (function () {
    ws_pecas = {};

    getMetaName = function (name) {
        var i, x, y, att, m = document.getElementsByTagName('meta');
        for (i = 0; i < m.length; i = i + 1) {
            x = m[i].attributes;
            for (y = 0; y < x.length; y = y + 1) {
                att = x[y];
                if (att.name === 'name' && att.value === name) {
                    return m[i].content;
                }
            }
        }
        return getMetaName;
    }

    webspectatorInsertPub = function (pub_position) {
        var strurl = window.location.hostname;
        if(strurl.indexOf('http://cms.ejesa')==-1){
            if (ws_pecas[pub_position]) {
                return ws_pecas[pub_position].replace(/\s+/g, '');
            }
            return webspectatorInsertPub;
        }
    }

    webspectator = function (){

        var webspectator_pubs = document.getElementsByName("pubs-web-spectator")[0].getAttribute("content") || "",
            webspectator_pubs = webspectator_pubs.toLowerCase();

        if (webspectator_pubs.length > 0) {
           var ws_pubs = webspectator_pubs.split(",");
           for (p in ws_pubs) {
               if (typeof ws_pubs[p] === "string") {
                    var peca = ws_pubs[p].split(":"),
                        peca_key = peca[0],
                        peca_val = peca[1];
                        ws_pecas[peca_key] = peca_val;
               }
           }
        }

        return webspectator;

    }

    return {
        getMetaName: getMetaName,
        webspectator: webspectator,
        webspectatorInsertPub: webspectatorInsertPub
    };
}());
*/



/** implementando módulo **/
/** Lista de últimas Home  **/
EJESA.CALL_BELIA00102 = (function () {
    'use strict';
    //métodos privados
    var callInit, init;

    callInit = function () {
        EJESA.BELIA00102.start();
    };

    init = function () {
        EJESA.Util.loadScript(BELIA00102, callInit);
    };

    //revelando API pública
    return {
        init: init
    };
}());



/** implementando módulo **/
/** Noticias Relacionadas **/
EJESA.CALL_BENEA00404 = (function () {
    'use strict';
    //métodos privados
    var callInit, init;

    callInit = function () {
        EJESA.BENEA00404.start();
    };

    init = function () {
        EJESA.Util.loadScript(BENEA00404, callInit);
    };

    //revelando API pública
    return {
        init: init
    };
}());


/** implementando módulo **/
/** Carrega WEBSPECTATOR **/
/*
EJESA.LOAD_WEBSPECTATOR = (function () {
    'use strict';
    //métodos privados
    var callInit, init;

    callInit = function () {
        EJESA.Util.loadJS(WEBSPECTATOR);
    };

    init = function () {
        EJESA.Util.getEnviroment();
        if (SITE_ENVIROMENT === 'production') {
            callInit();
        }
    };

    //revelando API pública
    return {
        init: init,
        callInit: callInit
    };
}());
*/


/** implementando módulo **/
/** Carrega script IBT para publicidade **/
/*
EJESA.LOAD_REALTIME_CORE = (function () {
    'use strict';
    //métodos privados
    var callInit, init;

    callInit = function () {
        EJESA.Util.loadJS(REALTIME_CORE);
    };

    init = function () {
        EJESA.Util.getEnviroment();
        if (SITE_ENVIROMENT === 'production') {
            callInit();
        }
    };

    //revelando API pública
    return {
        init: init,
        callInit: callInit
    };
}());
*/


/** implementando módulo **/
/** Carrega biblioteca de APP IBT **/
/*
EJESA.LOAD_IBTX = (function () {
    'use strict';
    var callInit, init;

    callInit = function () {
        EJESA.Util.loadJS(IBTX);
    };

    init = function () {
        EJESA.Util.getEnviroment();
        if (SITE_ENVIROMENT === 'production') {
            callInit();
        }
    };

    //revelando API pública
    return {
        init: init,
        callInit: callInit
    };
}());
*/

/** implementando módulo **/
/** Ultimas Noticias  **/
EJESA.CALL_BELIA00206 = (function () {
    'use strict';
    //métodos privados
    var callInit, init;

    callInit = function () {
        EJESA.BELIA00206.start();
    };

    init = function () {
        EJESA.Util.loadScript(BELIA00206, callInit);
    };

    //revelando API pública
    return {
        init: init
    };
}());



/** implementando módulo **/
/** Empilhamento Video **/
EJESA.CALL_BELIA00306 = (function () {
    'use strict';
    //métodos privados
    var callInit, init;

    callInit = function () {
        EJESA.BELIA00306.start();
    };

    init = function () {
        EJESA.Util.loadScript(BELIA00306, callInit);
    };

    //revelando API pública
    return {
        init: init
    };
}());



$(document).ready(function(){
    $(".BETA00404").css("float", "none");
});

