/*global document: false */
/*global window: false */
/*global DOMParser: false */
/*global ActiveXObject: false */
/*global console*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.COPACONFEDERACOES.Resultados'); /** Tabela de Classificação do campeonato **/


/** implementando módulo **/
/** Tabela de Classificação do campeonato **/
EJESA.COPACONFEDERACOES.Resultados = (function () {
    'use strict';
    var start,
        getXML,
        path = "http://www.ongoing.com.br/dataodia/",
        xml =   [
            'condeferacoes_jogos_671.xml',
            'condeferacoes_jogos_587.xml'
        ],
        tabs = [
            "grupoa",
            "grupob"
        ],
        renderiza,
        mountGame,
        mountHTML,
        games = [];

    start = function () {
        var i, url;
        for (i = 0; i < tabs.length; i += 1) {
            url = path + xml[i];
            getXML(url, tabs[i]);
        }
    };

    getXML = function (xml) {
        var xhr;

        xhr = EJESA.COPACONFEDERACOES.Util.getXHR();

        xhr.onreadystatechange = function () {
            if (xhr.readyState !== 4) {
                return false;
            }
            if (xhr.status !== 200) {
                console.log("Error, status code: " + xhr.status);
                return false;
            }
            renderiza(xhr.responseText);
        };

        xhr.open("GET", xml, true);
        xhr.send("");

    };

    renderiza = function (data) {

        var parser,
            xmlDoc,
            partidas,
            partida,
            i;

        if (window.DOMParser) {
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(data, "text/xml");
        } else {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            xmlDoc.loadXML(data);
        }

        partidas = xmlDoc.getElementsByTagName('Partida');
        for (i = 0; i < partidas.length; i = i + 1) {
            partida = partidas[i];
            mountGame(partida);
        }
        mountHTML();
    };

    mountHTML = function () {

        var i,
            game,
            html,
            data;

        for (i = 0; i < games.length; i += 1) {
            game = games[i];
            data = game[1][0].textContent;
            data = data.replace('/2013', ' -');


            html = '';
            html += "<li class='resultado'>";
            html += "<div class='data'>" + data + "</div>";
            html += "<div class='times'>";
            html += game[2][0].textContent;
            html += " ";
            html += game[3][0].textContent;
            html += " X ";
            html += game[5][0].textContent;
            html += " ";
            html += game[4][0].textContent;
            html += "</div>";
            html += "<div class='status'>" + game[0][0].textContent.substring(7) + "</div>";
            html += "</li>";

            $('#carousel_ul').append(html);
        }

    };

    mountGame = function (game) {
        var status,
            data,
            time1,
            placar1,
            time2,
            placar2,
            obj = [];

        status = game.getElementsByTagName('Status');
        data = game.getElementsByTagName('Data');
        time1 = game.getElementsByTagName('SiglaMandante');
        placar1 = game.getElementsByTagName('PlacarMandante');
        time2 = game.getElementsByTagName('SiglaEquipeVisitante');
        placar2 = game.getElementsByTagName('PlacarVisitante');

        obj.push(status);
        obj.push(data);
        obj.push(time1);
        obj.push(placar1);
        obj.push(time2);
        obj.push(placar2);

        games.push(obj);

    };

    return {
        start: start
    };

}());

