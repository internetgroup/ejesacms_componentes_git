/*global document: false */
/*global console*/
// 3.alterar variavel query do módulo EJELIA00602

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.EJELIA00906'); /** Lista de últimas da Notícia **/

/** implementando módulo **/
/** Lista de últimas da Notícia **/
EJESA.EJELIA00906 = (function () {
    'use strict';
    //propriedades privadas
    var QTDNEWS = 20,
        SITE = 'odia',
        SOLR = 'http://odia.ig.com.br/_indice/noticias/select?',
        FORMAT = 'json',
        start,
        renderiza,
        page = 1,
        pageIdentify,
        makeAddress,
        executeSearch,
        pageMarked,
        resetPagination,
        terms = '',
        mountTerms,
        noticias = 'http://odia.ig.com.br/noticias/';
    //fim da declaracao var


    //métodos privados
    mountTerms = function() {
        var term,
            i,
            urlPage,
            urlSplit,
            urlTerms,
            urlClean,
            idx;

        urlPage = window.location.href;
        idx = urlPage.indexOf('?');
        if (idx > 0) {
            urlClean = urlPage.substring(0, idx);
        } else {
            urlClean = urlPage;
        }

        urlSplit = urlClean.split("/");
        urlTerms = urlSplit.slice(4);
        for (i = 0; i < urlTerms.length; i += 1) {

            term = "\"";
            term += urlTerms[i].replace(/_/g, ' ');
            term += "\"";
            terms = term;
        }
        noticias += urlTerms;
    };

    //métodos privados
    pageIdentify = function () {
        var start,
            url = window.location,
            urlString = url.toString(),
            urlArray = urlString.split("?");

        if (urlArray !== undefined && urlArray.length > 1) {
            page = urlArray[1];
        }

        //console.log(page);


        start = (page - 1) * QTDNEWS;
        return start;
    };

    makeAddress = function (start) {

        var address = SOLR;

        address += 'start=';
        address += start;
        address += '&size=';
        address += QTDNEWS;
        address += '&site=';
        address += SITE;
        address += '&comb_termos=';
        address += terms;
        address += '&wt=';
        address += FORMAT;

        return address;
    };



    pageMarked = function () {
        var mp,
            range,
            posn = 0,
            posp = 0,
            pos0 = 0,
            pos1 = 0,
            pos2 = 0,
            pos3 = 0,
            pos4 = 0,
            pos5 = 0,
            urln = '',
            urlp = '',
            posid;

        noticias += '?';

        if (page > 5) {
            range = page / 5;

            pos0 = (range.toFixed(0) * 5);



            if (page % 5 === 1) {

                pos1 = pos0 + 1;
                pos2 = pos0 + 2;
                pos3 = pos0 + 3;
                pos4 = pos0 + 4;
                pos5 = pos0 + 5;

                document.getElementById('hn_lnk_1').innerHTML = pos1;
                document.getElementById('hn_lnk_2').innerHTML = pos2;
                document.getElementById('hn_lnk_3').innerHTML = pos3;
                document.getElementById('hn_lnk_4').innerHTML = pos4;
                document.getElementById('hn_lnk_5').innerHTML = pos5;

                posid = page % 5;

            } else {
                posid = page % 5;
                if (posid === 0) {

                    pos1 = parseInt(page, 10) + 1;
                    pos2 = parseInt(page, 10) + 2;
                    pos3 = parseInt(page, 10) + 3;
                    pos4 = parseInt(page, 10) + 4;
                    pos5 = parseInt(page, 10) + 5;


                    document.getElementById('hn_lnk_1').innerHTML = pos0 - 4;
                    document.getElementById('hn_lnk_2').innerHTML = pos0 - 3;
                    document.getElementById('hn_lnk_3').innerHTML = pos0 - 2;
                    document.getElementById('hn_lnk_4').innerHTML = pos0 - 1;
                    document.getElementById('hn_lnk_5').innerHTML = pos0;

                    posid = 5;
                } else {
                    pos1 = pos0 + 1;
                    pos2 = pos0 + 2;
                    pos3 = pos0 + 3;
                    pos4 = pos0 + 4;
                    pos5 = pos0 + 5;

                    document.getElementById('hn_lnk_1').innerHTML = pos1;
                    document.getElementById('hn_lnk_2').innerHTML = pos2;
                    document.getElementById('hn_lnk_3').innerHTML = pos3;
                    document.getElementById('hn_lnk_4').innerHTML = pos4;
                    document.getElementById('hn_lnk_5').innerHTML = pos5;
                }
            }

            document.getElementById('hn_lnk_1').href = noticias + pos1;
            document.getElementById('hn_lnk_2').href = noticias + pos2;
            document.getElementById('hn_lnk_3').href = noticias + pos3;
            document.getElementById('hn_lnk_4').href = noticias + pos4;
            document.getElementById('hn_lnk_5').href = noticias + pos5;


            mp = 'hn_pos_' + posid;

        } else {

            document.getElementById('hn_lnk_1').href = noticias + 1;
            document.getElementById('hn_lnk_2').href = noticias + 2;
            document.getElementById('hn_lnk_3').href = noticias + 3;
            document.getElementById('hn_lnk_4').href = noticias + 4;
            document.getElementById('hn_lnk_5').href = noticias + 5;

            mp = 'hn_pos_' + page;
        }

        posp = parseInt(page, 10) - 1;
        posn = parseInt(page, 10) + 1;
        urln = noticias + posn;
        urlp = noticias + posp;

        document.getElementById('hn_pos_p').href = urlp;
        document.getElementById('hn_pos_n').href = urln;

        EJESA.Util.removeClassByID(mp, 'headline_nav_off');
        EJESA.Util.addClassByID(mp, 'headline_nav_on', true);


    };

    resetPagination = function () {
        EJESA.Util.removeClassByID('hn_pos_1', 'headline_nav_on');
        EJESA.Util.removeClassByID('hn_pos_2', 'headline_nav_on');
        EJESA.Util.removeClassByID('hn_pos_3', 'headline_nav_on');
        EJESA.Util.removeClassByID('hn_pos_4', 'headline_nav_on');
        EJESA.Util.removeClassByID('hn_pos_5', 'headline_nav_on');

        EJESA.Util.addClassByID('hn_pos_1', 'headline_nav_off');
        EJESA.Util.addClassByID('hn_pos_2', 'headline_nav_off');
        EJESA.Util.addClassByID('hn_pos_3', 'headline_nav_off');
        EJESA.Util.addClassByID('hn_pos_4', 'headline_nav_off');
        EJESA.Util.addClassByID('hn_pos_5', 'headline_nav_off');
    };





    executeSearch = function (address) {


        var xhr;

        xhr = EJESA.Util.getXHR();

        xhr.onreadystatechange = function () {
            if (xhr.readyState !== 4) {
                return false;
            }
            if (xhr.status !== 200) {
                console.log("Error, status code: " + xhr.status);
                return false;
            }
            renderiza(xhr.responseText);
        };

        xhr.open("GET", address, true);
        xhr.send("");
    };

    renderiza = function (data) {

        var obj = JSON.parse(data),
            docs,
            i,
            limit,
            date_news,
            token,
            tit,
            sub,
            html = '';

        docs = obj.response.docs;

        for (i = 0; i < docs.length; i = i + 1) {

            var buscaTags = docs[i].palavrasChaves;
            if(buscaTags.indexOf("chargedoaroeira") == -1){

                html += "<li>";

                html += "<div class='item'><a href='" + docs[i].url + "' target='_blank'><span class='photo'>";

                limit = 0;
                if (docs[i].urlImgEmp_idCorteImagem !== undefined) {

                    html += "<img src='" + docs[i].urlImgEmp_140x90 + "'>";
                    limit = 120;
                    token = true;
                } else {
                    token = false;
                    limit = 280;
                }

                html += "</span>";

                date_news = EJESA.Util.dateSolr(docs[i].startDate, '$3/$2/$1 - $4:$5');

                if (token) {
                    html += "<span class='headline'>";
                } else {
                    html += "<span class='headline-full'>";
                }

                html += "<span class='date'>";
                html += date_news + ' - ' + docs[i].secaoBreadcrumb;
                html += "</span>";
                html += "<h1>";

                tit = '';
                if (docs[i].titulo.length > limit) {
                    tit = docs[i].titulo.substring(0, limit) + "...";
                } else {
                    tit = docs[i].titulo;
                }

                html += tit;
                html += "</h1><h2>";
                if (docs[i].olho.length > limit) {
                    sub = docs[i].olho.substring(0, limit) + "...";
                } else {
                    sub = docs[i].olho;
                }
                html += sub;
                html += "</h2></span></a>";



                html += "</li>";

            }
        }
        document.getElementById('EJELIA00906HL').innerHTML = html;
    };


    start = function () {

        var start,
            address;

        start = pageIdentify();

        mountTerms();

        address = makeAddress(start);

        executeSearch(address);

        resetPagination();

        pageMarked();


    };

    //revelando API pública
    return {
        start: start
    };
}());

