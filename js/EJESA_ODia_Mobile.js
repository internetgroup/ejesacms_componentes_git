var ODIAMOBILE = ODIAMOBILE || {};

ODIAMOBILE.criaNS = function (namespace) {
    var nsparts = namespace.split(".");
    var parent = ODIAMOBILE;
    if (nsparts[0] === "ODIAMOBILE") {
        nsparts = nsparts.slice(1);
    }
    for (var i = 0; i < nsparts.length; i++) {
        var partname = nsparts[i];
        if (typeof parent[partname] === "undefined") {
            parent[partname] = {};
        }
        parent = parent[partname];
    }
    return parent;
};

ODIAMOBILE.criaNS("ODIAMOBILE.LOADIFRAMES");
ODIAMOBILE.LOADIFRAMES = (function () {
   	'use strict';

    setIframe = function (secao,url,altura) {
    	var link = 'http://odia.ig.com.br/'+secao+'/'+url+secao+'.mobile';
        var iframe = document.createElement('iframe');
        iframe.frameBorder=0;
        iframe.width='100%';
        iframe.height=parseInt(altura)+'px';
        iframe.id=secao;
        iframe.setAttribute('scrolling', 'no');
        iframe.setAttribute('src', link);
        /* iframe.setAttribute('style', 'margin:30px 0 0 -20px;padding:0;display:none;');
         iframe.setAttribute('onload', 'ajustaLoad("'+url+'")'); */
        iframe.setAttribute('id', url);

        var alvo_id = document.getElementById('add-editorias');
        alvo_id.parentNode.insertBefore(iframe, alvo_id);
    }
});

ODIAMOBILE.criaNS("ODIAMOBILE.PUBLICIDADE");
ODIAMOBILE.PUBLICIDADE = (function () {
   	'use strict';
});

document.addEventListener("DOMContentLoaded", function(event) { 
	ODIAMOBILE.LOADIFRAMES.setIframe('rio-de-janeiro', 'noticia/rio-de-janeiro', 200);
	ODIAMOBILE.LOADIFRAMES.setIframe('esporte', 'esporte', 200);
	ODIAMOBILE.LOADIFRAMES.setIframe('diversao', 'diversao', 200);
	ODIAMOBILE.LOADIFRAMES.setIframe('economia', 'noticia/economia', 200);
});