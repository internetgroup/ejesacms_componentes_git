/*global document: false */
/*global console*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.EJENEA01008'); /** Lista de noticias relacionadas **/


/** implementando módulo **/
/** Lista de noticias relacionadas **/
EJESA.EJENEA01008 = (function () {

	'use strict';
	/* propriedades privadas */
	var QTDNEWS = 2,
		SITE = 'odia',
		SOLR = 'http://odia.ig.com.br/_indice/noticias/select?',
		FORMAT = 'json',
		start,
		achou = false,
		posA=-1,
		posP,
		auxiliar = -10,
		renderiza,
		montaNoticias,
		tipoBusca,
		page = 1,
		makeAddress,
		realizaBuscaDia,
		realizaBuscaMes,
		executeSearch,
		parseTag,
		date_news2 = '',
		tags = EJESA.Util.getMetaName('tags'),
		sessao = EJESA.Util.getMetaName('odia-section-id'),
		idnoticia = EJESA.Util.getMetaName('articleid') ;


	parseTag = function (tags) {
		var mytag = '',
			tag,
			mytags,
			i;
		mytags = tags.split('%20');
		for (i = 0; i < mytags.length; i += 1) {	
			tag = "\"";
			tag += mytags[i].replace(/_/g, ' ');
			tag += "\"";
			mytag += tag;
		}
		return mytag;
	};


	makeAddress = function () {

		var address = SOLR;

		address += 'site=';
		address += SITE;
		address += '&secoes_EH=';
		address += sessao;
		address += '&size=1';
		address += '&wt=';
		address += FORMAT;
		address += '&id=';
		address += idnoticia;

		return address;

	};


	realizaBuscaDia = function (aux) {

		var address = SOLR;

		address += 'site=';
		address += SITE;
		address += '&secoes_EH=';
		address += sessao;
		address += '&size=80';
		address += '&wt=';
		address += 'json';
		address += '&periodo=';
		address += 'hoje';

		address += '&start=';
		address += aux;
		
		return address;

	};

	realizaBuscaMes = function (aux) {

		var address = SOLR;

		address += 'site=';
		address += SITE;
		address += '&secoes_EH=';
		address += sessao;
		address += '&size=80';
		address += '&wt=';
		address += 'json';
		address += '&periodo=';
		address += 'mes';

		address += '&start=';
		address += aux;

		return address;

	};


	executeSearch = function (address) {

		var xmlhttp;

        if (window.XMLHttpRequest) {/* code for IE7+, Firefox, Chrome, Opera, Safari */
          xmlhttp = new XMLHttpRequest();
        }
        else {/* code for IE6, IE5 */
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        xmlhttp.onreadystatechange = function(){

            if (xmlhttp.readyState !== 4) {
                return false;
            }
            if (xmlhttp.status !== 200) {
                console.log("Error, status code: " + xmlhttp.status);
                return false;
            }

            renderiza(xmlhttp.responseText);
                
        }

        xmlhttp.open("GET",address,true);
        xmlhttp.send("");
	};

	renderiza = function (data) {

		var obj = JSON.parse(data);
		montaNoticias(obj);

	};

	montaNoticias = function(obj){

			var hoje,
				dia,
				mes,
				dataHoje,
				dataNoticia = '',
				buscaNoticiaDia,
				buscaNoticiaMes,
				docs,
				html_prev = '',
				html_next = '',
				limit,
				tit,
				tituloprev ='',
				posicaoAnterior,
				posicaoPosterior;
			
			hoje = new Date();
			dia = hoje.getDate();
			mes = hoje.getMonth()+1;

		

			docs = obj.response.docs;


		/*Teremos 2 objetos: um com um tamanho = 1, que correspondera ao que retorna a noticia atual
		Outro que corresponde a busca e que será acrescido toda a vez que nao for encontrado um igual ao id

		busco pelo objeto
		se o valor da dataNoticia for igual a '' e o tamanho do objeto for = 1 
			entao eu preciso calcular a data de hoje e compara-la com a da noticia */

			if(dataNoticia == '' && docs.length == 1){
				dataNoticia = (EJESA.Util.dateSolr(docs[0].startDate, '$3$2'));
			}
			else{
			
				auxiliar +=10;

				if(achou == false){
					if(typeof $("meta[name='articleid']") != "undefined" && $("meta[name='articleid']").size()>0){
						buscaNoticiaMes = realizaBuscaMes(auxiliar);
						executeSearch(buscaNoticiaMes);
					}

					for (var i = 0; i < docs.length ; i++){
						if(obj.response.docs[i].id == idnoticia && achou !=true){	
							if(i == 0){
								posicaoAnterior = 1
								posicaoPosterior = i-1;
								achou = true;
								i = docs.length;
							}
							else{
								if(i == 80){
									achou = false;
								}	
								else{
									posicaoAnterior = i+ 1;
									posicaoPosterior = i - 1;
									achou = true;
									i = docs.length;

								}
								
							}

						}
					}
				}

			}
			if(typeof posicaoPosterior != "undefined"){
				posP = posicaoPosterior;
				if(docs.length == 1){
					posP = 0;
				}
				else{
					posP = posicaoPosterior;
				}
			}
			
			if(typeof posicaoAnterior != "undefined" ){
				if(docs.length == 1){
					posA = 0;
				}
				else{
					posA = posicaoAnterior;
				}
			}
			
			if(achou == true){
			
				if(posP == -1 ){
					html_next += "<div class='headline'>";

					html_next += "<span class='headline-item-eye'>";
					html_next += "Não existem notícias posteriores"
					html_next += "</br></span>";

					html_next += "<span class='headline-item-title'>";
					html_next += "<a>Não existem notícias posteriores</a>";
					html_next += "</span>";
						
				}

				else{

					html_next += "<div class='headline'>";

					limit = 100;

					html_next += "<span class='headline-item-eye'>";
					html_next += EJESA.Util.dateSolr(obj.response.docs[posP].startDate, '$4:$5');
					html_next += "</br></span>";

					html_next += "<span class='headline-item-title'>";
					html_next += "<a href='" + obj.response.docs[posP].url + "'>";

					tit = '';
					if (obj.response.docs[posP].titulo > limit) {
						tit = obj.response.docs[posP].titulo.substring(0, limit) + "...";
					} else {
						tit = obj.response.docs[posP].titulo;
					}

					html_next += tit;
					html_next += "</a>";
					html_next+= "</span>";
				

					if(typeof document.getElementById('next-link') != "undefined" && document.getElementById('next-link') != null){
						document.getElementById('next-link').href =  String(obj.response.docs[posP].url);

					}
				}

				if(posA != -1) {

					html_prev += "<div class='headline'>";

					limit = 100;

					html_prev += "<span class='headline-item-eye'>";
					html_prev += EJESA.Util.dateSolr(obj.response.docs[posA].startDate, '$4:$5');
					html_prev += "</br></span>";

					html_prev += "<span class='headline-item-title'>";
					html_prev += "<a href='" + obj.response.docs[posA].url + "'>";

					tit = '';
					if (obj.response.docs[posA].titulo > limit) {
					tit = obj.response.docs[posA].titulo.substring(0, limit) + "...";
					} else {
					tit = obj.response.docs[posA].titulo;
					}

					html_prev += tit;
					html_prev += "</a>";
					html_prev += "</span>";

					if(typeof document.getElementById('prev-link') != "undefined" && document.getElementById('prev-link') != null){
						document.getElementById('prev-link').href =  String(obj.response.docs[posA].url);

					}

				}
				
			}
		
			if(typeof $("meta[name='articleid']") != "undefined" && $("meta[name='articleid']").size()>0){
				document.getElementById('EJENEA01008HLP').innerHTML = html_prev;
				document.getElementById('EJENEA01008HLN').innerHTML = html_next;
			}

	}

	start = function () {

		var address,
			buscaNoticiaDia,
			buscaNoticiaMes;

		address = makeAddress();
		buscaNoticiaDia = realizaBuscaDia(auxiliar);

		executeSearch(address);
		executeSearch(buscaNoticiaDia);

	};


	/*revelando API pública*/
	return {
		start: start
	};
}());
