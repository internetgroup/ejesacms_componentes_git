/*global document: false */
/*global console*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.BELIA00206'); /** Lista de noticias relacionadas **/


/** implementando módulo **/
/** Lista de noticias relacionadas **/
EJESA.BELIA00206 = (function () {

	'use strict';
	//propriedades privadas
	var QTDNEWS = 10,
		SITE = 'brasileconomico',
		SOLR ,
		FORMAT = 'json',
		start,
		titulosecao ='',
		renderiza,
		makeAddress,
		exibeMais,
		address,
		inicio = 0,
		page = 1,
        terms = '',
        mountTerms,
		id = EJESA.Util.getMetaName('be-section-id'),
		idparent = EJESA.Util.getMetaName('be-section-parent'),
        noticias = 'http://brasileconomico.ig.com.br/noticias/',
		executeSearch;
			//fim da declaracao var


       var site_enviroment,
            strUrl = ' ' + window.location + ' ',
            edition_prod = 'cms.ejesa.igcorp.com.br/edicao.html#start=0',
            prod_preview = 'cms.ejesa.igcorp.com.br/edicaoHomes/preview',
            edition_qa = 'cms-ejesa-qa.igcorp.com.br/edicao.html#start=0',
            qa_preview = 'cms-ejesa-qa.igcorp.com.br/edicaoHomes/preview',
            prod_site = 'brasileconomico.ig.com.br';

            site_enviroment = '';

            SOLR = 'http://brasileconomico.ig.com.br/_indice/noticias/select?';

            if (strUrl.match(edition_qa) ||strUrl.match(qa_preview) ){
                SOLR = 'http://cms-ejesa-qa.igcorp.com.br/solr/editorialStaging/select?sort=updatedDate%20desc&q=%2Bsite:brasileconomico%20%2B';
           }

            else if (strUrl.match(edition_prod) ||strUrl.match(prod_preview)  || strUrl.match(prod_site)){
                SOLR = 'http://brasileconomico.ig.com.br/_indice/noticias/select?secoes_EH=';
            }

//métodos privados
    mountTerms = function() {
        var term,
            i,
            urlPage,
            urlSplit,
            urlTerms,
            urlClean,
            idx;

        urlPage = window.location.href;
        idx = urlPage.lastIndexOf('?');
        if (idx > 0) {
            urlClean = urlPage.substring(0, idx);
        } else {
            urlClean = urlPage;
        }

        urlSplit = urlClean.split("/");
        urlTerms = urlSplit.slice(4);
        for (i = 0; i < urlTerms.length; i += 1) {
	        if( window.location.href.split("/").slice(4) != "noticias" ){
	            term = "%22";
	            term += urlTerms[i].replace(/_/g, ' ');
	            term += "%22";
	            terms = term;
        	}
        }

		noticias += urlTerms;
    };



	makeAddress = function (inicio) {
		var address = SOLR;

		//se for o ultimas da home, deve exibir tudo


		//se for o 'ultimas noticias de uma secao, deve exibir do pai'
		//id correspondente do ultimas de cada secao (QA):
		


		//ultimas home: 52d7df0c923c1e6e3b0013ca
		// brasil:52d7de8f923c1e6e3b0013c5
		//financas: 52d7e177923c1e6e3b0013ec
		//negocios: 52d7e4af3320ca4169001402
		//mundo: 52d7e4ec3320ca4169001405
		//tecnologia: 52d7e6bc3320ca416900140b
		// opiniao : 52d7e74f923c1e6e3b0013ff
		//vida e estilo: 52d7e7e6923c1e6e3b001405


		//se for a página de cada secao (editoria) deve exibir de todas as paginas

		//variaveis de produçao:




		// brasil: 52d7dc5d3320ca41690013df
			// economia: 52d7e11a923c1e6e3b0013e6
			// politica : 52d7e145923c1e6e3b0013e9
			// mosaico politico: 532843c9442c3b3a3c0008b3
			// olhar do planalto: 53284470442c3b3a3c0008c4
			// relatorio dc : 53284444442c3b3a3c0008c1
			// ponto final: 532845eb442c3b3a3c0008cb
			// sintonia fina : 533abb086a71dddf1600011a


		//financas: 52d7dc8a3320ca41690013e1
			//mercados : 52d7e2983320ca41690013f3
			// financas pessoais : 52d7e2983320ca41690013f3
			// o mercado como ele e : 5328461c442c3b3a3c0008ce

		//negocios: 52d7dc9a3320ca41690013e3
			// plano de negocios: 52d7e40a3320ca41690013fc
			// pme: 52d7e47d3320ca4169001400
			// cliente e cia: 5328450c442c3b3a3c0008c6

		//mundo: 52d7dce73320ca41690013e8
			// informe new york: 53284406442c3b3a3c0008bf
			// mundo emergente: 533a9d0d6a71dddf160000fe

		//tecnologia: 52d7dd213320ca41690013ea
			//coluna nelson: 52d7e713923c1e6e3b0013fd

		// opiniao : 52d7dd3a3320ca41690013ec

		//vida e estilo: 52d7ddb1923c1e6e3b0013bf
			// automania : 52d7e806923c1e6e3b001407
			// esporte clube: 52d7e8503320ca416900140d
			// estilo sa: 52d7e86f3320ca416900140f
			// alem da taca : 5328455f442c3b3a3c0008c8



		//Se for uma das secoes de ultimas noticias
			//se nao for a home
			//se for uma das secoes

		if( id != "52d7df0c923c1e6e3b0013ca"){
			if( id == "52d7de8f923c1e6e3b0013c5" || id == "52d7e177923c1e6e3b0013ec" || id == "52d7e4af3320ca4169001402" || id == "52d7e4ec3320ca4169001405" || id == "52d7e6bc3320ca416900140b" || id == "52d7e74f923c1e6e3b0013ff" || id == "52d7e7e6923c1e6e3b001405"){
				address += idparent;
			}
			else if(id == "52d7dc5d3320ca41690013df"){
				titulosecao = "Brasil";
				address += "52d7dc5d3320ca41690013df%2052d7e11a923c1e6e3b0013e6%2052d7e145923c1e6e3b0013e9%20532843c9442c3b3a3c0008b3%2053284470442c3b3a3c0008c4%2053284444442c3b3a3c0008c1%20532845eb442c3b3a3c0008cb%20533abb086a71dddf1600011a";
				// brasil: 52d7dc5d3320ca41690013df%2052d7e11a923c1e6e3b0013e6%2052d7e145923c1e6e3b0013e9%20532843c9442c3b3a3c0008b3%2053284470442c3b3a3c0008c4%2053284444442c3b3a3c0008c1%20532845eb442c3b3a3c0008cb%20533abb086a71dddf1600011a
			}
			else if(id == "52d7dc8a3320ca41690013e1"){
				titulosecao = "Finanças";
				address += "52d7e2983320ca41690013f3%2052d7e2983320ca41690013f3%205328461c442c3b3a3c0008ce"
				//financas:52d7e2983320ca41690013f3%2052d7e2983320ca41690013f3%205328461c442c3b3a3c0008ce"
			}
			else if(id == "52d7dc9a3320ca41690013e3"){
				titulosecao = "Negócios";
				address += "52d7dc9a3320ca41690013e3%2052d7e40a3320ca41690013fc%2052d7e47d3320ca4169001400%205328450c442c3b3a3c0008c6"
				//negocios: "52d7dc9a3320ca41690013e3%2052d7e40a3320ca41690013fc%2052d7e47d3320ca4169001400%205328450c442c3b3a3c0008c6"
			}
			else if(id == "52d7dce73320ca41690013e8"){
				titulosecao = "Mundo";
				address += "52d7dce73320ca41690013e8%2053284406442c3b3a3c0008bf%20533a9d0d6a71dddf160000fe"
				//mundo: 	52d7dce73320ca41690013e8%2053284406442c3b3a3c0008bf%20533a9d0d6a71dddf160000fe				
			}
			else if(id == "52d7dd213320ca41690013ea"){
				titulosecao = "Tecnologia";
				address += "52d7dd213320ca41690013ea%2052d7e713923c1e6e3b0013fd"
				//tecnologia: 52d7dd213320ca41690013ea%2052d7e713923c1e6e3b0013fd	
			}
			else if(id == "52d7dd3a3320ca41690013ec"){
				titulosecao = "Opinião";
				address += "52d7dd3a3320ca41690013ec"
				// opiniao : 52d7dd3a3320ca41690013ec
			}
			else if(id == "52d7ddb1923c1e6e3b0013bf"){
				titulosecao = "Vida & Estilo";
				address += "52d7ddb1923c1e6e3b0013bf%2052d7e806923c1e6e3b001407%2052d7e8503320ca416900140d%2052d7e86f3320ca416900140f%205328455f442c3b3a3c0008c8"

				//vida e estilo: 52d7ddb1923c1e6e3b0013bf%2052d7e806923c1e6e3b001407%2052d7e8503320ca416900140d%2052d7e86f3320ca416900140f%205328455f442c3b3a3c0008c8
			}
			else{
				address += id;
			}

		}


		address += '&start=';
		address += inicio;
		address += '&size=';
		address += QTDNEWS;
		address += '&site=';
		address += SITE;
		address += '&wt=';
		address += FORMAT;
	 //   address += '&comb_termos=';
      //  address += terms;

		return address;
	};


	executeSearch = function (query) {

		/*
		var i, xhr;

		xhr = EJESA.Util.getXHR();

	    xhr.onreadystatechange = function () {
	        if (xhr.readyState !== 4) {
	            return false;
	        }
	        if (xhr.status !== 200) {
	            console.log("Error, status code: " + xhr.status);
	            return false;
	        }
	        renderiza(xhr.responseText);
	    };

	    xhr.open("GET", query, true);
	    xhr.send("");
	    */

	    var xmlhttp;

        if (window.XMLHttpRequest) {
          xmlhttp = new XMLHttpRequest();
        }
        else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }

        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState !== 4) {
                return false;
            }
            if (xmlhttp.status !== 200) {
                console.log("Error, status code: " + xmlhttp.status);
                return false;
            }
            renderiza(xmlhttp.responseText);
        }
        xmlhttp.open("GET",query,true);
        xmlhttp.send("");

	};

	renderiza = function (data) {
		var obj = JSON.parse(data),
			docs,
			i,
			limit,
			date_news,
			hora_news,
			sections,
			mysection,
			tit,
			html = '',
			eye,
			tamanho,
			tammax;


		docs = obj.response.docs;


		if (titulosecao == ''){
			var tam;
			tam = docs[0].secaoBreadcrumb.split("&nbsp;").length;
			titulosecao = docs[0].secaoBreadcrumb.split("&nbsp;")[tam-1];
		}

		tammax  = obj.response.numFound;

		if(inicio == 0 && id != "52d7df0c923c1e6e3b0013ca"){
			html += "<div class='row titulo-separador'>"
			html += "	<div class='row col-md-12'>"
			html += "		<h2> Mais de "+titulosecao+" »</h2>"
			html += "	</div>"
			html += "</div>"
		}

        if(docs.length > 5){
        	if(docs.length < 10){
        		tamanho = docs.length;
        	}else{
        		tamanho = QTDNEWS;
        	}
        }
        else{
        	tamanho = docs.length;
        }


        for (i = 0; i < tamanho; i += 1) {

			if (typeof docs[i].urlImgEmp_282x162 === "undefined" || docs[i].urlImgEmp_282x162 == "") {

				html += "<li class='col-md-12 artigo-sem-imagem'>";
				html += "	<div class='row'>";
				html += "		<div class='col-md-2'>";

	 			hora_news = EJESA.Util.dateSolr(docs[i].startDate, '$4:$5');
	 			date_news = EJESA.Util.dateSolr(docs[i].startDate, '$3/$2/$1');

			//	html += " 			<span class='hora-postagem'>"+hora_news+" </span>
				html += "			<span class='data-postagem'> "+date_news+"</span> " ;
				html += "		</div>";

				eye = '';
				if (docs[i].olho.length > 100) {
					eye = docs[i].olho.substring(0, 100) + "...";
				} else {
					eye = docs[i].olho;
				}


				tit = '';
				if (docs[i].titulo.length > 300) {
					tit = docs[i].titulo.substring(0, 300) + " ...";
				} else {
					tit = docs[i].titulo;
				}

				html += "			<div class='col-md-10'>";
				html += "				<a href='" + docs[i].url + "' title=''>";
				html += "					<h3>"+tit+"</h3>";
				html += "					<p>"+eye+"</p>";
				html += "				</a>";
				html += "			</div>";


				}
			else {
				html += "<li class='col-md-12'>";
				html += "	<div class='row'>";
				html += "		<div class='col-md-2'>";

	 			hora_news = EJESA.Util.dateSolr(docs[i].startDate, '$4:$5');
	 			date_news = EJESA.Util.dateSolr(docs[i].startDate, '$3/$2/$1');
			//	html += " 			<span class='hora-postagem'>"+hora_news+" </span>
				html += "			<span class='data-postagem'> "+date_news+"</span> " ;		
				html += "		</div>";

				eye = '';
				if (docs[i].olho.length > 100) {
					eye = docs[i].olho.substring(0, 100) + "...";
				} else {
					eye = docs[i].olho;
				}


				tit = '';
				if (docs[i].titulo.length > 200) {
					tit = docs[i].titulo.substring(0, 200) + "...";
				} else {
					tit = docs[i].titulo;
				}

				html += "			<div class='col-md-10'>";
				html += "				<a href='" + docs[i].url + "' title=''>";

				if (typeof docs[i].urlImgEmp_idCorteImagem !== "undefined" || docs[i].urlImgEmp_idCorteImagem !== "" || typeof docs[i].urlImgEmp_282x162 !== "undefined") {
                	html += "<img width='150' height='86' src='" + docs[i].urlImgEmp_282x162 + "'>";
	            }

				html += "					<h3>"+tit+"</h3>";
				html += "					<p>"+eye+"</p>";
				html += "				</a>";
				html += "			</div>";
			}

			html += "</a></li>";
         }


		//para nao aparecer mais a opcao quando nao houver mais noticias a serem exibidas


		var aux;
		aux = parseInt(page)*5;

       if( aux <  tammax ){
        //	document.getElementById("mostrarMais").style.display = "none";

			html += "<li id='mostrarMais' class='col-md-12 carregar-mais'>"
			html += "	<div class='row exibenoticia' id='exibenoticia'>"
			html += "		<a onclick='javascript:EJESA.BELIA00206.exibeMais()'>"
			html += "			<span>"
			html += "			</span>"
			html += "			MOSTAR MAIS NOTÍCIAS"
			html += "		</a>"
			html += "	</div>"
			html += "</li>"

        }

		$(".bxloader").css('display','none');

		document.getElementById('BELIA00206HL').insertAdjacentHTML("beforeend", html) ;

	};



	start = function () {

		var search;

		address = makeAddress(inicio);

		executeSearch(address);



	};

	exibeMais  = function (){

		document.getElementById('BELIA00206HL').insertAdjacentHTML("beforeend", "<div style='display:block; text-align:center'><img class='bxloader' src='http://s0.ejesa.ig.com.br/css/img/bx_loader.gif'></div>") ;

		page = page+1;

		$("#mostrarMais").remove();
		inicio = inicio + QTDNEWS;

		address = makeAddress(inicio);
		executeSearch(address);

	};

	//revelando API pública
	return {
		start: start,
		exibeMais: exibeMais
	};
}());
