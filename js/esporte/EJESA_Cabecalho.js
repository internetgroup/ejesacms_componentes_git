/*global console*/
/*global $*/
/*global jQuery*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.FUTEBOL.Cabecalho'); /** Dados do Campeonato **/

/** implementando módulo **/
EJESA.FUTEBOL.Cabecalho = (function () {
    'use strict';
    var start,
        url = 'http://odia.ig.com.br/includes/EJEHEA00806.shtml';


    start = function () {

        jQuery.ajax({
            url: url,
            Type: 'GET',
            success: function(d) {
                $('#cabecalho').html(d);
            }
        });

    };



    //revelando API pública
    return {
        start: start
    };

}());