/*global document: false */
/*global console*/
/*global $*/
/*global setInterval*/
/*global jQuery*/

var EJESA = EJESA || {},
    DOMINIO = 'http://futebol.statig.com.br';
    //DOMINIO = 'http://localhost/odia/futebol';

/** declarando os módulos **/
EJESA.namespace('EJESA.FUTEBOL.Narracao'); /** Narração do Jogo **/

/** implementando módulo **/
/** Tabela de Classificação do campeonato **/
EJESA.FUTEBOL.Narracao = (function () {
    'use strict';
    var start,
        renderiza,
        mountURL,
        url = '',
        job,
        delay = 30000;



    mountURL = function () {
        var param = EJESA.FUTEBOL.Util.querystring();
        url += DOMINIO;
        url += "/campeonatos/";
        url += param.idcampeonato;
        url += "/jogos/narracao-";
        url += param.idpartida;
        url += ".json?callback=renderiza&_=";
        url += +(new Date());
    };

    job = function() {

        mountURL();

        jQuery.ajax({
            url: url,
            dataType: 'jsonp',
            crossDomain: true,
            jsonp: false,
            jsonpCallback: 'narracaoJogoCampeonato',
            success: function(d) {
                renderiza(JSON.stringify(d));
            }
        });

    };

    start = function () {

        job();

        setInterval(job, delay);

    };

    renderiza = function (data) {

        var obj,
            itens,
            i,
            item,
            html_li = '';

        obj = JSON.parse(data);
        itens = obj.campeonato.narracoes.narracao;
        for (i = 0; i < itens.length; i += 1) {
            item = itens[i];
            switch (item.acao) {
            case "PenaltGol":
                html_li += '<li class="acao gol" ';
                break;
            case "Gol":
                html_li += '<li class="acao gol" ';
                break;
            case "Substituir":
                html_li += '<li class="acao substituicao" ';
                break;
            case "CAmarelo":
                html_li += '<li class="acao cartaoAmarelo" ';
                break;
            case "CVermelho":
                html_li += '<li class="acao cartaoVermelho" ';
                break;
            default:
                html_li += '<li class="acao" ';

            }

            html_li += 'id="narracao_';
            html_li += item.id;
            html_li += '" style="display: list-item;">';
            html_li += '<div class="tempo">';
            html_li += '<div class="minuto">';
            html_li += item.momento;
            html_li += '</div>';
            html_li += '<div class="periodo">';
            if (item.periodo === '5º Tempo') {
                html_li += 'Pênaltis';
            } else {
                html_li += item.periodo;
            }
            html_li += '</div>';
            html_li += '</div>';
            html_li += '<div class="assunto">';
            html_li += '<div class="time">';
            if (item.nomeEquipe !== undefined) {
                html_li += item.nomeEquipe;
            } else {
                html_li += '';
            }
            html_li += '</div>';
            html_li += '<div class="texto">';
            html_li +=  item.descricao;
            html_li += '</div>';
            html_li += '<div class="ico">';
            if (item.idEquipe !== undefined) {
                html_li += '<img src="http://i0.statig.com.br/esporte/tempo-real/futebol/escudos/';
                html_li += item.idEquipe;
                html_li += '.png">';
            } else {
                html_li += '';
            }
            html_li += '</div>';
            html_li += '</div>';
            html_li += '</li>';
            $('#narracao_ao_vivo').html(html_li);
        }
    };



    //revelando API pública
    return {
        start: start
    };

}());