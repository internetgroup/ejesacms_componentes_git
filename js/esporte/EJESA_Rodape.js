/*global console*/
/*global $*/
/*global jQuery*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.FUTEBOL.Rodape'); /** Dados do Campeonato **/

/** implementando módulo **/
EJESA.FUTEBOL.Rodape = (function () {
    'use strict';
    var start,
        url = 'http://odia.ig.com.br/includes/EJEFOE00101.shtml';


    start = function () {

        jQuery.ajax({
            url: url,
            Type: 'GET',
            success: function(d) {
                $('#rodape').html(d);
            }
        });

    };



    //revelando API pública
    return {
        start: start
    };

}());