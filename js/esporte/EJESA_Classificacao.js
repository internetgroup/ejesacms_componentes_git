/*global document: false */
/*global console*/
/*global $*/
/*global setInterval*/
/*global jQuery*/

var EJESA = EJESA || {},
    DOMINIO = 'http://services.staging.e-odia.com.br/api/est3';
    //DOMINIO = 'http://localhost/odia/futebol';
    // http://futebol.statig.com.br/campeonatos/751/classificacao.json

/** declarando os módulos **/
EJESA.namespace('EJESA.FUTEBOL.Classificacao'); /** Narração do Jogo **/

/** implementando módulo **/
/** Tabela de Classificação do campeonato **/
EJESA.FUTEBOL.Classificacao = (function () {
    'use strict';
    var start, VV
        renderiza,
        mountURL,
        url = '',
        job,
        delay = 30000;


    mountURL = function () {
        var paramID = $('#rodadaatual').val();
        url += DOMINIO;
        url += "/campeonato/";
        url += paramID;
        url += "/classificacaogeral.json?";
        url += +(new Date());
    };

    job = function() {

        mountURL();

        // mudar para json

        jQuery.ajax({
            url: url,
            dataType: 'json',
            success: function(d) {
                renderiza(JSON.stringify(d));
            }
        });

    };

    start = function () {

        job();

        setInterval(job, delay);

    };

    renderiza = function (data, rodada) {

        var obj,
            equipes,
            equipe,
            info,
            time,
            html = '',
            i = 0;

        obj = JSON.parse(data);
        equipes = obj.campeonato.classificacoes.Guanabara.GRUPO_UNICO.equipes;


        html += '<table border="0" cellpadding="0" cellspacing="0" class="tabJogos" id="tabJogos">';
        html += '<tbody><tr>';
        html += '<th></th>';
        html += '<th class="times">Times</th>';
        html += '<th>PG</th>';
        html += '<th>J</th>';
        html += '<th>V</th>';
        html += '<th>E</th>';
        html += '<th>D</th>';
        html += '<th>GP</th>';
        html += '<th>GC</th>';
        html += '<th>SG</th>';
        html += '</tr>';


        for (equipe in equipes) {
            if (equipes.hasOwnProperty(equipe)) {
                info = equipes[equipe];


                if (i < 4) {
                    html += '<tr class="primeiros">';
                }

                if (i > 15) {
                    html += '<tr class="ultimos">';
                }

                html += '<td><strong>' + info.posicao + '</strong></td>';
                html += '<td class="times"><strong>' + info.nome + '</strong></td>';
                html += '<td class="cinza"><strong>' + info.pontosGanhos + '</strong></td>';
                html += '<td>' + info.jogos + '</td>';
                html += '<td class="cinza">' + info.vitorias + '</td>';
                html += '<td>' + info.empates + '</td>';
                html += '<td class="cinza">' + info.derrotas + '</td>';
                html += '<td>' + info.golsPro + '</td>';
                html += '<td class="cinza">' + info.golsContra + '</td>';
                html += '<td>' + info.saldoGols + '</td>';
                html += '</tr>';

            }
            i += 1;
        }

        html += '</tbody>';
        html += '</table>';

        $("#classificacao").html(html);

    };



    //revelando API pública
    return {
        start: start
    };

}());
