/*global console*/
/*global $*/
/*global setInterval*/
/*global jQuery*/

var EJESA = EJESA || {},
    DOMINIO = 'http://futebol.statig.com.br';
    IMGPATH = 'http://s0.ejesa.ig.com.br/img/odia/brasileirao2013/';
    //IMGPATH = 'http://localhost/odia/futebol/img/';
    //DOMINIO = 'http://localhost/odia/futebol';

/** declarando os módulos **/
EJESA.namespace('EJESA.FUTEBOL.Escalacao'); /** Narração do Jogo **/

/** implementando módulo **/
/** Escalação dos times no jogo **/
EJESA.FUTEBOL.Escalacao = (function () {
    'use strict';
    var start,
        renderiza,
        mountURL,
        url = '',
        mountTeam,
        mountPenaltyShot,
        mountPlayer,
        job,
        delay = 30000;

    mountURL = function () {
        var param = EJESA.FUTEBOL.Util.querystring();
        url += DOMINIO;
        url += "/campeonatos/";
        url += param.idcampeonato;
        url += "/jogos/ao-vivo-min-";
        url += param.idpartida;
        url += ".json?callback=renderiza&_=";
        url += +(new Date());
    };

    job = function () {

        mountURL();

        jQuery.ajax({
            url: url,
            dataType: 'jsonp',
            crossDomain: true,
            jsonp: false,
            jsonpCallback: 'aoVivoJogoCampeonatoMin',
            success: function(d) {
                renderiza(JSON.stringify(d));
            }
        });

    };


    start = function () {

        job();

        setInterval(job, delay);

    };

    mountPlayer = function (player, substitution) {

        var html_li = '',
            tshirt,
            yellowCard,
            redCard,
            gols,
            y,
            substitute;


        substitute = player.substituto;

        tshirt = player.camisa;
        tshirt = tshirt < 10 ? '0' + tshirt : tshirt;

        html_li += '<li id="jogador_';
        html_li += player.id;
        html_li += '">';
        if (substitute === undefined) {
            if (substitution) {
                html_li += '<div class="jogador entra">';
            } else {
                html_li += '<div class="jogador">';
            }
        } else {
            html_li += '<div class="jogador sai">';
        }
        html_li += '<div class="pos">';
        html_li += player.posicao;
        html_li += '</div>';
        html_li += '<div class="nome">';
        if (player.posicao !== 'tec') {
            html_li += tshirt;
            html_li += ' - ';
        }
        html_li += player.nome;
        html_li += '</div>';

        if (player.posicao !== 'tec') {
            //cartões e gols
            yellowCard = player.cartao_amarelo;
            redCard = player.cartao_vermelho;
            gols = player.gols;
            if (yellowCard > 0 || redCard > 0 || gols.length > 0) {
                html_li += '<div class="acao">';
                if (yellowCard > 0) {
                    html_li += '<div class="amarelo"></div>';
                }
                if (redCard > 0) {
                    html_li += '<div class="vermelho"></div>';
                }
                if (gols.length > 0) {
                    for (y = 0; y < gols.length; y += 1) {
                        html_li += '<div class="gol"></div>';
                    }
                }
                html_li += '</div>';
            }

        }

        html_li += '</div>';
        html_li += '</li>';

        if (substitute !== undefined) {
            //substituição
            html_li += mountPlayer(substitute, true);
        }

        return html_li;

    };

    mountTeam = function (team) {
        var html_team,
            i;

        html_team = '';
        html_team += '<h4>Escalação</h4>';
        html_team += '<ul>';
        for (i = 0; i < team.length; i += 1) {
            html_team += mountPlayer(team[i], false);
        }
        html_team += '</ul>';

        return html_team;

    };

    mountPenaltyShot = function (penalties, pos) {

        var i,
            html_penalty;

        if (penalties.length > 0) {
            html_penalty = '<div class="cobrancas_' + pos + '">';
            for (i = 0; i < penalties.length; i += 1) {
                if (penalties[i].convertido === 'sim') {
                    html_penalty += '<div class="batida_gol"></div>';
                } else {
                    html_penalty += '<div class="batida_erro"></div>';
                }
            }
            html_penalty += '</div>';
        }

        return html_penalty;

    };

    renderiza = function (data) {
        var obj,
            mandante,
            visitante,
            campeonato,
            estadio,
            rodada,
            equipe_1,
            equipe_2,
            nome_eqp_1,
            gols_eqp_1,
            nome_eqp_2,
            gols_eqp_2,
            penal_eqp_1,
            penal_eqp_2,
            cobrancas_1,
            cobrancas_2,
            gols_pen_1,
            gols_pen_2,
            escudoA,
            escudoB;

        obj = JSON.parse(data);


        //dados do jogo
        campeonato = obj.campeonato;
        var campeonato2 = campeonato.split('-')
        //console.log(campeonato2[0])
        estadio = obj.estadio;
        rodada =  obj.rodada +'ª Rodada ' + '<br />' + campeonato2[0];

        //pegando equipes
        equipe_1 = obj.equipes[0];
        equipe_2 = obj.equipes[1];


        //placar
        nome_eqp_1 = equipe_1.nome;
        gols_eqp_1 = equipe_1.gols;
        nome_eqp_2 = equipe_2.nome;
        gols_eqp_2 = equipe_2.gols;

        //penaltis
        penal_eqp_1 = equipe_1.penaltis.cobrancas;
        penal_eqp_2 = equipe_2.penaltis.cobrancas;

        if (penal_eqp_1.length > 0) {
            cobrancas_1 = mountPenaltyShot(penal_eqp_1, 1);
            gols_pen_1 = equipe_1.penaltis.convertidos;
        }
        if (penal_eqp_2.length > 0) {
            cobrancas_2 = mountPenaltyShot(penal_eqp_2, 2);
            gols_pen_2 = equipe_2.penaltis.convertidos;
        }
        //escalações
        mandante = mountTeam(equipe_1.escalacao);
        visitante = mountTeam(equipe_2.escalacao);

        //escudos
        escudoA = '<img src="' + IMGPATH + 'equipe_' + equipe_1.id + '_h86.png" height="86">';
        escudoB = '<img src="' + IMGPATH + 'equipe_' + equipe_2.id + '_h86.png" height="86">';

        //renderizando
        $('#estadio').html(estadio);
        $('#rodada').html(rodada);
        $('#timeAnome').html(nome_eqp_1);
        $('#timeAgols').html(gols_eqp_1);
        $('#timeBnome').html(nome_eqp_2);
        $('#timeBgols').html(gols_eqp_2);
        $('#timeA').html(mandante);
        $('#timeB').html(visitante);
        $('#escudoA').html(escudoA);
        $('#escudoB').html(escudoB);

        $('.timeAnome2').html(nome_eqp_1);
        $('.timeBnome2').html(nome_eqp_2);

        if (penal_eqp_1.length > 0 && penal_eqp_2.length > 0) {
            $('#penaltisA').html(cobrancas_1);
            $('#penaltisB').html(cobrancas_2);
            $('#gols_pen_1').html(gols_pen_1);
            $('#gols_pen_2').html(gols_pen_2);

            $('#tempoRegulamentar').css("display", "block");
            $('#disputaPenaltis').css("display", "block");
        }
    };

    //revelando API pública
    return {
        start: start
    };

}());
