/* vertical sorter plugin */
IBTX.onReady(function(){
	;(function ( $, window, document, undefined ) {
		var pluginName = "verticalSorter"
			,defaults = {
				enabled: true
				,speed: 500//speed of the animations
				,checkFocus: false// only animate when window has focus
				,enableDecreaseSize: false//only allows container to increase
				,sortingFunction: function(itemA, itemB){
					var valA = parseInt($(itemA).attr('data-sort'))
						,valB = parseInt($(itemB).attr('data-sort'))
					;
					return (valA - valB);
				}
			}
			,queueName = 'verticalSorterQ'
		;
		
		function Plugin( element, options ){
			this.element = element;
			this.arrElms = [];
			this.elmsHeight = [];
			this.sorting = false;
			this.options = defaults;
			
			this.extendOptions(options);
			this.init();
		}

		Plugin.prototype = {
			extendOptions: function(options){
				this.options = $.extend( {}, this.options, options );
			}
			,init: function(){
				var that = this
					,$elm = $(this.element)
					,$items = $elm.children()
					,i=0
					,$item = {}
					,t = 0//top
					,h = 0//height
					,sumH = 0//total height
				;
				if(that.sorting){ return; }//only one sorting at a time
				if( ! that.options.enabled ){ return; }//It's disabled... 
				that.sorting = true;
				
				$elm.css({position:'static',height:'auto',display:'block'});
				$items.each(function(i,el){
					$item = $(el);
					if($item.css('position')!='absolute'){
						$item.css({position:'absolute',top:sumH})
					}
					h = $item.outerHeight(true);//true: includeMargin
					$.data(el, 'height', h);
					sumH += h;
					that.arrElms.push(el);
				});
				// resizing container
				if(	that.options.enableDecreaseSize  === false
					&& sumH<=that.elmsHeight
				){
					sumH = that.elmsHeight;//only allows the increase in size
				}
				$elm.css({
					position:'relative'
					,top:0
					,left:0
					,height: sumH 
					,margin:0
					,padding:0
					,overflow: 'hidden'
					,display:'block'
				});
				that.elmsHeight = sumH;
				
				that.arrElms.sort(that.options.sortingFunction);
				
				sumH = 0; // calculate next position
				for(i=0; i<that.arrElms.length; i++){
					$item = $(that.arrElms[i]);
					h = $item.data('height');
					t = sumH;
					sumH += h;
					$item.css({'z-index':90-i}).animate(
						{top:t}
						,{
							duration: that.options.speed
							,queue: queueName
						}
					);
				}
				$items.dequeue(queueName);//start queue
				
				setTimeout(function(){
					that.clear();
					that.sorting = false;//we should have ended by now
				}, that.options.speed+100);// we added a litle delay just in case
			}
			,clear: function(){
				var that = this
					,$elm = $(this.element)
					,$items = $elm.children()
				;
				$items.stop(queueName, true);//stop all animations related to sorting
				that.arrElms = [];
			}
		};

		$.fn[pluginName] = function ( options ) {
			var instance = {};
			return this.each(function () {
				instance = $.data(this, "plugin_" + pluginName);
				if (!instance) {
					instance = new Plugin( this, options );
					$.data(this, "plugin_" + pluginName, instance);
				}else{
					if(options){
						instance.extendOptions(options);
					}
					instance.init();
				}
			});
		};
	})( IBTX.jQuery, window, document );
});
/* custom configs */
IBTX.onReady(function(){
	var $ = IBTX.jQuery
		,myTpl = ''
	;
	myTpl = ''
		+'<li class="rt-top-item" data-rt-top-item-rank="{{rank}}" data-rt-top-item-total="{{total}}" data-rt-top-item-id="{{pageId}}">'
		+'    <div class="headline-list">'
		+'        <div class="headline-position rank">-</div>'
		+'        <div class="headline-title" style="margin-top:5px"><a href=""><a href="{{meta.url}}">{{{meta.title}}}</a></div>'
		+'        <div class="headline-time" style="font-weight:normal;text-transform:lowercase;margin-top:2px"><span class="rt-top-item-total">{{total}}</span> pessoas estão lendo esta notícia</div>'
		+'    </div>'
		+'</li>'
	;
	// hooks
	IBTX.App.top.find(".rt-top").each(function (item) {
		var topContainer = $('.rt-top:first')
			,sortTimer = 0
			,sortTtl = 800
			,numItems = 5
			,newsItems = {}//hack. prevent duplicates until we fix the setTimeout bug
		;
		topContainer.hover(//disable sorting animation on mouse over
			function(){
				topContainer.verticalSorter({enabled: false});
			},
			function(){
				topContainer.verticalSorter({enabled: true});
			}
		);
		//enable vertical sorting
		topContainer.verticalSorter({
			speed: 1000 //speed of the animations
			,sortingFunction: function(itemA, itemB){//custom sorting function
				var valA = parseInt($(itemA).find('.rank').text(), 10)
					,valB = parseInt($(itemB).find('.rank').text(), 10)
				;
				return (valA - valB);//sort ascending
			},
			enableDecreaseSize: true
		});
		
		(function updateSort(){
			var sorted = false;
			topContainer.find('.rt-top-item').each(function(){
				var newRank = parseInt($(this).attr('data-rt-top-item-rank'), 10)+1 || '-'
					,oldRank = parseInt($(this).find('.rank').text(), 10) || '-'
				;
				if(newRank !== oldRank){
					$(this).find('.rank').text(newRank);
					sorted = true;
				}
			});
			topContainer.verticalSorter();
			sortTtl = sorted ? 4000 : 1500;
			setTimeout(updateSort, sortTtl);
		})();
		
		item.setConfiguration({
			filter: "type:article",
			numItems: numItems,
			itemTpl: myTpl
		});
		
		item.hooks.set('preRender', function (data, cb) {
			data.meta.title = data.meta.title.replace(/- O Dia$/, '');
			cb(null, data);
		});
		
		item.hooks.set('render', function (newNode, data, cb) {
			var $item;
			
			if(data.action === 'update'){
				$item = topContainer.find('[data-rt-top-item-id='+data.pageId+']');
				$item.attr('data-rt-top-item-rank', data.rank);
				$item.attr('data-rt-top-item-total', data.total);
				$item.attr('data-rt-top-diffs-counter', $(newNode).attr('data-rt-top-diffs-counter'));
				$item.find('.rt-top-item-rank').html(data.rank);
				$item.find('.rt-top-item-total').html(data.total);
				if(data.rank === numItems-1){
					$item.addClass('last').siblings('.rt-top-item').removeClass('last');
				}
				if(newsItems[data.pageId]){ // update items data
					newsItems[data.pageId] = data; 
				}
				cb(null, newNode, data).preventDefaults();
			}else{//new item
				if(newsItems[data.pageId]){ //hack: prevent duplicates
					cb(null, newNode, data).preventDefaults();
					return; 
				}
				$(newNode).find('.rank').html(data.rank+1);
				clearTimeout(sortTimer);
				sortTimer = setTimeout(function(){
					topContainer.verticalSorter();
				},100);
				newsItems[data.pageId] = data;
				cb(null, newNode, data);
			}
		});
		item.hooks.set('renderDelete', function (newNode, data, cb) {
			delete newsItems[data.pageId];
			cb(null, newNode, data);
		});
	});
});