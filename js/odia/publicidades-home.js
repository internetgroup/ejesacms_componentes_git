var secaoURL = '', 
    pbSecao = '', 
    nomeSecao, 
    contemIntervencao, 
    contemSuperVideo, 
    valor, 
    retornoIntervencao, 
    timerIntervencaoRefresh, 
    loadIntervencao = 0,
    adBotao, 
    adTopClick, 
    adDhtml, 
    adIntervencao, 
    adRodapeRetraido, 
    adRodapeExpandido, 
    adSuperVideo, 
    dfpSite = 'odia', 
    dfpId = '1009826';

if (document.addEventListener("DOMContentLoaded", function() {

        if(window.location.href.search("edicao.html") == -1){


        var dfpBlocosdeanuncio = [
            {dfpVar: 'adBotao',           dfpLabel: '_botao',        dfpSize: '[200, 90]',   dfpContainer: 'botao_1'},
            {dfpVar: 'adRetmedio1',       dfpLabel: '_retmedio1',    dfpSize: '[300, 250]',  dfpContainer: 'retangulo_medio_1'},
            {dfpVar: 'adRetmedio2',       dfpLabel: '_retmedio2',    dfpSize: '[300, 250]',  dfpContainer: 'retangulo_medio_2'},
            {dfpVar: 'adRetmedio3',       dfpLabel: '_retmedio3',    dfpSize: '[300, 250]',  dfpContainer: 'retangulo_medio_3'},
            {dfpVar: 'adSelo1',           dfpLabel: '_selo1',        dfpSize: '[300, 100]',  dfpContainer: 'selo_1'},
            {dfpVar: 'adSelo2',           dfpLabel: '_selo2',        dfpSize: '[300, 100]',  dfpContainer: 'selo_2'},
            {dfpVar: 'adSelo3',           dfpLabel: '_selo3',        dfpSize: '[300, 100]',  dfpContainer: 'selo_3'},
            {dfpVar: 'adSuperbanner1',    dfpLabel: '_superbanner1', dfpSize: '[728, 90]',   dfpContainer: 'superbanner_1'},
            {dfpVar: 'adSuperbanner2',    dfpLabel: '_superbanner2', dfpSize: '[728, 90]',   dfpContainer: 'superbanner_2'},
            {dfpVar: 'adTopClick',        dfpLabel: '_topclick',     dfpSize: '[940, 300]',  dfpContainer: 'topclick'},
            {dfpVar: 'adDhtml',           dfpLabel: '_dhtml',        dfpSize: '[400, 400]',  dfpContainer: 'pub-dhtml'},
            {dfpVar: 'adIntervencao',     dfpLabel: '_intervencao',  dfpSize: '[940, 465]',  dfpContainer: 'pub-intervencao'},
            {dfpVar: 'adRodapeRetraido',  dfpLabel: '_expand',       dfpSize: '[940, 75]',   dfpContainer: 'retraido'},
            {dfpVar: 'adRodapeExpandido', dfpLabel: '_expand',       dfpSize: '[940, 300]',  dfpContainer: 'expandido'},
            {dfpVar: 'adSuperVideo',      dfpLabel: '_video',        dfpSize: '[1, 1]',      dfpContainer: 'video-area'}
        ];


        googletag.cmd.push(function() {

            googletag.pubads().enableSingleRequest();
            googletag.pubads().enableAsyncRendering();
            googletag.pubads().disableInitialLoad();
            googletag.pubads().collapseEmptyDivs();
            googletag.enableServices();

            
            adBotao             = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_botao',        [200, 90],  'botao_1').addService(googletag.pubads());
            adRetmedio1         = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_retmedio1',    [300, 250], 'retangulo_medio_1').addService(googletag.pubads());
            adRetmedio2         = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_retmedio2',    [300, 250], 'retangulo_medio_2').addService(googletag.pubads());
            adRetmedio3         = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_retmedio3',    [300, 250], 'retangulo_medio_3').addService(googletag.pubads());
            adRetmedio4         = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_retmedio4',    [300, 250], 'retangulo_medio_4').addService(googletag.pubads());
            adRetmedio5         = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_retmedio5',    [300, 250], 'retangulo_medio_5').addService(googletag.pubads());
            adSelo1             = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_selo1',        [300, 100], 'selo_1').addService(googletag.pubads());
            adSelo2             = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_selo2',        [300, 100], 'selo_2').addService(googletag.pubads());
            adSelo3             = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_selo3',        [300, 100], 'selo_3').addService(googletag.pubads());
            adSuperbanner1      = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_superbanner1', [728, 90],  'superbanner_1').addService(googletag.pubads());
            adSuperbanner2      = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_superbanner2', [728, 90],  'superbanner_2').addService(googletag.pubads());

            adTopClick          = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_topclick',     [940, 300], 'topclick').addService(googletag.pubads());        
            adDhtml             = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_dhtml',        [400, 400], 'pub-dhtml').addService(googletag.pubads());
            adIntervencao       = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_intervencao',  [940, 465], 'pub-intervencao').addService(googletag.pubads());
            adRodapeRetraido    = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_expand',       [940, 75],  'retraido').addService(googletag.pubads());
            adRodapeExpandido   = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_expand',       [940, 300], 'expandido').addService(googletag.pubads());
            adSuperVideo        = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_home_video',        [1, 1],     'video-area').addService(googletag.pubads());
            


            googletag.pubads().enableSingleRequest();
            googletag.enableServices();

            googletag.display('botao_1');
            googletag.display('retangulo_medio_1');
            googletag.display('retangulo_medio_2');
            googletag.display('retangulo_medio_3');
            googletag.display('retangulo_medio_4');
            googletag.display('retangulo_medio_5');
            googletag.display('selo_1');
            googletag.display('selo_2');
            googletag.display('superbanner_1');
            googletag.display('superbanner_2');
            googletag.display('topclick');
            googletag.display('pub-dhtml');
            googletag.display('pub-intervencao');
            googletag.display('retraido');
            googletag.display('expandido');
            googletag.display('video-area');

            googletag.pubads().addEventListener('slotRenderEnded', function(event) {

                if (event.slot === adTopClick) {

                    var contemTopClick = !event.isEmpty;
                    if(contemTopClick === false) {
                        verificarIntervencao();
                    }
                } 

                function verificarIntervencao() {
                    /* console.log('%c verificando intervencao ...', 'color:orange;'); */
                    setTimeout(function() {
                        refreshIntervencao();
                        setTimeout(function() {
                            timerIntervencaoRefresh = setInterval(refreshIntervencao, 100);
                            EJESA.WSPublicidades.adicionarIntervencao();
                        },500);
                    }, 500);
                }

                if (event.slot === adDhtml) {
                    var contemDhtml = !event.isEmpty;
                    if(contemDhtml === true) {
                        EJESA.WSPublicidades.adicionarDHTML();
                    } 
                }

                if (event.slot === adRodapeRetraido || event.slot === adRodapeExpandido) {
                    var contemRodape = !event.isEmpty;
                    if(contemRodape === true) {
                        EJESA.WSPublicidades.addRodapeExpansivo();
                    } 
                } 

                if (event.slot === adSuperVideo) {
                    var contemSuperVideo = !event.isEmpty;
                    if(contemSuperVideo === true) {
                        setTimeout(function() {
                            EJESA.WSPublicidades.adicionarSuperAdVideo();
                        },1000);
                    } 
                } 

            });

        });

            googletag.pubads().refresh([adBotao]);
            googletag.pubads().refresh([adRetmedio1]);
            googletag.pubads().refresh([adRetmedio2]);
            googletag.pubads().refresh([adRetmedio3]);
            googletag.pubads().refresh([adRetmedio4]);
            googletag.pubads().refresh([adRetmedio5]);
            googletag.pubads().refresh([adSelo1]);
            googletag.pubads().refresh([adSelo2]);
            googletag.pubads().refresh([adSelo3]);
            googletag.pubads().refresh([adSuperbanner1]);
            googletag.pubads().refresh([adSuperbanner2]);

            googletag.pubads().refresh([adDhtml]);
            googletag.pubads().refresh([adTopClick]);
            googletag.pubads().refresh([adRodapeRetraido]);
            googletag.pubads().refresh([adRodapeExpandido]);
            googletag.pubads().refresh([adSuperVideo]);

    }

}));


function refreshIntervencao() {
    googletag.cmd.push(function() {
        googletag.pubads().addEventListener('slotRenderEnded', function(event) {
            if (event.slot === adIntervencao) {
                contemIntervencao = !event.isEmpty;
                if(contemIntervencao === true) {
                     intervencao = 1;
                    clearInterval(timerIntervencaoRefresh);
                    retornoIntervencao = retornoDFP(contemIntervencao)
                }
            }
        });
        if(loadIntervencao === 0) {
            loadIntervencao = 1;
            googletag.pubads().refresh([adIntervencao]);
        }
    });
}

function retornoDFP(contem) {
    if(contem === false) {
        valor = false;
    } else {
        valor = true;
    }
    return valor;
}