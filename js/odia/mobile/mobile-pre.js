
var ODIAMOBILE = ODIAMOBILE || {};

ODIAMOBILE.criaNS = function (namespace) {
	var nsparts = namespace.split(".");
	var parent = ODIAMOBILE;
	if (nsparts[0] === "ODIAMOBILE") {
		nsparts = nsparts.slice(1);
	}
	for (var i = 0; i < nsparts.length; i++) {
		var partname = nsparts[i];
		if (typeof parent[partname] === "undefined") {
			parent[partname] = {};
		}
		parent = parent[partname];
	}
	return parent;
};

ODIAMOBILE.criaNS("ODIAMOBILE.LOADIFRAMES");
ODIAMOBILE.LOADIFRAMES = (function () {
	setIframe = function (secaoArq,urlArq,alturaArq,nomeArq) {
		var link = 'http://odia.ig.com.br/'+secaoArq+'/'+urlArq+'.html.high';
		var iframe = document.createElement('iframe');
		iframe.frameBorder=0;
		iframe.width='100%';
		iframe.height=parseInt(alturaArq)+'px';
		iframe.id=secaoArq;
		iframe.setAttribute('scrolling', 'no');
		iframe.setAttribute('src', link);
		iframe.setAttribute('id', urlArq);

		var alvo_id = document.getElementById('add-editorias');
		alvo_id.parentNode.insertBefore(iframe, alvo_id);

		return setIframe;
	}
	return {
		setIframe: setIframe
	};
}());

ODIAMOBILE.criaNS("ODIAMOBILE.FAVORITOS");
ODIAMOBILE.FAVORITOS = (function () {
	var init = function () {
		var cookie = lerCookie('odiaFavorito');
		if (cookie === null) {
        	criaSmartBanner();
        	var fechar = document.getElementById('close');
		    fechar.addEventListener('click', function(e) {
				e.preventDefault()
				criaCookie('odiaFavorito','cookieODIA', 30);
			});
        }
	};

	var sistema = function () {
		var userAgent = navigator.userAgent || navigator.vendor || window.opera;
        var sistema = "";
        if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
            sistema = 'iOS';
        } else if (userAgent.match(/Android/i)) {
            sistema = 'Android';
        } else {
            sistema = 'desconhecido';
        }
        return sistema;
	};

	var criaCookie = function (nome, valor, dias) {
		if (dias) {
            var data = new Date();
            data.setTime(data.getTime() + (dias * 24 * 60 * 60 * 1000));
            var expirar = "; expires=" + data.toGMTString();
        } else var expirar = "";
        document.cookie = nome + "=" + valor + expirar + "; path=/";
        document.getElementById("favorito").style.display = "none";
	};

	var lerCookie = function (nome) {
		var nameEQ = nome + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
	};

	var limpaCookie = function (nome) {
		createCookie(nome, "", -1);
	};

	var criaSmartBanner = function () {
		document.getElementById("favorito").style.display = "block";

		var span = document.createElement('span');
		var htmlBotao = '<a href="#" id="close">X</a>';
	    span.innerHTML = htmlBotao;
	    span.setAttribute("class", "close");
	    document.getElementById("favorito").appendChild(span);

	    var logo = document.createElement('span');
	    logo.setAttribute("class", "logo-dia-favorito");
	    document.getElementById("favorito").appendChild(logo);

	    var texto = "";

	    if(sistema() === 'iOS') {

	    	texto = '<strong>Crie um atalho de O DIA no seu aparelho. </strong><span>Toque <img src="http://s0.ejesa.ig.com.br/img/odia/mobile/icone_ios.png"> e selecione <strong>Adicionar √  tela de in√≠cio.</strong></span>';
	    	
	    } else {
	    	
	    	texto = '<strong>Crie um atalho de O DIA no seu aparelho. </strong><span>Acesse o <strong>Menu</strong> do navegador e clique em <strong>Adicionar √  tela inicial.</strong></span>';

	    }
	   
		var informacao = document.createElement('div');
	    informacao.setAttribute("class", "info no-button");
	    informacao.innerHTML = texto;
	    document.getElementById("favorito").appendChild(informacao);

	};

	return {
		init: init
	};

}());


ODIAMOBILE.criaNS("ODIAMOBILE.PUBLICIDADE");
ODIAMOBILE.PUBLICIDADE = (function () {
	'use strict';
}());

ODIAMOBILE.criaNS("ODIAMOBILE.UTEIS");
ODIAMOBILE.UTEIS = (function () {

	var ws_pecas = {};

	var getMetaName = function (name) {
		var i, x, y, att, m = document.getElementsByTagName('meta');
		for (i = 0; i < m.length; i = i + 1) {
			x = m[i].attributes;
			for (y = 0; y < x.length; y = y + 1) {
				att = x[y];
				if (att.name === 'name' && att.value === name) {
					return m[i].content;
				}
			}
		}
		return getMetaName;
	}

	var getLinkRel = function (rel) {
		var i, x, y, att, m = document.getElementsByTagName('link');
		for (i = 0; i < m.length; i = i + 1) {
			x = m[i].attributes;
			for (y = 0; y < x.length; y = y + 1) {
				att = x[y];
				if (att.name === 'rel' && att.value === rel) {
					return m[i].content;
				}
			}
		}
		return getLinkRel;
	}

	var autoResize = function (id) {
	    var newheight;
	    var newwidth;

	    if(document.getElementById){
	        newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
	        newwidth=document.getElementById(id).contentWindow.document .body.scrollWidth;
	    }

	    document.getElementById(id).height= (newheight) + "px";
	    document.getElementById(id).width= (newwidth) + "px";
	}

	var webspectatorInsertPub = function (pub_position) {
		if (ws_pecas[pub_position]) {
			return ws_pecas[pub_position].replace(/\s+/g, '');
		}
		return webspectatorInsertPub;
	}

	var webspectator = function (){

		/* pubs-web-spectator */
		var webspectator_pubs = document.getElementsByName("odia-OAS-query")[0].getAttribute("content") || "",
			webspectator_pubs = webspectator_pubs.toLowerCase();

		if (webspectator_pubs.length > 0) {
		   var ws_pubs = webspectator_pubs.split(",");
		   for (p in ws_pubs) {
			   if (typeof ws_pubs[p] === "string") {
					var peca = ws_pubs[p].split(":"),
						peca_key = peca[0],
						peca_val = peca[1];
						ws_pecas[peca_key] = peca_val;
			   }
		   }
		}

		return webspectator;

	}

	var fixabarra = function () {
		var inicio = 0;
		var submenuItens = document.getElementsByClassName('segundo-nivel'),
		            l = submenuItens.length, i;
	    if(window.pageYOffset >= 32) {
	    	if(inicio == 0) {
		   		inicio = 1;
	   			document.getElementById("banner_1").classList.add('pub-fixa');
	   			document.getElementById("header").classList.add('odia-fixa');
	   			document.getElementById("menu").classList.add('menu-fixa');
				document.getElementsByTagName("BODY")[0].classList.add('espaco-body');

		        for( i=0; i<l; i++) {
		        	submenuItens[i].classList.add('menu-fixa');
		        }
	    	}
	    } else if(window.pageYOffset < 32) {
	    	inicio = 0;
	    	document.getElementById("banner_1").classList.remove('pub-fixa');
	    	document.getElementById("header").classList.remove('odia-fixa');
	    	document.getElementById("menu").classList.remove('menu-fixa');
		document.getElementsByTagName("BODY")[0].classList.remove('espaco-body');

	        for( i=0; i<l; i++) {
	        	submenuItens[i].classList.remove('menu-fixa');
	        }
	    }
	}

	var addListener = function (element, type, callback) {
		if (element.addEventListener) element.addEventListener(type, callback);
		else if (element.attachEvent) element.attachEvent('on' + type, callback);
	}

	var eventoGa = function (category, action, label, value) {
		if(getMetaName('odia-iframe-home') != 'true'){
			ga('send', 'event', category, action, label, value);
		}
	}

	var getURLParameter = function (parametro) {
		return decodeURIComponent((new RegExp('[?|&]' + parametro + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
	};

	return {
		getMetaName: getMetaName,
		getLinkRel: getLinkRel,
		webspectator: webspectator,
		webspectatorInsertPub: webspectatorInsertPub,
		fixabarra: fixabarra,
		autoResize: autoResize,
		eventoGa: eventoGa,
		getURLParameter: getURLParameter
	};

}());

ODIAMOBILE.criaNS("ODIAMOBILE.CONTEUDO");
ODIAMOBILE.CONTEUDO = (function () {
	var QTDNEWS = 5,
		SITE = 'odia',
		SOLR = 'http://odia.ig.com.br/_indice/noticias/select?',
		FORMAT = 'json',
		start,
		renderiza,
		page = 1,
		pageIdentify,
		makeAddress,
		query,
		executeSearch,
		pageMarked,
		nextPage,
		prevPage,
		resetPagination,
		executeAjax,
		renderizaCapa,
		renderizaPassafoto,
		paginacaoNumero, 
		section_id = ODIAMOBILE.UTEIS.getMetaName('odia-section-id'),
		section_name = ODIAMOBILE.UTEIS.getMetaName('odia-section-name');

	ajax = function () {
		var xhr;

        if (window.XMLHttpRequest) {
          xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
		return xhr;
	};

	makeAddress = function (start) {

		var address = SOLR;

		address += 'start=';
		address += start;
		address += '&size=';
		address += QTDNEWS;
		address += '&site=';
		address += SITE;
		if(section_name != 'Home'){
			address += '&secoes_EH=';
			address += section_id;
		}
		address += '&wt=';
		address += FORMAT;

		return address;
	};

	executeSearch = function (address,container) {

		var i, xhr;

		xhr = ajax();

		xhr.onreadystatechange = function () {
			if (xhr.readyState !== 4) {
				return false;
			}
			if (xhr.status !== 200) {
				console.log("Error, status code: " + xhr.status);
				return false;
			}
			if(xhr.readyState === 4 && xhr.status === 200) {
				renderiza(xhr.responseText, container);
			}
		};

		xhr.open("GET", address, true);
		xhr.send("");
	};

	executeAjax = function (protocolo,address,tipo) {

		var i, xhr;

		xhr = ajax();

		xhr.onreadystatechange = function () {
			if (xhr.readyState !== 4) {
				return false;
			}
			if (xhr.status !== 200) {
				console.log("Error, status code: " + xhr.status);
				return false;
			}
			if(xhr.readyState === 4 && xhr.status === 200) {
				if(tipo=="capa"){
					renderizaCapa(xhr.responseText);
				}else if(tipo=="tvcoletivo"){
					renderizaTvColetivo(xhr.responseText);
				}else if(tipo=="busca"){
					renderizaBusca(xhr.responseText);
				}else{
					renderizaPassafoto(xhr.responseText);
				}
			}
		};

		xhr.open(protocolo, address, true);
		xhr.send("");
	};

	pageIdentify = function () {
		var start = (page - 1) * QTDNEWS;
		return start;
	};

	renderiza = function (dados, container) {

		var obj = JSON.parse(dados),
			docs,
			i,
			limit,
			date_news,
			sections,
			mysection,
			tit,
			html = '';

		docs = obj.response.docs;

		for (i = 0; i < docs.length; i = i + 1) {

			html += '<li>';
			html += '<a href="' + docs[i].url + '">';

			limit = 0;

			if (docs[i].urlImgEmp_idCorteImagem !== undefined) {
				html += '<img src="' + docs[i].urlImgEmp_80x60 + '">';
				limit = 90;
			} else {
				limit = 180;
			}

			tit = '';
			if (docs[i].titulo.length > limit) {
				tit = docs[i].titulo.substring(0, limit) + "...";
			} else {
				tit = docs[i].titulo;
			}


			html += '       <h4>'+ tit +'</h4>';
			html += '   </a>';

			html += '</li>';
		}

		document.getElementById(container).innerHTML = html;

		var element_btmaisnoticias =  document.getElementById('bt-mais-noticias');
		if (typeof(element_btmaisnoticias) != 'undefined' && element_btmaisnoticias != null){
			document.getElementById("bt-mais-noticias").classList.toggle('btn-ajax-loading');
		}

	};

	renderizaCapa = function (dados) {
		var obj = JSON.parse(dados);
		document.getElementById('containerCapa').innerHTML = '<img src="http://s0.ejesa.ig.com.br/img/odia/capa/'+obj.paper_date+'620x1045.jpg">';
	};

	zero_pad = function (num) {
		if (num < 10)
			num = '0' + num;

		return num;
	};

	solr_formatar_data = function (data) {
		var ext = /^(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)Z$/.exec(data);
		var utc = new Date();
		utc.setUTCFullYear(ext[1]);
		utc.setUTCMonth(parseInt(ext[2], 10) - 1);
		utc.setUTCDate(ext[3]);
		utc.setUTCHours(ext[4]);
		utc.setUTCMinutes(ext[5]);

		var mon = this.zero_pad(parseInt(utc.getMonth(), 10) + 1);
		var day = this.zero_pad(utc.getDate());
		var hour = this.zero_pad(utc.getHours());
		var min = this.zero_pad(utc.getMinutes());
		var year = utc.getFullYear();

		/*return hour + ":" + min + " - " + day + "/" + mon + "/" + year;*/
		return day+"/"+mon+"/"+year+" - "+hour+":"+min;
	};

	renderizaTvColetivo = function (dados) {
		var obj = JSON.parse(dados);
		document.getElementById('tvcoletivo-url').setAttribute("url", obj.url);
		document.getElementById('tvcoletivo-src').setAttribute("src", obj.src);
		document.getElementById('tvcoletivo-title').innerHTML = obj.title;
	};

	fazBusca = function (dados) {

		var queries = QueryString.q;

		console.log(queries);

		executeAjax('GET', 'http://odia.ig.com.br/_indice/noticias/select?start=0&size=5&site=*&comb_termos="'+ queries +'"&wt=json', 'busca');

	};

	renderizaBusca = function (dados) {
		
		var obj = JSON.parse(dados),
			docs,
			i,
			date_news,
			sections,
			mysection,
			tit,
			html = '';

		docs = obj.response.docs;

		for (i = 0; i < docs.length; i = i + 1) {
			html += '<li>';
            html += '	<a href="' + docs[i].url + '">';
            html += '		docs[i]secaoBreadcrumb - O Dia<br>';
            html += '		docs[i].url <img src="'+ docs[i].urlImgEmp_80x60 +'">';
            html += '		<h3>'+ docs[i].updatedDate +' ... '+ docs[i].titulo +'</h3>';
            html += '	</a>';
            html += '</li>';
		}
		document.getElementById('main').innerHTML = html;
		
	};

	fazBusca = function (limite) {

		var element_btmaisnoticias =  document.getElementById('bt-mais-noticias');
		if (typeof(element_btmaisnoticias) != 'undefined' && element_btmaisnoticias != null){
			document.getElementById("bt-mais-noticias").classList.toggle('btn-ajax-loading');
		}

		QTDNEWS = QTDNEWS + limite;
		var queries = ODIAMOBILE.UTEIS.getURLParameter('q');
		executeAjax('GET', 'http://odia.ig.com.br/_indice/noticias/select?start=0&size='+ QTDNEWS +'&site=*&comb_termos="'+ queries +'"&wt=json', 'busca');

	};

	submitBusca = function () {
		var q = document.getElementById('mobile-busca').value;
		window.open("/busca/index.html.high?q="+ q , "_top");
	};

	renderizaBusca = function (dados) {

		var obj = JSON.parse(dados),
			docs,
			i,
			date_news,
			sections,
			mysection,
			tit,
			html = '';

		docs = obj.response.docs;

		for (i = 0; i < docs.length; i = i + 1) {

			docs[i].updatedDate = solr_formatar_data(docs[i].updatedDate);

            html += '<li>';
            html += '	<a href="' + docs[i].url + '">';
            if(docs[i].urlImgEmp_80x60){
            	html += '		<img src="'+ docs[i].urlImgEmp_80x60 +'">';
        	}
            html += '		<span>'+ docs[i].updatedDate +'</span><br>';
            html += '		<h4>'+ docs[i].titulo +'</h4>';
            html += '		<p>'+ docs[i].olho +'</p>';
            html += '	</a>';
            html += '</li>';
		}
		document.getElementById('busca-lista').innerHTML = html;
		document.getElementById("bt-mais-noticias").style.display = 'block';
		document.getElementById("bt-mais-noticias").classList.toggle('btn-ajax-loading');
		
	};

	getScript = function (source, callback) {
	    var script = document.createElement('script');
	    var prior = document.getElementsByTagName('script')[0];
	    script.async = 1;
	    prior.parentNode.insertBefore(script, prior);

	    script.onload = script.onreadystatechange = function( _, isAbort ) {
	        if(isAbort || !script.readyState || /loaded|complete/.test(script.readyState) ) {
	            script.onload = script.onreadystatechange = null;
	            script = undefined;

	            if(!isAbort) { if(callback) callback(); }
	        }
	    };

	    script.src = source;
	};

	montaSwiper = function () {
		var swiper = new Swiper('.swiper-destaques', {
			pagination: '.swiper-pagination',
			paginationClickable: '.swiper-pagination',
			nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',
			spaceBetween: 0,
			loop: true,
			autoplay: 4000,
			/*onSlideChangeEnd: function (swiper) {
		       ODIAMOBILE.UTEIS.eventoGa('Mobile Carrossel', 'Slide', 'Passafoto topo', swiper.activeIndex);
		    },*/
		    onClick: function (swiper) {
		       ODIAMOBILE.UTEIS.eventoGa('Mobile Carrossel', 'Click', 'Passafoto topo', parseInt(swiper.activeIndex));
		    }
		});
	};

	montaSwiperColunistas = function () {
		    var mySwiper2 = new Swiper ('.swiper-colunistas', {
		      loop: true,
		      autoplay: 4000,
		      pagination: '.swiper-pagination2',
		      nextButton: '.swiper-button-next2',
		      prevButton: '.swiper-button-prev2',
		      paginationClickable: '.swiper-pagination2',
		      spaceBetween: 0
		    });
	};

	renderizaPassafoto = function (dados) {

		var obj = JSON.parse(dados),
			docs,
			i,
			date_news,
			sections,
			mysection,
			tit,
			html = '';

		docs = obj.noticias.passafoto.chamadas;

		for (i = 0; i < docs.length; i = i + 1) {
			html += '<div class="swiper-slide">';
            html += '	<a href="' + docs[i].link + '">';
            html += '		<img src="'+ docs[i].imagens.imagem_300_250 +'">';
            html += '	</a>';
            html += '	<h3>'+ docs[i].titulo +'</h3>';
            html += '</div>';
		}
		document.getElementById('home-passafoto-container').innerHTML = html;

		montaSwiper();

	}

	start = function (container) {

		var start,
			address;

		start = pageIdentify();
		address = makeAddress(start);
		executeSearch(address, container);

	};

	maisNoticias = function (quantidade, container) {

		QTDNEWS = QTDNEWS + quantidade;
		paginacaoNumero = QTDNEWS / 5;
		/* loga evento a partir da segunda pagina√ß√£o */
		document.getElementById("bt-mais-noticias").setAttribute("data-paginacao", paginacaoNumero);
		start(container);

	};

	nextPage = function (quantidade) {

		page = page + 1;
		if (page > quantidade) {
			page = 1;
		}
		start();

	};

	prevPage = function (quantidade) {

		page = page - 1;
		if (page < 1) {
			page = quantidade;
		}
		start();

	};

	toggle_visibility = function (id) {
	   var e = document.getElementById(id);
	   if(e.style.display == 'block'){
		  e.style.display = 'none';
	   }
	   else{
		  e.style.display = 'block';
	   }
	   return toggle_visibility;
	};


	scrollWin = function () {
		window.scrollTo(0,0);
	};


	getParametros = function (palavra) {
	    palavra = palavra.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + palavra + "=([^&#]*)"),
	    resultados = regex.exec(location.search);
	    return resultados === null ? "" : decodeURIComponent(resultados[1].replace(/\+/g, " "));
	};

	return {
		start: start,
		toggle_visibility: toggle_visibility,
		renderizaCapa: renderizaCapa,
		renderizaPassafoto: renderizaPassafoto,
		renderizaTvColetivo: renderizaTvColetivo,
		scrollWin: scrollWin,
		executeAjax: executeAjax,
		nextPage: nextPage,
		prevPage: prevPage,
		maisNoticias: maisNoticias,
		getParametros: getParametros,
		fazBusca: fazBusca,
		renderizaBusca: renderizaBusca,
		solr_formatar_data: solr_formatar_data,
		zero_pad: zero_pad,
		submitBusca: submitBusca,
		montaSwiperColunistas: montaSwiperColunistas
	};

}());

/*ODIAMOBILE.UTEIS.webspectator(); */

document.addEventListener("DOMContentLoaded", function(event) {
	if(ODIAMOBILE.UTEIS.getMetaName('odia-section-name') == 'Capas'){
		ODIAMOBILE.CONTEUDO.executeAjax('GET', 'http://s0.ejesa.ig.com.br/img/odia/capa/info.json', 'capa');
	}
});

window.onscroll = function (e) {
	ODIAMOBILE.UTEIS.fixabarra();
}