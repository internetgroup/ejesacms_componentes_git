/*global document: false */
/*global console*/
/*global $*/
/*global setInterval*/
/*global jQuery*/

var EJESA = EJESA || {},
    DOMINIO = 'http://est3.odia.ejesa.com.br/api/est3';
    // DOMINIO = 'http://futebol.statig.com.br';
    //DOMINIO = 'http://localhost/odia/futebol';

/** declarando os mÃ³dulos **/
EJESA.namespace('EJESA.FUTEBOL.Classificacao'); /** NarraÃ§Ã£o do Jogo **/

/** implementando mÃ³dulo **/
/** Tabela de ClassificaÃ§Ã£o do campeonato **/
EJESA.FUTEBOL.Classificacao = (function () {
    'use strict';
    var start,
        renderiza,
        mountURL,
        url = '',
        job,
        delay = 30000;



    mountURL = function () {
        var param = EJESA.FUTEBOL.Util.codigocampeonato();
        url += DOMINIO;
        url += "/campeonato/";
        url += "1109";
        url += "/classificacaogeral.json?";
        url += +(new Date());
    };

    job = function() {

        mountURL();

        jQuery.ajax({
            url: url,
            dataType: 'json',
            crossDomain: true,
            // jsonp: false,
            // jsonpCallback: 'classificacaoCampeonato',
            success: function(d) {
                renderiza(JSON.stringify(d));
            }
        });

    };

    start = function () {

        job();

        setInterval(job, delay);

    };

    renderiza = function (data) {

        var obj,
            equipes,
            equipe,
            info,
            time,
            html = '',
            i = 0;

        obj = JSON.parse(data);
        equipes = obj.campeonato.classificacoes.SEM_TACA.GRUPO_UNICO.equipes;


        html += '<table border="0" cellpadding="0" cellspacing="0" class="tabJogos" id="tabJogos">';
        html += '<tbody><tr>';
        html += '<th></th>';
        html += '<th class="times">Times</th>';
        html += '<th>PG</th>';
        html += '<th>J</th>';
        html += '<th>V</th>';
        html += '<th>E</th>';
        html += '<th>D</th>';
        html += '<th>GP</th>';
        html += '<th>GC</th>';
        html += '<th>SG</th>';
        html += '</tr>';


        for (equipe in equipes) {
            if (equipes.hasOwnProperty(equipe)) {
                info = equipes[equipe];


                if (i < 4) {
                    html += '<tr class="primeiros">';
                }

                if (i > 15) {
                    html += '<tr class="ultimos">';
                }

                html += '<td><strong>' + info.posicao + '</strong></td>';
                html += '<td class="times"><strong>' + info.nome + '</strong></td>';
                html += '<td class="cinza"><strong>' + info.pontosGanhos + '</strong></td>';
                html += '<td>' + info.jogos + '</td>';
                html += '<td class="cinza">' + info.vitorias + '</td>';
                html += '<td>' + info.empates + '</td>';
                html += '<td class="cinza">' + info.derrotas + '</td>';
                html += '<td>' + info.golsPro + '</td>';
                html += '<td class="cinza">' + info.golsContra + '</td>';
                html += '<td>' + info.saldoGols + '</td>';
                html += '</tr>';

            }
            i += 1;
        }

        html += '</tbody>';
        html += '</table>';

        $("#classificacao").html(html);

    };



    //revelando API pÃºblica
    return {
        start: start
    };

}());/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


