/*global console*/
/*global $*/
/*global setInterval*/
/*global jQuery*/



var EJESA = EJESA || {},
    DOMINIO = 'http://est3.odia.ejesa.com.br/api/est3',
    IMGPATH = 'http://s0.ejesa.ig.com.br/img/odia/brasileirao2013/',
    HTMLPATH = 'http://futebol.staging.e-odia.com.br/brasileirao2013/',
    RODADA = parseInt($("#rodadaatual").val());
    // http://futebol.statig.com.br/campeonatos/751/estruturaJogos.json?1389899047976&callback=estruturaCampeonato

    console.log('RODADA: '+RODADA+' >>> '+$("#campeonatoid").val());

/** declarando os módulos **/
EJESA.namespace('EJESA.FUTEBOL.Campeonato'); /** Dados do Campeonato **/

/** implementando módulo **/
EJESA.FUTEBOL.Campeonato = (function () {
    'use strict';
    var start,
        renderizaRodada,
        renderizaJogos,
        rederizaCabeca,
        mountURLRound,
        mountURLGames,
        urlRound = '',
        urlGames = '',
        job,
        rodadas = [],
        getGames;

    mountURLRound = function () {
        // var param = EJESA.FUTEBOL.Util.querystring();
        urlRound += DOMINIO;
        urlRound += "/campeonato/";
        urlRound += $("#campeonatoid").val();
        urlRound += "/listapartida.json?aqui&hora=";
        urlRound += +(new Date());
    };

    mountURLGames = function (campeonato, rodada) {
        urlGames = DOMINIO;
        urlGames += "/campeonato/";
        urlGames += $("#campeonatoid").val();
        urlGames += "/";
        urlGames += rodada; /* 1109_1109_25_jogos.json_jogos.json */
        urlGames += "?";
        urlGames += +(new Date());

    };

    job = function () {

        mountURLRound();

        console.log(urlRound);

        jQuery.ajax({
            url: urlRound,
            dataType: 'json',
            crossDomain: true,
            success: function(d) {
                console.log(JSON.stringify(d));
                renderizaRodada(JSON.stringify(d));
            }
        });

    };


    start = function () {

        job();

    };



    renderizaRodada = function (data) {
        var obj,
            fases,
            rdds,
            rodada,
            id;

        obj = JSON.parse(data);

        //dados do jogo
        fases = obj.fases.FASE_UNICA;
        rdds = obj.fases.FASE_UNICA.rodadas;
        id = obj.campeonato.id;
        // RODADA = obj.campeonato.rodadaATual;


        for (rodada in rdds) {
            if (rdds.hasOwnProperty(rodada)) {
                rodadas.push(rdds[rodada]);
            }
        }
        
        getGames(id, rodadas[RODADA - 1]);


    };

    rederizaCabeca = function (campeonato, rodada) {

        var html = '',
            antes = (rodada > 1) ? rodada - 2 : 0,
            depois = (rodada < rodadas.length) ? rodada : rodadas.length - 1;

        html += '<a href="javascript: EJESA.FUTEBOL.Campeonato.getGames(';
        html += campeonato;
        html += ', \'';
        html += rodadas[antes];
        html += '\');"';
        html += ' class="trocar-rodada-volta">';
        html += '<img src="' + IMGPATH + 'nav_esquerda_rodadas.png" />';
        html += '</a>';
        html += '<div class="rodada"><span id="number">';
        html += rodada;
        html += '</span>ª rodada</div>';
        html += '<a href="javascript: EJESA.FUTEBOL.Campeonato.getGames(';
        html += campeonato;
        html += ', \'';
        html += rodadas[depois];
        html += '\');"';
        html += ' class="trocar-rodada-frente" >';
        html += '<img src="' + IMGPATH + 'nav_direita_rodadas.png" />';
        html += '</a>';
        html += '<div class="clear"></div>';

        $('#rodada').html(html);

    };

    renderizaJogos = function (data, rodada) {
        var obj,
            jogo,
            info,
            html,
            htmlJogos = '',
            campeonatoId;

        obj = JSON.parse(data);


        for (jogo in obj) {
            if (obj.hasOwnProperty(jogo)) {
                info = obj[jogo];

                campeonatoId = info.idCampeonato;

                html = '';

                html += '<li><div class="escudo">';
                html += '<img src="' + IMGPATH + 'equipe_';
                html += info.idMandante;
                html += '_h41.png" alt="" title=""></div>';
                html += '<div class="time">';
                html += info.siglaMandante;
                html += '</div>';
                html += '<div class="data-local">';
                html += info.data;
                html += '<span>';
                html += (info.partidaIniciada)?info.placarMandante:'';
                html += '<span class="x">x</span>';
                html += (info.partidaIniciada)?info.placarVisitante:'';
                html += '</span>';
                html += info.estadio;
                /*
                if (info.temNarracao === 'Sim') {
                    //html += '<br><a href="' + HTMLPATH + 'aovivo.html?idcampeonato=751&idpartida=';
                    html += '<br><a href="aovivo.html?idcampeonato=751&idpartida=';
                    html += info.id;
                    html += '" target="_blank">conferir partida</a>';
                }
                */
                html += '</div>';
                html += '<div class="time">';
                html += info.siglaEquipeVisitante;
                html += '</div>';
                html += '<div class="escudo"><img src="' + IMGPATH + 'equipe_';
                html += info.idVisitante;
                html += '_h41.png" alt="" title=""></div>';
                html += '<div class="clear"></div>';
                html += '</div></li>';

                htmlJogos += html;

            }
        }
        $('#jogos ul').html(htmlJogos);

        rederizaCabeca(campeonatoId, rodadas.indexOf(rodada) + 1);
    };


    getGames = function (campeonato, rodada) {

        mountURLGames(campeonato, rodada);

        console.log('urlGames: '+urlGames)
        
        jQuery.ajax({
            url: urlGames,
            dataType: 'json',
            crossDomain: true,
            /*jsonpCallback: rodada.substring(0, rodada.indexOf('.json')),*/
            success: function(d) {
                renderizaJogos(JSON.stringify(d), rodada);
            }
        });

    };


    //revelando API pública
    return {
        start: start,
        getGames: getGames
    };

}());
