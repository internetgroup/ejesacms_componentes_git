/*global document */
 /*global XMLHttpRequest*/
 /*global ActiveXObject*/
 
 var EJESA = EJESA || {},
     narracao = 'http://s0.ejesa.ig.com.br/js/odia/esporte/brasileirao/EJESA_Narracao.js?v=0.2';
     escalacao = 'http://s0.ejesa.ig.com.br/js/odia/esporte/brasileirao/EJESA_Escalacao.js?v=0.2';
     campeonato = 'http://s0.ejesa.ig.com.br/js/odia/esporte/brasileirao/EJESA_Campeonato.js?v=0.18';
     cabecalho = 'http://s0.ejesa.ig.com.br/js/odia/esporte/brasileirao/EJESA_Cabecalho.js?v=0.2';
     rodape = 'http://s0.ejesa.ig.com.br/js/odia/esporte/brasileirao/EJESA_Rodape.js?v=0.2';
     classificacao = 'http://s0.ejesa.ig.com.br/js/odia/esporte/brasileirao/EJESA_Classificacao.js?v=0.6';
 
 
 EJESA.namespace = function (ns_string) {
    'use strict';
    var parts = ns_string.split('.'),
        parent = EJESA,
        i;
 
    if (parts[0] === "EJESA") {
        parts = parts.slice(1);
    }
 
    for (i = 0; i < parts.lenght; i += 1) {
        if (typeof parent[parts[i]] === 'undefined') {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }
    return parent;
 };
 
 /** declarando os mÃ³dulos **/
 EJESA.namespace('EJESA.FUTEBOL');
 EJESA.namespace('EJESA.FUTEBOL.Util');
 EJESA.namespace('EJESA.FUTEBOL.CALL_Narracao'); /** NarraÃ§Ã£o do Jogo **/
 EJESA.namespace('EJESA.FUTEBOL.CALL_Escalacao'); /** EscalaÃ§Ã£o do Jogo **/
 EJESA.namespace('EJESA.FUTEBOL.CALL_Campeonato'); /** Dados do Campeonato **/
 EJESA.namespace('EJESA.FUTEBOL.CALL_Jogos'); /** Lista Jogos **/
 
 EJESA.FUTEBOL = {};
 
 /** implementando mÃ³dulo **/
 EJESA.FUTEBOL.Util = (function () {
    'use strict';
    //propriedades privadas
    //mÃ©todos privados
 
    var querystring = function () {
 
         var query_string,
             query,
             vars,
             i,
             pair,
             arr;
 
         query_string = {};
         query = window.location.search.substring(1);
         vars = query.split("&");
 
         for (i = 0; i < vars.length; i += 1) {
             arr = [];
 
             pair = vars[i].split("=");
 
             if (query_string[pair[0]] === undefined) {
 
                 query_string[pair[0]] = pair[1];
 
             } else if (typeof query_string[pair[0]] === "string") {
 
                 arr = [ query_string[pair[0]], pair[1] ];
                 query_string[pair[0]] = arr;
 
             } else {
 
                 query_string[pair[0]].push(pair[1]);
                 
             }
         }
         return query_string;
     },
 
        getXHR = function () {
            var i,
                xhr,
                activexIds = [
                    'MSXML2.XMLHTTP.3.0',
                    'MSXML2.XMLHTTP',
                    'Microsoft.XMLHTTP'
                ];
            if (typeof XMLHttpRequest === "function") { // native XHR
                xhr =  new XMLHttpRequest();
            } else { // IE before 7
                for (i = 0; i < activexIds.length; i += 1) {
                    try {
                        xhr = new ActiveXObject(activexIds[i]);
                        break;
                    } catch (e) {}
                }
            }
            return xhr;
        },
 
        trim = function (str) {
            return str.replace(/^\s+|\s+$/g, "");
        },
 
        arraySort = function (list) {
 
            list.sort(function (a, b) {
                return (a.rodada.nodeValue - b.rodada.nodeValue);
            });
 
            return list;
        },
 
        loadScript = function (url, callback) {
 
            var head, script;
            head = document.getElementsByTagName('head')[0];
 
            script = document.createElement('script');
            script.type = "text/javascript";
            if (script.readyState) {
                script.onreadystatechange = function () {
                    if (script.readyState === "loaded" || script.readyState === "complete") {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else {
                script.onload = function () {
                    callback();
                };
            }
            script.src = url;
 
            head.appendChild(script);
        },
 
         codigocampeonato = function () {
 
             //var campeonatoid = document.getElementById('campeonatoid').value;
             //return campeonatoid;
 
         };
 
    //revelando API pÃºblica
    return {
        getXHR: getXHR,
        trim: trim,
        loadScript: loadScript,
        arraySort: arraySort,
        querystring: querystring,
         codigocampeonato: codigocampeonato
    };
 }());
 
 /** implementando mÃ³dulo **/
 /** NarraÃ§Ã£o do jogo **/
 EJESA.FUTEBOL.CALL_Narracao = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;
 
    callInit = function () {
        EJESA.FUTEBOL.Narracao.start();
    };
 
    init = function () {
        EJESA.FUTEBOL.Util.loadScript(narracao, callInit);
    };
 
    //revelando API pÃºblica
    return {
        init: init
    };
 }());
 
 /** implementando mÃ³dulo **/
 /** EscalaÃ§Ã£o do jogo **/
 EJESA.FUTEBOL.CALL_Escalacao = (function () {
     'use strict';
     //mÃ©todos privados
     var callInit, init;
 
     callInit = function () {
         EJESA.FUTEBOL.Escalacao.start();
     };
 
     init = function () {
         EJESA.FUTEBOL.Util.loadScript(escalacao, callInit);
     };
 
     //revelando API pÃºblica
     return {
         init: init
     };
 }());
 
 
 /** implementando mÃ³dulo **/
 /** Lista de Jogos **/
 EJESA.FUTEBOL.CALL_Campeonato = (function () {
     'use strict';
     //mÃ©todos privados
     var callInit, init;
 
     callInit = function () {
         EJESA.FUTEBOL.Campeonato.start();
     };
 
     init = function () {
         EJESA.FUTEBOL.Util.loadScript(campeonato, callInit);
     };
 
     //revelando API pÃºblica
     return {
         init: init
     };
 }());
 
 
 /** Header da pÃ¡gina **/
 EJESA.FUTEBOL.CALL_Cabecalho = (function () {
     'use strict';
     //mÃ©todos privados
     var callInit, init;
 
     callInit = function () {
         EJESA.FUTEBOL.Cabecalho.start();
     };
 
     init = function () {
         EJESA.FUTEBOL.Util.loadScript(cabecalho, callInit);
     };
 
     //revelando API pÃºblica
     return {
         init: init
     };
 }());
 
 /** Footer da pÃ¡gina **/
 EJESA.FUTEBOL.CALL_Rodape = (function () {
     'use strict';
     //mÃ©todos privados
     var callInit, init;
 
     callInit = function () {
         EJESA.FUTEBOL.Rodape.start();
     };
 
     init = function () {
         EJESA.FUTEBOL.Util.loadScript(rodape, callInit);
     };
 
     //revelando API pÃºblica
     return {
         init: init
     };
 }());
 
 /** Footer da pÃ¡gina **/
 EJESA.FUTEBOL.CALL_Classificacao = (function () {
     'use strict';
     //mÃ©todos privados
     var callInit, init;
 
     callInit = function () {
         EJESA.FUTEBOL.Classificacao.start();
     };
 
     init = function () {
         EJESA.FUTEBOL.Util.loadScript(classificacao, callInit);
     };
 
     //revelando API pÃºblica
     return {
         init: init
     };
 }());/* 
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
Add a comment to this line
  */