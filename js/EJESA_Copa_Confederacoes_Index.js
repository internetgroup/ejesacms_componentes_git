/*global document */
/*global XMLHttpRequest*/
/*global ActiveXObject*/

var EJESA = EJESA || {},
//https://www.ejesa.com.br/dataodia/listajogosdecampeonato.xml
	CLASSIFICACAO = 'http://s0.ejesa.ig.com.br/js/odia/EJESA_Copa_Classificacao.js?v=0.19',
	RESULTADOS = 'http://s0.ejesa.ig.com.br/js/odia/EJESA_Copa_Resultados.js?v=0.3';

EJESA.namespace = function (ns_string) {
	'use strict';
	var parts = ns_string.split('.'),
		parent = EJESA,
		i;

	if (parts[0] === "EJESA") {
		parts = parts.slice(1);
	}

	for (i = 0; i < parts.lenght; i += 1) {
		if (typeof parent[parts[i]] === 'undefined') {
			parent[parts[i]] = {};
		}
		parent = parent[parts[i]];
	}
	return parent;
};

/** declarando os módulos **/
EJESA.namespace('EJESA.COPACONFEDERACOES');
EJESA.namespace('EJESA.COPACONFEDERACOES.Util');
EJESA.namespace('EJESA.COPACONFEDERACOES.CALL_Classificacao'); /** Tabela de classificação **/
EJESA.namespace('EJESA.COPACONFEDERACOES.CALL_Resultados'); /** Resultados dos Jogos **/

EJESA.COPACONFEDERACOES = {};

/** implementando módulo **/
EJESA.COPACONFEDERACOES.Util = (function () {
	'use strict';
	//propriedades privadas
	//métodos privados

	var	getXHR = function () {
			var i,
				xhr,
				activexIds = [
			        'MSXML2.XMLHTTP.3.0',
			        'MSXML2.XMLHTTP',
			        'Microsoft.XMLHTTP'
				];
		    if (typeof XMLHttpRequest === "function") { // native XHR
		        xhr =  new XMLHttpRequest();
		    } else { // IE before 7
		        for (i = 0; i < activexIds.length; i += 1) {
		            try {
		                xhr = new ActiveXObject(activexIds[i]);
		                break;
		            } catch (e) {}
		        }
		    }
		    return xhr;
		},

		trim = function (str) {
			return str.replace(/^\s+|\s+$/g, "");
		},

		arraySort = function (list) {

			list.sort(function (a, b) {
			    return (a.rodada.nodeValue - b.rodada.nodeValue);
			});

			return list;
		},

		loadScript = function (url, callback) {

			var head, script;
			head = document.getElementsByTagName('head')[0];

			script = document.createElement('script');
			script.type = "text/javascript";
			if (script.readyState) {
				script.onreadystatechange = function () {
					if (script.readyState === "loaded" || script.readyState === "complete") {
						script.onreadystatechange = null;
						callback();
					}
				};
			} else {
				script.onload = function () {
					callback();
				};
			}
			script.src = url;

			head.appendChild(script);
		};

	//revelando API pública
	return {
		getXHR: getXHR,
		trim: trim,
		loadScript: loadScript,
		arraySort: arraySort
	};


 } ());


/** implementando módulo **/
/** Classificação do campeonato **/
EJESA.COPACONFEDERACOES.CALL_Classificacao = (function () {
	'use strict';
	//métodos privados
	var callInit, init;

	callInit = function () {
		EJESA.COPACONFEDERACOES.Classificacao.start();
	};

	init = function () {
		EJESA.COPACONFEDERACOES.Util.loadScript(CLASSIFICACAO, callInit);
	};

	//revelando API pública
	return {
		init: init
	};
}());


/** implementando módulo **/
/** Resultados do campeonato **/
EJESA.COPACONFEDERACOES.CALL_Resultados = (function () {
	'use strict';
	//métodos privados
	var callInit, init;

	callInit = function () {
		EJESA.COPACONFEDERACOES.Resultados.start();
	};

	init = function () {
		EJESA.COPACONFEDERACOES.Util.loadScript(RESULTADOS, callInit);
	};

	//revelando API pública
	return {
		init: init
	};
}());