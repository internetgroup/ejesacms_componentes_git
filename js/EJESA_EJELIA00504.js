/*global document: false */
/*global window */
/*global console*/



var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.EJELIA00504'); /** Abas de Subeditorias do Rio **/

/** implementando módulo **/
/** Abas de Subeditorias do Rio **/
EJESA.EJELIA00504 = (function () {
	'use strict';
	//propriedades privadas
	var mountEJELIA00504,
		mountODia24Horas,
		mountODiaEstado,
		mountODiaBaixada,
		getMyContent,
		url = window.location,
		urlString = url.toString(),
		urlArray = urlString.split("/"),
		abaSelecionada = 1,
		cleantab,
		getEJEHLE01602,
		getEJEHLE00202,
		getEJEHLE00301,
		html,
		init,
		start,
		load;

	//métodos privados
	cleantab = function () {
		EJESA.Util.addClassNodeList('.optab', 'tabmenu_off');
		EJESA.Util.removeClassName('.optab', 'tabmenu_off');
	};

	getEJEHLE01602 = function (obj) {

		var html = '',
			link = obj.dados.chamada.link,
			titulo = obj.dados.chamada.titulo,
			chapeu = obj.dados.chamada.chapeu,
			imagens = obj.dados.chamada.imagens,
			imagem = imagens.imagem_300_175;

	    html += "<div class='EJEHLE01602'>";
	    html += "<div class='headline'>";
	    html += "<div class='headline-img'>";
	    html += "<a href='" + link  + "' title='" + titulo  + "'>";
	    html += "<img width='300' height='175' alt='" + titulo + "' src='" + imagem + "'>";
	    html += "</a>";
	    html += "</div>";
	    html += "<div class='headline-hat'>";
	    html += "<a href='" + link + "' title='" + chapeu + "'>";
	    html += chapeu;
	    html += "</a>";
	    html += "</div>";
	    html += "<div class='headline-title'>";
	    html += "<a href='" + link + "' title='" + titulo + "'>";
	    html += titulo;
	    html += "</a>";
	    html += "</div>";
	    html += "</div>";
	    html += "</div>";

	    return html;
	};

	getEJEHLE00202 = function (obj) {

	    var html = '',
			link = obj.dados.chamada.link,
			titulo = obj.dados.chamada.titulo,
			chapeu = obj.dados.chamada.chapeu;

	    html += "<div class='EJEHLE00202'>";
	    html += "<div class='headline'>";
	    html += "<div class='headline-hat'>";
	    html += "<a title='" + chapeu + "' href='" + link + "'>";
	    html += chapeu;
	    html += "</a>";
	    html += "</div>";
	    html += "<div class='headline-title'>";
	    html += "<a title='" + titulo + "' href='" + link + "'>";
	    html += titulo;
	    html += "</a>";
	    html += "</div>";
	    html += "</div>";
	    html += "</div>";

	    return html;
	};

	getEJEHLE00301 = function (obj) {

	    var html = '',
			link = obj.dados.chamada.link,
			titulo = obj.dados.chamada.titulo,
			chapeu = obj.dados.chamada.chapeu,
			imagens = obj.dados.chamada.imagens,
			imagem = imagens.imagem_140_95;

	    html += "<div class='EJEHLE00301'>";
	    html += "<div class='headline'>";
	    html += "<div class='headline-img'>";
	    html += "<a title='" + imagem + "' href='" + link + "'>";
	    html += "<img alt='" + titulo + "' src='" + imagem + "'>";
	    html += "</a>";
	    html += "</div>";
	    html += "<div class='headline-hat'>";
	    html += "<a title='" + chapeu + "' href='" + link + "'>";
	    html += chapeu;
	    html += "</a>";
	    html += "</div>";
	    html += "<div class='headline-title'>";
	    html += "<a title='" + titulo + "' href='" + link + "'>";
	    html += titulo;
	    html += "</a>";
	    html += "</div>";
	    html += "</div>";
	    html += "</div>";

	    return html;
	};

	mountEJELIA00504 = function (data) {
		var myobj = data,
			odiabaixada,
			odianoestado,
			odia24horas,
			html;

		if (myobj.noticias.odiabaixada !== undefined) {
			odiabaixada = myobj.noticias.odiabaixada;
			html = mountODiaBaixada(odiabaixada);
		}

		if (myobj.noticias.odianoestado !== undefined) {
			odianoestado = myobj.noticias.odianoestado;
			html = mountODiaEstado(odianoestado);
		}

		if (myobj.noticias.odia24horas !== undefined) {
			odia24horas = myobj.noticias.odia24horas;
			html = mountODia24Horas(odia24horas);
		}

		document.getElementById('EJELIA00504HL').innerHTML = html;
	};

	mountODia24Horas = function (data) {
		var html = "<div class='EJESTA00101'>",
			i,
			componente,
			objeto,
			lista;

		lista = data.noticias.odia24horas;

		for (i = 0; i < lista.length; i = i + 1) {
			objeto = lista[i];
			componente = objeto.tipo;

			if (componente === 'EJEHLE01602') {
				html += getEJEHLE01602(objeto);
			}
			if (componente === 'EJEHLE00202') {
				html += getEJEHLE00202(objeto);
			}
			if (componente === 'EJEHLE00301') {
				html += getEJEHLE00301(objeto);
			}
			//if (componente === 'EJETOE00404') {
				//html += getEJETOE00404(objeto);
			//}
		}
		html += "</div>";

		cleantab();
		EJESA.Util.addClassName('option3', 'tabmenu_on', true);
		EJESA.Util.removeClassName('option3', 'tabmenu_off');

		//document.getElementById('EJETOE00404').style.display = 'none';
		document.getElementById('EJELIA00504HL').style.display = 'block';
		document.getElementById('EJELIA00504HL').innerHTML = html;
	};

	mountODiaBaixada = function (data) {
		var html = "<div class='EJESTA00101'>",
			i,
			objeto,
			componente,
			lista;

		lista = data.noticias.odiabaixada;
		for (i = 0; i < lista.length; i = i + 1) {
			objeto = lista[i];
			componente = objeto.tipo;

			if (componente === 'EJEHLE01602') {
				html += getEJEHLE01602(objeto);
			}
			if (componente === 'EJEHLE00202') {
				html += getEJEHLE00202(objeto);
			}
			if (componente === 'EJEHLE00301') {
				html += getEJEHLE00301(objeto);
			}
			//if (componente === 'EJETOE00404') {
				//html += getEJETOE00404(objeto);
			//}
		}
		html += "</div>";


		cleantab();
		EJESA.Util.addClassName('option1', 'tabmenu_on', true);
		EJESA.Util.removeClassName('option1', 'tabmenu_off');

		//document.getElementById('EJETOE00404').style.display = 'none';
		document.getElementById('EJELIA00504HL').style.display = 'block';
		document.getElementById('EJELIA00504HL').innerHTML = html;

		return html;
	};

	mountODiaEstado = function (data) {
		var html = "<div class='EJESTA00101'>",
			i,
			objeto,
			componente,
			lista;


		lista = data.noticias.odianoestado;

		for (i = 0; i < lista.length; i = i + 1) {
			objeto = lista[i];
			componente = objeto.tipo;

			if (componente === 'EJEHLE01602') {
				html += getEJEHLE01602(objeto);
			}
			if (componente === 'EJEHLE00202') {
				html += getEJEHLE00202(objeto);
			}
			if (componente === 'EJEHLE00301') {
				html += getEJEHLE00301(objeto);
			}
			//if (componente === 'EJETOE00404') {
				//html += getEJETOE00404(objeto);
			//}
		}
		html += "</div>";

		cleantab();
		EJESA.Util.addClassName('option2', 'tabmenu_on', true);
		EJESA.Util.removeClassName('option2', 'tabmenu_off');

		//document.getElementById('EJETOE00404').style.display = 'none';
		document.getElementById('EJELIA00504HL').style.display = 'block';
		document.getElementById('EJELIA00504HL').innerHTML = html;
	};

	getMyContent = function (urlcontent, callback) {

		var i, xhr;

		xhr = EJESA.Util.getXHR();

	    xhr.onreadystatechange = function () {
	        if (xhr.readyState !== 4) {
	            return false;
	        }
	        if (xhr.status !== 200) {
	            console.log("Error, status code: " + xhr.status);
	            return false;
	        }

	        callback(JSON.parse(xhr.responseText));
	    };

	    xhr.open("GET", urlcontent, true);
	    xhr.send("");
	};

	init = function () {
		//executar ao iniciar
		load = document.getElementById('option1').onclick;
		load.call(document.getElementById('option1'));
	};

	start = function () {

	};

	//carregando no final da página
	window.onload = function () {

		document.getElementById('option1').onclick = function () {
			getMyContent("http://odia.ig.com.br/odiabaixada/odiabaixada.json", mountODiaBaixada);
		};

		document.getElementById('option2').onclick = function () {
			getMyContent("http://odia.ig.com.br/odiaestado/odiaestado.json", mountODiaEstado);
		};

		document.getElementById('option3').onclick = function () {
			getMyContent("http://odia.ig.com.br/odia24horas/odia24horas.json", mountODia24Horas);
		};

		init();

	};

	//revelando API pública
	return {
		start: start
	};
}());


$("#EJELIA00504HL .EJESTA00101").css ('display' , 'none');
$(".EJETOE00404").css ('display' , 'block');
