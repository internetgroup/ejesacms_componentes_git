/*global document: false */
/*global console*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.EJELIA00302'); /** Lista de últimas da Editoria **/

/** implementando módulo **/
/** Lista de últimas da Editoria **/
EJESA.EJELIA00302 = (function () {

	'use strict';
	var QTDNEWS = 5,
		SITE = 'odia',
		SOLR = 'http://odia.ig.com.br/_indice/noticias/select?',
		FORMAT = 'json',
		start,
		renderiza,
		page = 1,
		pageIdentify,
		makeAddress,
		query,
		executeSearch,
		pageMarked,
		nextPage,
		prevPage,
		resetPagination,
		section_id = EJESA.Util.getMetaName('odia-section-id');



//métodos privados
	pageIdentify = function () {
		var start = (page - 1) * QTDNEWS;
		return start;
	};

	makeAddress = function (start) {

		var address = SOLR;

		address += 'start=';
		address += start;
		address += '&size=';
		address += QTDNEWS;
		address += '&site=';
		address += SITE;
		address += '&secoes_EH=';
		address += section_id;
		address += '&wt=';
		address += FORMAT;

		return address;
	};

	pageMarked = function () {
		var mp,
			range,
			posid = '';

		if (page > 5) {
			range = page / 5;

			if (page % 5 === 1) {

				posid = page % 5;

			} else {
				posid = page % 5;
				if (posid === 0) {
					posid = 5;
				}
			}

			mp = 'hn_pos_' + posid;


		} else {
			mp = 'hn_pos_' + page;
		}

		EJESA.Util.removeClassByID(mp, 'headline_nav_off');
		EJESA.Util.addClassByID(mp, 'headline_nav_on', true);
	};

	resetPagination = function () {
		EJESA.Util.removeClassByID('hn_pos_1', 'headline_nav_on');
		EJESA.Util.removeClassByID('hn_pos_2', 'headline_nav_on');
		EJESA.Util.removeClassByID('hn_pos_3', 'headline_nav_on');
		EJESA.Util.removeClassByID('hn_pos_4', 'headline_nav_on');
		EJESA.Util.removeClassByID('hn_pos_5', 'headline_nav_on');

		EJESA.Util.addClassByID('hn_pos_1', 'headline_nav_off');
		EJESA.Util.addClassByID('hn_pos_2', 'headline_nav_off');
		EJESA.Util.addClassByID('hn_pos_3', 'headline_nav_off');
		EJESA.Util.addClassByID('hn_pos_4', 'headline_nav_off');
		EJESA.Util.addClassByID('hn_pos_5', 'headline_nav_off');
	};


	executeSearch = function (address) {
		var xmlhttp;
        
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState !== 4) {
                return false;
            }
            if (xmlhttp.status !== 200) {
                console.log("Error, status code: " + xmlhttp.status);
                return false;
            }
            renderiza(xmlhttp.responseText);
                
        }
        xmlhttp.open("GET",address,true);
        xmlhttp.send("");
	};


	renderiza = function (data) {
		var obj = JSON.parse(data),
			docs,
			i,
			limit,
			date_news,
			sections,
			mysection,
			tit,
			html = '';

		docs = obj.response.docs;

		for (i = 0; i < docs.length; i = i + 1) {

			html += "<li>";

			html += "<span class='headline-item'><p>";

			limit = 0;
			if (docs[i].urlImgEmp_idCorteImagem !== undefined) {
				html += "<a href='" + docs[i].url + "'>";
				html += "<img src='" + docs[i].urlImgEmp_80x60 + "'>";
				html += "</a>";
				limit = 90;
			} else {
				limit = 180;
			}

			date_news = EJESA.Util.dateSolr(docs[i].startDate, '$4:$5');

			html += "<span class='headline-item-eye'>";
			html += date_news;
			html += " - ";

			sections = docs[i].secaoBreadcrumb.split('›');
			mysection = sections[sections.length - 1];
			html += EJESA.Util.trim(mysection);

			html += "</br></span>";
			html += "<span class='headline-item-title'>";
			html += "<a href='" + docs[i].url + "'>";

			tit = '';
			if (docs[i].titulo.length > limit) {
				tit = docs[i].titulo.substring(0, limit) + "...";
			} else {
				tit = docs[i].titulo;
			}

			html += tit;
			html += "</a>";
			html += "</span>";
			html += "</p></span>";

			html += "</li>";
		}

		document.getElementById('EJELIA00302HL').innerHTML = html;

	};

	//métodos privados
	start = function () {

		var start,
			address,
			search;

		start = pageIdentify();

		address = makeAddress(start);

		executeSearch(address);

		resetPagination();

		pageMarked();

	};

	nextPage = function () {

		page = page + 1;
		if (page > 5) {
			page = 1;
		}
		start();

	};

	prevPage = function () {

		page = page - 1;
		if (page < 1) {
			page = 5;
		}
		start();

	};
  //procedimentos de inicialização
    //posicionando abas
    window.onload = function () {
        $('#news-item-commented').click(function() {
	        $('.headline-nav').css('display', 'none');
            $('#news-commented').css('display', 'block');
            $('#news-last').css('display', 'none');
            $('#news-last').css('border-bottom', 'none');
            $('#news-commented').css('border-bottom', '2px solid #b2b2b2');


		    $('#news-item-last').removeClass('ativo');
            $('#news-item-last').addClass('inativo');
   			$('#news-item-commented').removeClass('inativo');
		    $('#news-item-commented').addClass('ativo');

        });
        $('#news-item-last').click(function() {
        	$('.headline-nav').css('display', 'block');
            $('#news-commented').css('display', 'none');
            $('#news-commented').css('border-bottom', 'none');
            $('#news-last').css('display', 'block');
            $('#news-last').css('border-bottom', '2px solid #b2b2b2');     


		    $('#news-item-last').removeClass('inativo');
            $('#news-item-last').addClass('ativo');
   			$('#news-item-commented').removeClass('ativo');
		    $('#news-item-commented').addClass('inativo');
        });
        $('#news-item-last').click();
        $('#news-item-commented').hide();
    };
	//revelando API pública
	return {
		start: start,
		nextPage: nextPage,
		prevPage: prevPage
	};
}());

