/*global document: false */
/*global console*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.EJENEA00504'); /** Lista de noticias relacionadas **/


/** implementando módulo **/
/** Lista de noticias relacionadas **/
EJESA.EJENEA00504 = (function () {

	'use strict';
	//propriedades privadas
	var QTDNEWS = 4,
		SITE = 'odia',
		SOLR = 'http://odia.ig.com.br/_indice/noticias/select?',
		FORMAT = 'json',
		start,
		renderiza,
		page = 1,
		makeAddress,
		executeSearch,
		parseTag,
		tags = EJESA.Util.getMetaName('tags');
	//fim da declaracao var

	//métodos privados
	parseTag = function (tags) {
		var mytag = '',
			tag,
			mytags,
			i;
		mytags = tags.split('%20');
		for (i = 0; i < mytags.length; i += 1) {
			tag = "\"";
			tag += mytags[i].replace(/_/g, ' ');
			tag += "\"";
			mytag += tag;
		}
		return mytag;
	};


	makeAddress = function () {

		var address = SOLR;

		address += 'start=';
		address += page;
		address += '&size=';
		address += QTDNEWS;
		address += '&site=';
		address += SITE;
		address += '&comb_termos=';
		address += parseTag(tags);
		address += '&wt=';
		address += FORMAT;

		return address;
	};


	executeSearch = function (query) {

		var i, xhr;

		xhr = EJESA.Util.getXHR();

	    xhr.onreadystatechange = function () {
	        if (xhr.readyState !== 4) {
	            return false;
	        }
	        if (xhr.status !== 200) {
	            console.log("Error, status code: " + xhr.status);
	            return false;
	        }
	        renderiza(xhr.responseText);
	    };

	    xhr.open("GET", query, true);
	    xhr.send("");
	};

	renderiza = function (data) {

		var obj = JSON.parse(data),
			docs,
			i,
			limit,
			date_news,
			sections,
			mysection,
			tit,
			html = '',
			eye;


		docs = obj.response.docs;

        for (i = 0; i < docs.length; i += 1) {

			html += "<li>";


			html += "<div class='headline'>";

			limit = 0;
			if (docs[i].urlImgEmp_140x90 !== undefined) {

				html += "<div class='headline-img'>";
				html += "<a href='" + docs[i].url + "' title=''>";
				html += "<img src='" + docs[i].urlImgEmp_140x90 +  "' width='140' height='90' />";
				html += "</a>";
				html += "</div>";
				limit = 60;
			} else {

				limit = 120;
			}

			tit = '';
			if (docs[i].titulo.length > limit) {
				tit = docs[i].titulo.substring(0, limit) + "...";
			} else {
				tit = docs[i].titulo;
			}

			html += "<div class='headline-title'>";
			html += "<a href='" + docs[i].url + "' title=''>";
			html += tit;
			html += "</a>";
			html += "</div>";

			if (docs[i].urlImgEmp_idCorteImagem === undefined) {

				eye = '';
				if (docs[i].olho.length > 80) {
					eye = docs[i].olho.substring(0, 80) + "...";
				} else {
					eye = docs[i].olho;
				}


				html += "<div class='headline-eye'>";
				html += "<a href='" + docs[i].url + "' title=''>";
				html += eye;
				html += "</a>";
				html += "</div>";

			}

			html += "</li>";
        }


		document.getElementById('EJENEA00504HL').innerHTML = html;
	};


	start = function () {

		var address,
			search;


		address = makeAddress();

		executeSearch(address);


	};



	//revelando API pública
	return {
		start: start
	};
}());
