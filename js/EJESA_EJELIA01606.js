//var ZOOM00 = 'http://static.zoom.com.br/PARTNER_MEDIA_PORTAL/';
var ZOOM00 = 'http://static.zoom.com.br/PARTNER_MEDIA_PORTAL/',
    
	// ZOOM000 = ZOOM00 + 'o_dia_28.xml';

   ZOOM000 = ZOOM00 + 'o_dia_28.xml',
   ZOOM01 = ZOOM00 + 'o_dia_30.xml',
   ZOOM02 = ZOOM00 + 'o_dia_27.xml',
   ZOOM03 = ZOOM00 + 'o_dia_29.xml',
   ZOOM04 = ZOOM00 + 'o_dia_64.xml',
   ZOOM05 = ZOOM00 + 'o_dia_42.xml';

var cleanShop = function () {
    'use strict';
    jQuery('.channel').addClass('taboff');
    jQuery('.channel').removeClass('tabon');
};

var getOffers = function (urlcontent, shopping, callback) {
    'use strict';
    jQuery.ajax({
        url : urlcontent,
        type: "GET",
        dataType : "xml",
        success : callback(shopping)
    });
};

var mountChannel = function (shopping) {
    'use strict';
    return function (data) {

        jQuery(data).find("vitrine").each(function () {
            var tags = this.childNodes.length,
                i,
                tag;
            for (i = 0; i < tags; i = i + 1) {
                tag = this.childNodes[i];
                if (tag.tagName === 'canal') {
                    jQuery(shopping).html(tag.childNodes[0].nodeValue);
                }
            }
        });

    };

};

var getProduct = function (obj, pos) {
    'use strict';
    var html = '',
        tags = obj.childNodes,
        loja = '',
        nome = '',
        texto = '',
        preco1 = '',
        preco2 = '',
        destino = '',
        imagem = '',
        i,
        tag,
        aux;


    for (i = 0; i < tags.length; i = i + 1) {
        tag = tags[i];
        if (tag.tagName === 'loja') {
            loja = tag.childNodes[0].nodeValue;
        }
        if (tag.tagName === 'nome') {
            nome = tag.childNodes[0].nodeValue;
        }
        if (tag.tagName === 'preco') {
            texto = tag.childNodes[0].nodeValue;
            aux = texto.split('de');
            preco1 = aux[0] + ' de';
            preco2 = aux[1];
        }
        if (tag.tagName === 'urldestino') {
            destino = tag.childNodes[0].nodeValue;
        }
        if (tag.tagName === 'urlimagem') {
            imagem = tag.childNodes[0].nodeValue;
        }
    }

    html += "<div class='product'>";
    if (pos) {
        html += "<div class='shape2'>";
    } else {
        html += "<div class='shape1'>";
    }
    html += "<div class='product-img'>";
    html += "<a href='" + destino +  "' target='_blank'>";
    html += "<img src='" + imagem  + "'>";
    html += "</a>";
    html += "</div>";
    html += "<div class='product-store'>";
    html += "<a href='" + destino +  "' target='_blank'>";
    html += loja;
    html += "</a>";
    html += "</div>";
    html += "<div class='product-title'>";
    html += "<a href='" + destino +  "' target='_blank'>";
    html += nome;
    html += "</a>";
    html += "</div>";
    html += "<div class='product-price'>";
    html += "<a href='" + destino +  "' target='_blank'>";
    html += preco1;
    html += "</a>";
    html += "</div>";
    html += "<div class='product-total'>";
    html += "<a href='" + destino +  "' target='_blank'>";
    html += preco2;
    html += "</a>";
    html += "</div>";
    html += "</div>";
    html += "</div>";

    return html;

};

var mountProduct = function () {
    'use strict';
    return function (data) {
        var html = '',
            pos = false;
        jQuery(data).find("vitrine").each(function () {
            var tags = this.childNodes.length,
                i,
                tag;
            for (i = 0; i < tags; i = i + 1) {
                tag = this.childNodes[i];

                if (i === (tags - 1)) {
                    pos = true;
                }

                if (tag.tagName === 'oferta') {
                    html += getProduct(tag, pos);
                }

            }

        });

        jQuery('#shopping-offers').html(html);
    };

};



(
    function () {

        'use strict';
        jQuery('#shop00').click(
            function () {

                cleanShop();
                jQuery('#shop00').addClass('tabon');
                jQuery('#shop00').removeClass('taboff');
                getOffers(ZOOM000, '', mountProduct);

            }
        );
        jQuery('#shop01').click(
            function () {

                cleanShop();
                jQuery('#shop01').addClass('tabon');
                jQuery('#shop01').removeClass('taboff');
                getOffers(ZOOM01, '', mountProduct);

            }
        );

        jQuery('#shop02').click(
            function () {

                cleanShop();
                jQuery('#shop02').addClass('tabon');
                jQuery('#shop02').removeClass('taboff');
                getOffers(ZOOM02, '', mountProduct);
            }
        );

        jQuery('#shop03').click(
            function () {

                cleanShop();
                jQuery('#shop03').addClass('tabon');
                jQuery('#shop03').removeClass('taboff');
                getOffers(ZOOM03, '', mountProduct);
            }
        );

        jQuery('#shop04').click(
            function () {

                cleanShop();
                jQuery('#shop04').addClass('tabon');
                jQuery('#shop04').removeClass('taboff');
                getOffers(ZOOM04, '', mountProduct);
            }
        );

        jQuery('#shop05').click(
            function () {

                cleanShop();
                jQuery('#shop05').addClass('tabon');
                jQuery('#shop05').removeClass('taboff');
                getOffers(ZOOM05, '', mountProduct);
            }
        );

        jQuery('#shop06').click(
            function () {

                cleanShop();
                jQuery('#shop06').addClass('tabon');
                jQuery('#shop06').removeClass('taboff');
                getOffers(ZOOM06, '', mountProduct);
            }
        );


    }()
);

var start = function () {
    'use strict';
    jQuery('#shop00').click();

};

