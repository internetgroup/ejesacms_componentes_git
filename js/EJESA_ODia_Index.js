/*global document: false */
/*global window */
/*global XMLHttpRequest*/
/*global ActiveXObject*/
/*global console*/

var topclick = 0;
var intervencao = 0;
var dhtml = 0;
var timerIntervencao;
var contador;
var gwd;
var adSuperVideo;
var adIntervencao;
var adRodapeRetraido;
var adRodapeExpandido;
var adTopClick;
var addhtml;
var alturaWindow = $(window).height();
var larguraWindow = $(window).width();
var player;
var btnsTopclickLoaded = 0;

var EJESA = EJESA || {};
var JS_ENVIROMENT = 'js';
var SOLR_NOTICIAS = 'http://odia.ig.com.br/_indice/noticias/',
    EJELIA00602 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJELIA00602.min.js?v=0.19',
    EJELIA00106 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJELIA00106.min.js?v=0.36',
    EJELIA00202 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJELIA00202.min.js?v=0.21',
    EJELIA00302 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJELIA00302.min.js?v=0.21',
    EJELIA01204 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJELIA01204.js?v=0.04',
    EJENEA00504 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJENEA00504.min.js?v=0.2',
    EJENEA01008 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJENEA01008.min.js?v=0.08',
    EJELIA01606 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJELIA01606.min.js?v=0.22',
    EJELIA00504 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJELIA00504.min.js?v=0.3',
    EJETOA01906 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJETOA01906.min.js?v=0.1',
    EJELIA00906 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJELIA00906.min.js?v=0.30',
    EJELIA01304 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJELIA01304.min.js?v=0.01',
    /*EJELIA00502 = 'http://s0.ejesa.ig.com.br/' + JS_ENVIROMENT + '/odia/EJESA_EJELIA00502.min.js?v=0.01',*/
    WEBSPECTATOR = 'http://services.webspectator.com/init/WS-00DIA/' + (+new Date),
    REALTIME_CORE = 'http://clients.realtime.co/odia/rt_core.js',
    IBTX = 'http://www.apps.realtime.co/IBTX/9s3Lrr/IBTX.js';
var SITE_ENVIROMENT;
var JQUERY;


EJESA.namespace = function (ns_string) {
    'use strict';
    var parts = ns_string.split('.'),
        parent = EJESA,
        i;

    if (parts[0] === "EJESA") {
        parts = parts.slice(1);
    }

    for (i = 0; i < parts.lenght; i += 1) {
        if (parent[parts[i]] === 'undefined') {
            parent[parts[i]] = {};
        }
        parent = parent[parts[i]];
    }
    return parent;
};

/** declarando os mÃ³dulos **/
EJESA.namespace('EJESA.Util');
EJESA.namespace('EJESA.CALL_EJELIA00602'); /** Lista de Ãºltimas da NotÃ­cia **/
EJESA.namespace('EJESA.CALL_EJELIA00106'); /** Lista de Ãºltimas do Canal **/
EJESA.namespace('EJESA.CALL_EJELIA00202'); /** Lista de Ãºltimas Home  **/
EJESA.namespace('EJESA.CALL_EJELIA00302'); /** Lista de Ãºltimas da Editoria **/
EJESA.namespace('EJESA.CALL_EJELIA00504'); /** Abas de Subeditorias do Rio **/
EJESA.namespace('EJESA.CALL_EJELIA01204'); /** Lista de Ãºltimas do Canal **/

EJESA.namespace('EJESA.CALL_EJENEA00504'); /** Noticias Relacionadas **/
EJESA.namespace('EJESA.CALL_EJENEA01008'); /** PaginaÃ§Ã£o de noticias **/

EJESA.namespace('EJESA.CALL_EJELIA01606'); /** Vitrine Zoom **/
EJESA.namespace('EJESA.CALL_EJETOA01906'); /** O Dia 24 Horas **/
EJESA.namespace('EJESA.CALL_EJELIA00906'); /** O Componente de Ãºltimas **/
EJESA.namespace('EJESA.CALL_EJELIA01304'); /** O Componente de scroll infinito **/
EJESA.namespace('EJESA.LOAD_REALTIME_CORE'); /** Carrega Biblioteca IBT **/
/* EJESA.namespace('EJESA.WEBSPECTATOR'); */ /** Carrega Biblioteca WEBSPECTATOR **/
EJESA.namespace('EJESA.LOAD_IBTX'); /** Carrega Biblioteca APP IBT **/

EJESA.namespace('EJESA.WSPublicidades');

EJESA.namespace('EJESA.SolRBusca');

/** implementando mÃ³dulo **/
EJESA.Util = (function () {
    'use strict';
    //propriedades privadas
    //mÃ©todos privados
    var zeroPad = function (num) {
        if (num < 10) {
            num = '0' + num;
        }
        return num;
    },  

        getEnviroment = function () {
            var site_enviroment,
                strUrl = ' ' + window.location + ' ',
                edition_prod = /cms\.ejesa\.igcorp\.com\.br/,
                prod_preview = /cms\.ejesa\.igcorp\.com\.br\/edicaoHomes\/preview/,
                edition_qa = /cms\-ejesa\-qa\.igcorp\.com\.br/,
                qa_preview = /cms\-ejesa\-qa\.igcorp\.com\.br\/edicaoHomes\/preview/;

            site_enviroment = 'production';

            if (strUrl.match(edition_prod)) {
                site_enviroment = 'edition_prod';
            }

            if (strUrl.match(prod_preview)) {
                site_enviroment = 'prod_preview';
            }

            if (strUrl.match(edition_qa)) {
                site_enviroment = 'edition_qa';
            }

            if (strUrl.match(qa_preview)) {
                site_enviroment = 'qa_preview';
            }

            SITE_ENVIROMENT = site_enviroment;

            return site_enviroment;
        },

        dateSolr = function (data, formato) {
            var ext = /^(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)Z$/.exec(data),
                utc = new Date(),
                mon,
                day,
                hour,
                min,
                local;

            utc.setUTCFullYear(ext[1]);
            utc.setUTCMonth(parseInt(ext[2], 10) - 1);
            utc.setUTCDate(ext[3]);
            utc.setUTCHours(ext[4]);
            utc.setUTCMinutes(ext[5]);
            mon = zeroPad(parseInt(utc.getMonth(), 10) + 1);
            day = zeroPad(utc.getDate());
            hour = zeroPad(utc.getHours());
            min = zeroPad(utc.getMinutes());
            local = utc.getFullYear() + '-' + mon + '-' + day + '-' + hour + '-' + min;
            return local.replace(/^(\d+)-(\d+)-(\d+)-(\d+)-(\d+)$/, formato);
        },

        getXHR = function () {
            var i,
                xhr,
                activexIds = [
                    'MSXML2.XMLHTTP.3.0',
                    'MSXML2.XMLHTTP',
                    'Microsoft.XMLHTTP'
                ];
            if (typeof XMLHttpRequest === "function") { // native XHR
                xhr =  new XMLHttpRequest();
            } else { // IE before 7
                for (i = 0; i < activexIds.length; i += 1) {
                    try {
                        xhr = new ActiveXObject(activexIds[i]);
                        break;
                    } catch (e) {}
                }
            }
            return xhr;
        },

        trim = function (str) {
            return str.replace(/^\s+|\s+$/g, "");
        },

        getMetaProperty = function (name) {
            var i, x, y, att, m = document.getElementsByTagName('meta');
            for (i = 0; i < m.length; i = i + 1) {
                x = m[i].attributes;
                for (y = 0; y < x.length; y = y + 1) {
                    att = x[y];
                    if (att.name === 'property' && att.value === name) {
                        return m[i].content;
                    }
                }
            }
        },

        getMetaName = function (name) {
            var i, x, y, att, m = document.getElementsByTagName('meta');
            for (i = 0; i < m.length; i = i + 1) {
                x = m[i].attributes;
                for (y = 0; y < x.length; y = y + 1) {
                    att = x[y];
                    if (att.name === 'name' && att.value === name) {
                        return m[i].content;
                    }
                }
            }
        },

        setClickNodeList = function (nodes, clique) {
            var i, x;
            for (i = 0; i < nodes.length; i = i + 1) {
                x = nodes[i];
                x.click(clique);
            }
        },

        addClassName = function (objElement, strClass, blnMayAlreadyExist) {

            var arrList,
                strClassUpper,
                i;

            if (objElement.className) {
                arrList = objElement.className.split(' ');
                if (blnMayAlreadyExist) {
                    strClassUpper = strClass.toUpperCase();
                    for (i = 0; i < arrList.length; i = i + 1) {
                        if (arrList[i].toUpperCase() === strClassUpper) {
                            arrList.splice(i, 1);
                            i = i - 1;
                        }
                    }
                }
                arrList[arrList.length] = strClass;
                objElement.className = arrList.join(' ');
            } else {
                objElement.className = strClass;
            }
        },

        addClassByID = function (id, strClass, blnMayAlreadyExist) {

            var arrList = [],
                strClassUpper,
                i,
                y,
                objElement,
                classes = '';

            objElement = document.getElementById(id);
            if (objElement.className) {
                arrList = objElement.className.split(' ');
                if (blnMayAlreadyExist) {
                    strClassUpper = strClass.toUpperCase();
                    for (i = 0; i < arrList.length; i = i + 1) {
                        if (arrList[i].toUpperCase() === strClassUpper) {
                            arrList.splice(i, 1);
                            i = i - 1;
                        }
                    }
                }
                arrList[arrList.length] = strClass;
                objElement.className = arrList.join(' ');
            } else {
                arrList[0] = strClass;
                objElement.className = strClass;
            }
            for (y = 0; y < arrList.length; y = y + 1) {
                if (arrList[y] !== '') {
                    classes += arrList[y];
                    classes += ' ';
                }
            }
            objElement.setAttribute('class', classes);
        },

        addClassNodeList = function (nodes, classe) {
            var i, x;
            for (i = 0; i < nodes.length; i = i + 1) {
                x = nodes[i];
                addClassName(x, classe, true);
            }
        },

        hasClassName = function (objElement, strClass) {

            var arrList,
                strClassUpper,
                i;

            if (objElement.className) {
                arrList = objElement.className.split(' ');
                strClassUpper = strClass.toUpperCase();
                for (i = 0; i < arrList.length; i = i + 1) {
                    if (arrList[i].toUpperCase() === strClassUpper) {
                        return true;
                    }
                }
            }

            return false;
        },

        removeClassName = function (objElement, strClass) {

            var arrList,
                strClassUpper,
                i;

            if (objElement.className) {
                arrList = objElement.className.split(' ');
                strClassUpper = strClass.toUpperCase();
                for (i = 0; i < arrList.length; i = i + 1) {
                    if (arrList[i].toUpperCase() === strClassUpper) {
                        arrList.splice(i, 1);
                        i = i - 1;
                    }
                }
            }
        },

        removeClassByID = function (id, strClass) {

            var arrList,
                strClassUpper,
                i,
                y,
                obj,
                classes = '';

            obj = document.getElementById(id);

            if (obj.className) {
                arrList = obj.className.split(' ');
                strClassUpper = strClass.toUpperCase();
                for (i = 0; i < arrList.length; i = i + 1) {
                    if (arrList[i].toUpperCase() === strClassUpper) {
                        arrList.splice(i, 1);
                        i = i - 1;
                    }
                }
                for (y = 0; y < arrList.length; y = y + 1) {
                    if (arrList[y] !== '') {
                        classes += arrList[y];
                        classes += ' ';
                    }
                }
            }
            obj.setAttribute('class', classes);
        },

        loadScript = function (url, callback) {

            var head, script;
            head = document.getElementsByTagName('head')[0];

            script = document.createElement('script');
            script.type = "application/javascript";
            if (script.readyState) { // IE
                script.onreadystatechange = function () {
                    if (script.readyState === "loaded" || script.readyState === "complete") {
                        script.onreadystatechange = null;
                        callback();
                    }
                };
            } else { // Outros
                script.onload = function () {
                    callback();
                };
            }
            script.src = url;

            head.appendChild(script);
        },

        loadJS = function (url) {

            var head, script;
            head = document.getElementsByTagName('head')[0];

            script = document.createElement('script');
            script.type = "application/javascript";
            script.async = true;
            script.src = url;

            head.appendChild(script);
        },

        fieldsMarked = function (field) {
            var itens = field,
                checks = [],
                i;
            for (i = 0; i < itens.length; i = i + 1) {
                if (itens[i].checked === true) {
                    checks.push(itens[i].value);
                }
            }
            return checks;
        };
    //fim da declaracao var

    //revelando API pÃºblica
    return {
        dateSolr: dateSolr,
        getXHR: getXHR,
        trim: trim,
        getMetaProperty: getMetaProperty,
        getMetaName: getMetaName,
        setClickNodeList: setClickNodeList,
        hasClassName : hasClassName,
        addClassName : addClassName,
        removeClassName : removeClassName,
        addClassNodeList : addClassNodeList,
        loadScript : loadScript,
        fieldsMarked : fieldsMarked,
        removeClassByID : removeClassByID,
        addClassByID : addClassByID,
        getEnviroment : getEnviroment,
        loadJS : loadJS
    };
}());

/** implementando mÃ³dulo **/
/** Lista de Ãºltimas da NotÃ­cia **/
EJESA.CALL_EJELIA00602 = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.EJELIA00602.start();
    };

    init = function () {
        EJESA.Util.loadScript(EJELIA00602, callInit);
    };

    //revelando API pÃºblica
    return {
        init: init
    };
}());

/** implementando mÃ³dulo **/
/** Lista de Ãºltimas do Canal **/
EJESA.CALL_EJELIA00106 = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.EJELIA00106.start();
    };

    init = function () {
        EJESA.Util.loadScript(EJELIA00106, callInit);
    };

    //revelando API pÃºblica
    return {
        init: init
    };
}());

/** implementando mÃ³dulo **/
/** Lista de Ãºltimas Home  **/
EJESA.CALL_EJELIA00202 = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.EJELIA00202.start();
    };

    init = function () {
        EJESA.Util.loadScript(EJELIA00202, callInit);
    };

    //revelando API pÃºblica
    return {
        init: init
    };
}());

/** implementando mÃ³dulo **/
/** componente scroll infinito  **/
EJESA.CALL_EJELIA01304 = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.EJELIA01304.start();
    };

    init = function () {
        EJESA.Util.loadScript(EJELIA01304, callInit);
    };

    //revelando API pÃºblica
    return {
        init: init
    };
}());

/** implementando mÃ³dulo **/
/** Lista de Ãºltimas Home  **/
EJESA.CALL_EJELIA00302 = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.EJELIA00302.start();
    };

    init = function () {
        EJESA.Util.loadScript(EJELIA00302, callInit);
    };

    //revelando API pÃºblica
    return {
        init: init
    };
}());

/** implementando mÃ³dulo **/
/** Noticias Relacionadas **/
EJESA.CALL_EJENEA00504 = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.EJENEA00504.start();
    };

    init = function () {
        EJESA.Util.loadScript(EJENEA00504, callInit);
    };

    //revelando API pÃºblica
    return {
        init: init
    };
}());

/** implementando mÃ³dulo **/
/** Vitrine ZOOM **/
EJESA.CALL_EJELIA01606 = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        start();
    };

    init = function () {
        EJESA.Util.getEnviroment();
        if (SITE_ENVIROMENT === 'production') {
            EJESA.Util.loadScript(EJELIA01606, callInit);
        }
    };

    //revelando API pÃºblica
    return {
        init: init
    };
}());

/** implementando mÃ³dulo **/
/** Abas de Subeditorias do Rio **/
EJESA.CALL_EJELIA00504 = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.EJELIA00504.start();
    };

    init = function () {
        EJESA.Util.loadScript(EJELIA00504, callInit);
    };

    //revelando API pÃºblica
    return {
        init: init
    };
}());

/** implementando mÃ³dulo **/
/** Carrega script IBT para publicidade **/
EJESA.LOAD_REALTIME_CORE = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.Util.loadJS(REALTIME_CORE);
    };

    init = function () {
        EJESA.Util.getEnviroment();
        if (SITE_ENVIROMENT === 'production') {
            callInit();
        }
    };

    //revelando API pÃºblica
    return {
        init: init,
        callInit: callInit
    };
}());


/** implementando mÃ³dulo **/
/** Carrega WEBSPECTATOR **/
EJESA.LOAD_WEBSPECTATOR = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.Util.loadJS(WEBSPECTATOR);
    };

    init = function () {
        EJESA.Util.getEnviroment();
        if (SITE_ENVIROMENT === 'production') {
            callInit();
        }
    };

    //revelando API pÃºblica
    return {
        init: init,
        callInit: callInit
    };
}());


/** implementando mÃ³dulo **/
/** Carrega biblioteca de APP IBT **/
EJESA.LOAD_IBTX = (function () {
    'use strict';
    var callInit, init;

    callInit = function () {
        EJESA.Util.loadJS(IBTX);
    };

    init = function () {
        EJESA.Util.getEnviroment();
        if (SITE_ENVIROMENT === 'production') {
            callInit();
        }
    };

    //revelando API pÃºblica
    return {
        init: init,
        callInit: callInit
    };
}());


/** implementando mÃ³dulo **/
/** Carrega componente O Dia 24 horas **/
EJESA.CALL_EJETOA01906 = (function () {
    'use strict';
    var callInit, init;

    callInit = function () {
        //EJESA.EJETOA01906.init();
    };

    init = function () {
        EJESA.Util.getEnviroment();
        if (SITE_ENVIROMENT === 'production') {
            EJESA.Util.loadScript(EJETOA01906, callInit);
        }
    };

    //revelando API pÃºblica
    return {
        init: init,
        callInit: callInit
    };
}());

/** implementando mÃ³dulo **/
/** Carrega componente de Ãšltimas **/
EJESA.CALL_EJELIA00906 = (function () {
    'use strict';
    var callInit, init;

    callInit = function () {
        EJESA.EJELIA00906.start();
    };

    init = function () {
        EJESA.Util.loadScript(EJELIA00906, callInit);
    };

    //revelando API pÃºblica
    return {
        init: init,
        callInit: callInit
    };
}());



EJESA.CALL_EJENEA01008 = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.EJENEA01008.start();
    };

    init = function () {
        EJESA.Util.loadScript(EJENEA01008, callInit);
    };

    //revelando API pÃºblica
    return {
        init: init
    };
}());


/** implementando mÃ³dulo **/
/** Lista de Ãºltimas do Canal **/
EJESA.CALL_EJELIA01204 = (function () {
    'use strict';
    //mÃ©todos privados
    var callInit, init;

    callInit = function () {
        EJESA.EJELIA01204.start();
    };

    init = function () {
        EJESA.Util.loadScript(EJELIA01204, callInit);
    };

    //revelando API pÃºblica
    return {
        init: init
    };
}());


EJESA.SolRBusca = (function () {

    var QTDNEWS = 5, 

    ajax = function () {
        var xhr;

        if (window.XMLHttpRequest) {
          xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
        return xhr;
    }, 

    executeAjaxBusca = function (protocolo,address) {

        var i, xhr;

        xhr = ajax();

        xhr.onreadystatechange = function () {
            if (xhr.readyState !== 4) {
                return false;
            }
            if (xhr.status !== 200) {
                console.log("Error, status code: " + xhr.status);
                return false;
            }
            if(xhr.readyState === 4 && xhr.status === 200) {
                renderizaBusca(xhr.responseText);
            }
        };

        xhr.open(protocolo, address, true);
        xhr.send("");
    },

    renderizaBusca = function (dados) {
            
        var obj = JSON.parse(dados),
            termos, 
            quantidadeNoticias, 
            myRegexp, 
            match, 
            obj_termos, 
            paginacaoHtml, 
            numPagLista, 
            docs,
            i,
            date_news,
            sections,
            mysection,
            tit,
            html = '';

        termos = obj.responseHeader.params.q;
        myRegexp = /[\"](.*?)[\"]/g;
        match = myRegexp.exec(termos);

        quantidadeNoticias = obj.response.numFound;
        docs = obj.response.docs;

        for (i = 0; i < docs.length; i = i + 1) {
            html += '<div class="result">';
            html += '   <div class="title-result"><a href="' + docs[i].url + '"></a></div>';
            html += '   <div class="detail">';
            if(docs[i].urlImgEmp_140x90 != undefined){
                html += '       <div class="thumbnail"><a href="' + docs[i].url + '"><img src="'+ docs[i].urlImgEmp_140x90 +'"></a></div>';
            }
            html += '       <div class="texto">';
            html += '           <a href="' + docs[i].url + '"><strong>'+ docs[i].titulo +'</strong></a><br>';
            html += '           <a href="' + docs[i].url + '">'+ docs[i].olho +'</a>';
            html += '       </div>';
            html += '   </div>';
            html += '</div>';

        }

        numPagLista = Math.floor(quantidadeNoticias / 20);

        document.getElementById('palavraPesquisa').innerHTML = match[1];
        document.getElementById('resultadoBuscaQtd').innerHTML = quantidadeNoticias;
        document.getElementById('resultadoBusca').innerHTML = html;

        if(quantidadeNoticias > 25){
            document.getElementById('mais-resultados').innerHTML = '<div class="mais-resultados" style="width:250px;background:#006699;margin-left:200px;text-align:center;"><a href="javascript:EJESA.SolRBusca.fazBusca(20);" style="display:block;color:#fff;text-decoration:none;font-size:16px;padding:15px;">Mais resultados</a></div>';
        }
        
    },

    fazBusca = function (limite) {
        QTDNEWS = QTDNEWS + limite;
        var queries = getURLParameter('q');
        
        var alvoLoading = 'resultadoBusca';
        if(QTDNEWS==25){
            alvoLoading = alvoLoading;
            document.getElementById('resultadoBusca').innerHTML = '<span style="display:block;width:30px;height:30px;padding:15px;background:url(http://s0.ejesa.ig.com.br/img/odia/loading-cinza.gif);background-repeat:no-repeat;background-position:45% 50%;"></span>';
        }else{
            alvoLoading = 'carregando-pos';
            document.getElementById('mais-resultados').innerHTML = '<div class="mais-resultados" style="width:250px;background:#006699;margin-left:200px;text-align:center;"><span style="display:block;color:#fff;text-decoration:none;font-size:16px;padding:6px 15px 6px 15px;"><img src="http://s0.ejesa.ig.com.br/img/odia/loading-006699.gif"></span></div>';
        }
        executeAjaxBusca('GET', '/_indice/noticias/select?start=0&size='+ QTDNEWS +'&site=*&comb_termos="'+ queries +'"&wt=json');
    },

    getURLParameter = function (parametro) {
        return decodeURIComponent((new RegExp('[?|&]' + parametro + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
    }

    return {
        fazBusca: fazBusca,
        getURLParameter: getURLParameter
    };

}());


EJESA.WSPublicidades = (function() {


    pegarFilme = function(movieName) {
        if (navigator.appName.indexOf("Microsoft") != -1) {
            return document.querySelectorAll(".topclick iframe")[0].contentWindow[movieName]
        } else {
            return document.querySelectorAll(".topclick iframe")[0].contentDocument[movieName]
        }
    }

    acoesTopClick = function(valor) {
        var objeto = document.querySelectorAll(".topclick iframe")[0];
        var atributoID;
        if (objeto.contentDocument.getElementsByTagName('object').length > 0) {
            atributoID = objeto.contentDocument.getElementsByTagName('object')[0].getAttribute("id");
            if (valor == 'ver') {
                EJESA.WSPublicidades.pegarFilme(atributoID).ver();
            } else {
                EJESA.WSPublicidades.pegarFilme(atributoID).fechar();
            }
        } else {
            if (valor == 'ver') {
                if (typeof objeto.contentWindow.gwd == 'object') {

                    contador = setInterval(EJESA.WSPublicidades.CloseTopClick, 16000);
                    gwd = 1;
                } else {
                    objeto.contentWindow.stage.setFlashVars('acao=ver');
                }
            } else {
                if (typeof objeto.contentWindow.gwd == 'object') {
                    clearInterval(contador);
                } else {
                    objeto.contentWindow.stage.setFlashVars('acao=fechar');
                }
            }
        }
    }


    addRodapeExpansivo = function() {

        setTimeout(function() {

            var cssRodape = "display:block;width:940px;height:75px;bottom:0px;left:50%;margin-left:-470px;position:fixed;z-index:9999999999999;";
            document.getElementById("bottomclick").style.cssText = cssRodape;


            var htmlExpansao = '<div class="bt-expansao" style="position:absolute;z-index:2;cursor:pointer;right:15px;top:26px;">';
                htmlExpansao += '<a href="javascript:void(0);" onclick="EJESA.WSPublicidades.expandirBottomClick();">';
                htmlExpansao += '<img src="http://s0.ejesa.ig.com.br/img/pub/arrow_expansao.png">';
                htmlExpansao += '</a></div>';

                var tempdiv = document.createElement('div');
                tempdiv.innerHTML = htmlExpansao;
                document.getElementById('retraido').appendChild(tempdiv.firstChild);

            var htmlRetracao = '<div class="bt-retracao" style="position:absolute;z-index:2;cursor:pointer;right:15px;top:15px;">';
                htmlRetracao += '<a href="javascript:void(0);" onclick="EJESA.WSPublicidades.retrairBottomClick();">';
                htmlRetracao += '<img src="http://s0.ejesa.ig.com.br/img/pub/arrow_retracao.png">';
                htmlRetracao += '</a></div>';

                var tempdiv = document.createElement('div');
                tempdiv.innerHTML = htmlRetracao;
                document.getElementById('expandido').appendChild(tempdiv.firstChild);

        }, 1000);

    }

    expandirBottomClick = function() {
        $("#bottomclick #retraido").stop().animate({ top: "+=75" }, 500, function () { 
            $("#bottomclick #expandido").stop().animate({  top: "-=300" }, 500)
        });
    }

    retrairBottomClick = function() {
        $("#bottomclick #expandido").stop().animate({ top: "+=300" }, 500, function () { 
            $("#bottomclick #retraido").stop().animate({  top: "-=75" }, 500)
        });
    }

    OpenTopClick = function() {

        topclick = 1;

        if(btnsTopclickLoaded == 0) {
            addBotoesTopClick();
        }

        function addBotoesTopClick() {

            btnsTopclickLoaded = 1;
            var btnsTopclick = '<div><div class="btn-fechar" style="display:none;position:absolute;z-index:2;cursor:pointer;right:5px;top:5px;"><a href="javascript:void(0);" onclick="EJESA.WSPublicidades.acoesTopClick(\'fechar\');"><img src="http://s0.ejesa.ig.com.br/img/pub/btn_fecha_interv.png"></a></div>';
            btnsTopclick += '<div class="ver-novamente" style="display:none; text-align:right;position:absolute;bottom:0px;z-index:1;width:100%;"><a href="javascript:void(0);" style="font-size:10px;font-weight:bold;color:#333;" onclick="EJESA.WSPublicidades.acoesTopClick(\'ver\');">VER NOVAMENTE</a></div></div>';
            var tempdiv = document.createElement('div');
            tempdiv.innerHTML = btnsTopclick;
            document.getElementsByClassName("topclick")[0].appendChild(tempdiv.firstChild);
        }

        document.getElementsByTagName("body")[0].setAttribute('class', 'com-topclick');
        var elementosTopClick = document.querySelectorAll('.topclick, .topclick .btn-fechar');
        var obj = document.getElementsByClassName('topclick');
        setTimeout(function() {
            for (var i = 0; i < elementosTopClick.length; i++) {
                elementosTopClick[i].style.display = "block";
            }
        }, 2000);

        var bt_novamente = document.querySelectorAll('.topclick .ver-novamente');
        for (var i = 0; i < bt_novamente.length; i++) {
            bt_novamente[i].style.display = "none";
        }

        for (var i = 0; i < obj.length; i++) {
            obj[i].style.height = '335px';
            if (gwd == 1) {
                document.querySelectorAll(".topclick iframe")[0].contentWindow.gwd.handleExpand_buttonAction();
            }
        }
    }

    CloseTopClick = function() {

        topclick = 0;

        document.getElementsByTagName("body")[0].removeAttribute("class");

        if (typeof document.querySelectorAll(".topclick iframe")[0].contentWindow.gwd == 'object') {
            clearInterval(contador);
            document.querySelectorAll(".topclick iframe")[0].contentWindow.gwd.handleClose_buttonAction();
        }

        var elementosTopClick = document.querySelectorAll('.topclick .btn-fechar');
        for (var i = 0; i < elementosTopClick.length; i++) {
            elementosTopClick[i].style.display = "none";
        }
        var obj = document.getElementsByClassName('topclick');
        for (var i = 0; i < obj.length; i++) {
            obj[i].style.height = '88px';
            if (gwd == 1) {
                document.querySelectorAll(".topclick iframe")[0].contentWindow.gwd.handleExpand_buttonAction();
            }
        }
        setTimeout(function() {
            var bt_novamente = document.querySelectorAll('.topclick .ver-novamente');
            for (var i = 0; i < bt_novamente.length; i++) {
                bt_novamente[i].style.display = "block";
            }
        }, 1000);

        EJESA.WSPublicidades.adicionarIntervencao();
    }


    adicionarIntervencao = function() {

        if (intervencao == 0) {

            if (document.getElementsByTagName("body")[0].classList.contains("com-topclick") == false) {

                if (topclick == 0) {

                    var cssString = "z-index:9999999999;top:390px;display:none!important;position:absolute;left:50%;margin-left:-468px;overflow:hidden;";
                    document.getElementById("pub-intervencao").style.cssText = cssString;

                    googletag.cmd.push(function() {
                        googletag.pubads().refresh([adIntervencao]);
                    });

                    timerIntervencao = setInterval(EJESA.WSPublicidades.CloseIntervencao, 15000);

                    var btnFechar = '<div class="btn-fechar-intervencao" style="position:absolute;z-index:2;cursor:pointer;right:8px;top:5px;display:block;">';
                    btnFechar += '<a href="javascript:void(0);" onclick="EJESA.WSPublicidades.CloseIntervencao();">';
                    btnFechar += '<img src="http://s0.ejesa.ig.com.br/img/pub/btn_fecha_interv.png">';
                    btnFechar += '</a></div>';

                    var tempdiv = document.createElement('div');
                    tempdiv.innerHTML = btnFechar;
                    document.getElementById('pub-intervencao').appendChild(tempdiv.firstChild);

                }
            }

        } else if (intervencao == 1) {

            var cssString = "z-index:9999999999;top:390px;display:block!important;position:absolute;left:50%;margin-left:-468px;overflow:hidden;";
            document.getElementById("pub-intervencao").style.cssText = cssString;

            timerIntervencao = setInterval(EJESA.WSPublicidades.CloseIntervencao, 15000);

            var btnFechar = '<div class="btn-fechar-intervencao" style="position:absolute;z-index:2;cursor:pointer;right:8px;top:5px;display:block;">';
            btnFechar += '<a href="javascript:void(0);" onclick="EJESA.WSPublicidades.CloseIntervencao();">';
            btnFechar += '<img src="http://s0.ejesa.ig.com.br/img/pub/btn_fecha_interv.png">';
            btnFechar += '</a></div>';

            var tempdiv = document.createElement('div');
            tempdiv.innerHTML = btnFechar;
            document.getElementById('pub-intervencao').appendChild(tempdiv.firstChild);

        }
    }


    CloseIntervencao = function() {
        intervencao = 1;
        clearInterval(timerIntervencao);
        $("#pub-intervencao").remove();

    }


    adicionarDHTML = function() {

        var cssString = "z-index: 9999999999;display:block;position:absolute;left:50%;margin-left:-200px;top:390px;overflow:hidden;width:400px;height:400px;";
        document.getElementById("pub-dhtml").style.cssText = cssString;

        timerDHTML = setInterval(EJESA.WSPublicidades.CloseDHTML, 13000);

        var btFecharDhtml = '<div class="btn-fechar-intervencao" style="position:absolute;z-index:2;cursor:pointer;right:5px;top:5px;">';
        btFecharDhtml += '<a href="javascript:void(0);" onclick="EJESA.WSPublicidades.CloseDHTML();">';
        btFecharDhtml += '<img src="http://s0.ejesa.ig.com.br/img/pub/btn_fecha_interv.png">';
        btFecharDhtml += '</a></div>';

        var tempdiv = document.createElement('div');
        tempdiv.innerHTML = btFecharDhtml;
        document.getElementById('pub-dhtml').appendChild(tempdiv.firstChild);
    }


    CloseDHTML = function() {
        dhtml = 1;
        clearInterval(timerDHTML);
        document.getElementById("pub-dhtml").remove();
    }


    adicionarSuperAdVideo = function() {

        /* CRIACAO DAS DIVS DINAMICAMENTE */
        var divAreaVideo = document.getElementById("video-area");

        var slotSuperVideo = document.createElement('div');
        slotSuperVideo.className = 'video ad-video-container';
        divAreaVideo.appendChild(slotSuperVideo);
        var cssString = "left: 0px; position:absolute; display:none;";
        document.getElementsByClassName("video ad-video-container")[0].style.cssText = cssString;

        var slotPlayer = document.createElement('div');
        slotPlayer.id = 'player';
        slotSuperVideo.appendChild(slotPlayer);

        /* CRIACAO DO PLAYER E BIBLIOTECA DO YOUTUBE */
        var tag = document.createElement('script');
        tag.src = "//www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        /* PEGA O ID DO YOUTUBE */
        var URLyoutube = document.querySelectorAll("#video-area iframe")[0].contentDocument.getElementsByTagName('body')[0].getElementsByClassName("youtube")[0].innerHTML;
        var IDyoutube = URLyoutube.split('?v=');

        window.onYouTubeIframeAPIReady = function() {
            player = new YT.Player('player', {
                height: '100%',
                width: '100%',
                playerVars: {
                    'autoplay': 0,
                    'controls': 0,
                    'modestbranding': 1,
                    'autohide': 2,
                    'wmode': 'opaque',
                    'rel': 0,
                    'enablejsapi': 1,
                    'theme': 'dark',
                    'showinfo': 0,
                    'cc_load_policy': 0,
                    'html5': 1,
                    'disablekb': 1,
                    'iv_load_policy': 3,
                    'loop': 1,
                    'origin': 'http://localhost/'

                },
                videoId: IDyoutube[1],
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
        }

        function onPlayerReady(event) {

            player.setPlaybackQuality('hd720');

            $(document).ready(function() {
                document.getElementsByTagName("body")[0].setAttribute('class', 'super-ad-video');
                setTimeout(function() {
                    $('body, html').animate({
                        scrollTop: 256
                    }, '1000', 'linear', function() {

                        var cssStringContainer = 'height:' + alturaWindow + 'px;width:100%';
                        document.getElementsByClassName("ad-video-container")[0].style.cssText = cssStringContainer;
                        var elementosSuperVideo = document.querySelectorAll('.video-area .btn-fechar, .video-area');
                        for (var i = 0; i < elementosSuperVideo.length; i++) {
                            elementosSuperVideo[i].style.display = "block";
                        }
                        document.getElementById("content").style.marginTop = alturaWindow + 256 + 'px';
                        event.target.playVideo();
                        setTimeout(function() {
                            var btnsVideo = '<div><div class="btn-volume" style="display:block; position:absolute;z-index:10;cursor:pointer;left:30px;bottom:50px;"><a href="javascript:void(0);" onclick="EJESA.WSPublicidades.muteSuperAdVideo();"><img src="http://s0.ejesa.ig.com.br/img/pub/bt_audio_muted_ad.png"></a></div>';
                            btnsVideo += '<div class="btn-veja-noticias" style="display:block; position:absolute;z-index:9;cursor:pointer;left:50%;bottom:45px;margin-left:-47px;"><a href="javascript:void(0);" onclick="EJESA.WSPublicidades.scrollSuperAdVideo();"><img src="http://s0.ejesa.ig.com.br/img/pub/bt_veja_noticias.png"></a></div></div>';
                            var tempdiv = document.createElement('div');
                            tempdiv.innerHTML = btnsVideo;
                            document.getElementsByClassName("ad-video-container")[0].appendChild(tempdiv.firstChild);
                        }, 600);

                    });
                }, 1000);
            });

            event.target.mute();
        }

        var done = false;
        function onPlayerStateChange(event) {
            if (event.data == YT.PlayerState.ENDED && !done) {
                /*event.target.playVideo();*/
                EJESA.WSPublicidades.muteSuperAdVideo();
                EJESA.WSPublicidades.scrollSuperAdVideo();

                ga('Publicidade', 'Click', 'SuperVideo', 'Replay');
            }
        }

        window.muteVideo = function() {
            if (player.isMuted()) {
                player.unMute();
                $('.ad-video-container .btn-volume img').attr('src', 'http://s0.ejesa.ig.com.br/img/pub/bt_audio_ad.png');
                ga('Publicidade', 'Click', 'SuperVideo', 'unMute');
            } else {
                player.mute();
                $('.ad-video-container .btn-volume img').attr('src', 'http://s0.ejesa.ig.com.br/img/pub/bt_audio_muted_ad.png');
                ga('Publicidade', 'Click', 'SuperVideo', 'Mute');
            }
        }

        window.pauseVideo = function() {
            player.pauseVideo();
        }
        window.playVideo = function() {
            player.playVideo();
        }
        window.stopVideo = function() {
            player.stopVideo();
        }
        window.setVolume = function() {
            player.stopVideo();
            $('#content').css('margin-top', '0');
        }

        var divs = $('#video-area');
        var limit = $(window).height();
        $(window).on('scroll', function() {
            var offSetWindow = $(window).scrollTop();
            var alturaWindowScroll = $(window).height() + 256;
            if ($("body, html").hasClass("super-ad-video")) {
                var st = $(this).scrollTop() - 256;
                if (st <= limit) {
                    divs.css({ 'opacity' : (1 - st/limit) });
                }
                if (offSetWindow >= alturaWindowScroll) {
                    EJESA.WSPublicidades.pausarVideo();
                    $('.layer-video a, .btn-volume, btn-veja-noticias').css('display', 'none');
                } else {
                    EJESA.WSPublicidades.playVideo();
                    $('.layer-video a, .btn-volume, btn-veja-noticias').css('display', 'block');
                }

            }
        });


        /* PEGA O HREF DO DFP */
        var URLhref = document.querySelectorAll("#video-area iframe")[0].contentDocument.getElementsByTagName('body')[0].getElementsByClassName("click")[0].getElementsByTagName('a')[0].href;
        if (URLhref) {

            var layerVideo = document.createElement('div');
            layerVideo.innerHTML = '<a href="' + URLhref + '" target="_blank" style="display:block;width:100%;height:100%;"></a>';
            layerVideo.className = 'layer-video';

            var childNode = document.getElementsByClassName("ad-video-container")[0];
            var parentDiv = childNode.parentNode;
            parentDiv.insertBefore(layerVideo, childNode);

            var cssStringLayerVideo = "width:100%; height:100%; z-index:2; position: absolute;";
            document.getElementsByClassName("layer-video")[0].style.cssText = cssStringLayerVideo;
        }

    }

    muteSuperAdVideo = function() {
        muteVideo();
    }
    pausarVideo = function() {
        pauseVideo();
    }
    playVideo = function() {
        playVideo();
    }
    scrollSuperAdVideo = function() {
        $('body, html').stop().animate({
            scrollTop: alturaWindow + 256
        }, '1000', 'linear', function() {
            EJESA.WSPublicidades.pausarVideo();
            if (!player.isMuted()) {
                EJESA.WSPublicidades.muteSuperAdVideo();
            }
        });
    }

    resizeVideoAd = function() {
        var height = $(window).height();
        var cssStringContainer = 'height:' + height + 'px;width:100%;';
        document.getElementsByClassName("ad-video-container")[0].style.cssText = cssStringContainer;
    }

    $(window).resize(function() {
        EJESA.WSPublicidades.resizeVideoAd();
    });

    return {

        pegarFilme: pegarFilme,
        acoesTopClick: acoesTopClick,
        OpenTopClick: OpenTopClick,
        CloseTopClick: CloseTopClick,
        CloseIntervencao: CloseIntervencao,
        adicionarIntervencao: adicionarIntervencao,
        adicionarSuperAdVideo: adicionarSuperAdVideo,
        muteSuperAdVideo: muteSuperAdVideo,
        pausarVideo: pausarVideo,
        playVideo: playVideo,
        scrollSuperAdVideo: scrollSuperAdVideo,
        resizeVideoAd: resizeVideoAd,
        addRodapeExpansivo: addRodapeExpansivo,
        expandirBottomClick: expandirBottomClick,
        retrairBottomClick: retrairBottomClick,
        adicionarDHTML: adicionarDHTML,
        CloseDHTML: CloseDHTML

    };

}());