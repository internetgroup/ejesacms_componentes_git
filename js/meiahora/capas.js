
	var capasMeiahora = (function() {

		var getURLParameter = function (variavel) {
			return decodeURIComponent((new RegExp('[?|&]' + variavel + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
		}

		var passDate = function (data,intervalo) {

			var dataInf = new Date(data);
			dataInf.setDate(dataInf.getDate() - intervalo);
			return dataInf;

		}

		var daysInMonth = function (month,year) {
			return new Date(year, month, 0).getDate();
		}

		var imageExists =  function (image_url){

			var http = new XMLHttpRequest();
			http.open('HEAD', image_url, false);
			http.send();
			return http.status != 404;

		}

		var principalCapa = function () {
			
			$('body').css('background-image','url(http://s0.ejesa.ig.com.br/img/meiahora/fundo-mh.jpg)');
			$('body').css('background-position','0 417px');
			$('body').css('background-repeat','repeat-x');

			var currentDate, 
				diadasemana,
				weekdays_format;
			var dias_semana = new Array('Domingo', 'Segunda','Terça', 'Quarta', 'Quinta','Sexta', 'Sábado');	
			
			if(getURLParameter('dia') == null){

				$.getJSON('http://s0.ejesa.ig.com.br/img/meiahora/capa/info.json',function(result){

					var capaID = result.paper_date;
					$( '.galeria-de-fotos img' ).attr('src', 'http://s0.ejesa.ig.com.br/img/meiahora/capa/'+capaID+'652x740.jpg');
					$( '.CHE33318 .share-download .download-pdf' ).attr('href', 'http://s0.ejesa.ig.com.br/img/meiahora/capa/'+capaID+'652x740.jpg');
					$( '.CHE33318 .share-download .download-pdf' ).attr('download', 'http://s0.ejesa.ig.com.br/img/meiahora/capa/'+capaID+'652x740.jpg');

					var ano = capaID.substr(5, 4);
					var mes = capaID.substr(9, 2);
					var dia = capaID.substr(11, 2);

					var mesInt = parseInt(mes, 10);
					var diaInt = parseInt(dia, 10);

					diadasemana = new Date(ano+'-'+mesInt+'-'+diaInt);

					weekdays_format = dias_semana[diadasemana.getDay()] + ' - ' + dia + '/' + mes + '/' + ano;
					document.getElementById('data-semana').innerHTML = weekdays_format;

				});

			}else{

				var dataHash = getURLParameter('dia');
				var dataSplit = dataHash.split('/');
				var dataPattern = dataSplit[2]+'/'+dataSplit[2]+dataSplit[1]+dataSplit[0]+'_';

				$( '.galeria-de-fotos img' ).attr('src', 'http://s0.ejesa.ig.com.br/img/meiahora/capa/'+dataPattern+'652x740.jpg');
				$( '.CHE33318 .share-download .download-pdf' ).attr('href', 'http://s0.ejesa.ig.com.br/img/meiahora/capa/'+dataPattern+'652x740.jpg');
				$( '.CHE33318 .share-download .download-pdf' ).attr('download', 'http://s0.ejesa.ig.com.br/img/meiahora/capa/'+dataPattern+'652x740.jpg');
				currentDate = dataSplit[0]+'/'+dataSplit[1]+'/'+dataSplit[2];

				var anoInt = dataSplit[2];
				var mesInt = parseInt(dataSplit[1], 10);
				var diaInt = parseInt(dataSplit[0], 10);

				diadasemana = new Date(anoInt+'-'+mesInt+'-'+diaInt);

				weekdays_format = dias_semana[diadasemana.getDay()] + ' - ' + currentDate;
				document.getElementById('data-semana').innerHTML = weekdays_format;

			}

		}

		var mostraCapas = function (start, end) {

			var start, end;

			if(start == '' && end == ''){
				var today = new Date();
				start = passDate(today, 8);
				end = today;
			}else{
				start = new Date(start);
			    end = new Date(end);
			}

			var year = start.getFullYear(),
			    month = start.getMonth(),
			    day = start.getDate();
			var dates = [start];

			while(dates[dates.length-1] < end) {
			  dates.push(new Date(year, month, ++day));
			}

			dates.reverse();

			for(i=1; i < dates.length; i++){

				var ano = dates[i].getFullYear(),
			    	mes = dates[i].getMonth()+1,
			    	dia = dates[i].getDate();

				if(dia < 10){dia = '0' + dia;}
				if(mes < 10){mes = '0' + mes;}

				var url = 'http://meiahora.ig.com.br/capas/?dia='+ dia +'/'+ mes +'/'+ ano;

			  	$('.resultado-capa').append('<li class="gd4 fl bordadefault">'+ 
					'	<h4>'+ dia +'/'+ mes +'/'+ ano +'</h4>'+
					'	<a href="'+ url +'"><img src="http://s0.ejesa.ig.com.br/img/meiahora/capa/'+ ano +'/'+ ano + mes + dia +'_314x361.jpg" width="204" height="232"></a>'+
					'	<div class="sharing-close">'+
					'		<ul>'+
					'			<li class="hidden-ico"><a href="#"><span class="icon facebook">fb</span>Facebook</a></li>'+
					'			<li class="hidden-ico"><a href="#"><span class="icon twitter">tw</span>Twitter</a></li>'+
					'			<li class="hidden-ico"><a href="#"><span class="icon google">g+</span>Google +</a></li>'+
					'			<li>'+
					'				<code><a href="http://s0.ejesa.ig.com.br/img/meiahora/capa/'+ ano +'/'+ ano + mes + dia +'_652x740.jpg" download="meiahora.ig.com.br-capa-'+ ano + mes + dia +'.png"><span class="icon download"></span></a></code>'+
					'				<code><a href="javascript:void(0);" onclick="capasMeiahora.shareCapas(event, \''+ url +'\');"><span class="icon shared"></span></a></code>'+
					'				<code><a href="http://s0.ejesa.ig.com.br/img/meiahora/capa/'+ ano +'/'+ ano + mes + dia +'_652x740.jpg" download="meiahora.ig.com.br-capa-'+ ano + mes + dia +'.png"><span class="icon zoom"></span></a></code>'+
					'			</li>'+
					'		</ul>'+
					'	</div>'+
					'</li>');

			}

		}

		var buscaCapas = function () {

			var ano = document.getElementById('ano').value;
			var mes = document.getElementById('mes').value;
			var dia = document.getElementById('dia').value;

			if(ano != '' && mes != '' && dia != ''){

				$('.resultado-capa').html('<li></li>');

				var url = 'http://meiahora.ig.com.br/capas/?dia='+ dia +'/'+ mes +'/'+ ano;

				$('.resultado-capa').append('<li class="gd4 fl bordadefault">'+ 
					'	<h4>'+ dia +'/'+ mes +'/'+ ano +'</h4>'+
					'	<a href="'+ url +'"><img src="http://s0.ejesa.ig.com.br/img/meiahora/capa/'+ ano +'/'+ ano + mes + dia +'_314x361.jpg" width="204" height="232"></a>'+
					'	<div class="sharing-close">'+
					'		<ul>'+
					'			<li class="hidden-ico"><a href="#"><span class="icon facebook">fb</span>Facebook</a></li>'+
					'			<li class="hidden-ico"><a href="#"><span class="icon twitter">tw</span>Twitter</a></li>'+
					'			<li class="hidden-ico"><a href="#"><span class="icon google">g+</span>Google +</a></li>'+
					'			<li>'+
					'				<code><a href="http://s0.ejesa.ig.com.br/img/meiahora/capa/'+ ano +'/'+ ano + mes + dia +'_652x740.jpg" download="meiahora.ig.com.br-capa-'+ ano + mes + dia +'.png"><span class="icon download"></span></a></code>'+
					'				<code><a href="javascript:void(0);" onclick="capasMeiahora.shareCapas(event, \''+ url +'\');"><span class="icon shared"></span></a></code>'+
					'				<code><a href="http://s0.ejesa.ig.com.br/img/meiahora/capa/'+ ano +'/'+ ano + mes + dia +'_652x740.jpg" download="meiahora.ig.com.br-capa-'+ ano + mes + dia +'.png"><span class="icon zoom"></span></a></code>'+
					'			</li>'+
					'		</ul>'+
					'	</div>'+
					'</li>');

			}else{
				
				if(ano != '' && mes != '' && dia == ''){

					$('.resultado-capa').html('<li></li>');

					var mesInt = parseInt(mes, 10);
					var allDays = daysInMonth(mesInt,ano);

					for (var i=allDays; i>=1; i--) {

						var diaI = '';

						if(i<10){diaI = '0' + i;}else{diaI = i;}

						var url = 'http://meiahora.ig.com.br/capas/?dia='+ diaI +'/'+ mes +'/'+ ano;

						$('.resultado-capa').append('<li class="gd4 fl bordadefault">'+ 
							'	<h4>'+ diaI +'/'+ mes +'/'+ ano +'</h4>'+
							'	<a href="'+ url +'"><img src="http://s0.ejesa.ig.com.br/img/meiahora/capa/'+ ano +'/'+ ano + mes + diaI +'_314x361.jpg" width="204" height="232"></a>'+
							'	<div class="sharing-close">'+
							'		<ul>'+
							'			<li class="hidden-ico"><a href="#"><span class="icon facebook">fb</span>Facebook</a></li>'+
							'			<li class="hidden-ico"><a href="#"><span class="icon twitter">tw</span>Twitter</a></li>'+
							'			<li class="hidden-ico"><a href="#"><span class="icon google">g+</span>Google +</a></li>'+
							'			<li>'+
							'				<code><a href="http://s0.ejesa.ig.com.br/img/meiahora/capa/'+ ano +'/'+ ano + mes + diaI +'_652x740.jpg" download="meiahora.ig.com.br-capa-'+ ano + mes + diaI +'.png"><span class="icon download"></span></a></code>'+
							'				<code><a href="javascript:void(0);" onclick="capasMeiahora.shareCapas(event, \''+ url +'\');"><span class="icon shared"></span></a></code>'+
							'				<code><a href="http://s0.ejesa.ig.com.br/img/meiahora/capa/'+ ano +'/'+ ano + mes + diaI +'_652x740.jpg" download="meiahora.ig.com.br-capa-'+ ano + mes + diaI +'.png"><span class="icon zoom"></span></a></code>'+
							'			</li>'+
							'		</ul>'+
							'	</div>'+
							'</li>');

					}

				}else{

					$('.resultado-capa').html('<li class="erro-busca">Escolha ANO e MÊS.</li>');

				}

				return false;

			}

			/*  */

		}

		var shareCapasFacebook = function (url) {
			window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(url), 'facebook-share-dialog', 'width=626,height=436');
			return false;
		}

		var shareCapasTwitter = function (url) {
			window.open('http://twitter.com/intent/tweet?text='+encodeURIComponent(url), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
			return false;
		}

		var shareCapasWhatsapp = function (url) {
			window.open('whatsapp://send?text=Meia Hora - '+encodeURIComponent(url));
			return false;
		}

		var shareCapas = function (event, url) {

			var element =  document.getElementById('div-layer');

			if (typeof(element) != 'undefined' && element != null) {
				
				element.parentNode.removeChild(element);

			}else{

				var layer = document.createElement('div');
				layer.setAttribute('id', 'div-layer');
				layer.style.position = 'absolute';
				layer.style.left = (event.pageX - 47) +'px';
				layer.style.top = (event.pageY) +'px';
				layer.style.width = '105px';
	            layer.style.height = '35px';
	            layer.innerHTML = '<div class="share-capas barra-compartilhamento">'+ 
					'	    <ul class="share-icons">'+ 
					'	        <li class="facebook">'+
					'	            <a href="javascript:void(0);" onclick="capasMeiahora.shareCapasFacebook(\''+ url +'\');" title="Compartilhe no Facebook" class="social-icon-facebook"></a>'+ 
					'	        </li>'+ 
					'	        <li class="twitter">'+ 
					'	            <a href="javascript:void(0);" onclick="capasMeiahora.shareCapasTwitter(\''+ url +'\');" title="Compartilhe no Twitter" class="social-icon-twitter"></a>'+ 
					'	        </li>'+ 
					'	        <li class="whatsapp">'+ 
					'	            <a href="javascript:void(0);" onclick="capasMeiahora.shareCapasWhatsapp(\''+ url +'\');" class="social-icon-whatsapp"></a>'+ 
					'	        </li>'+ 
					'	    </ul>'+ 
					'	</div>'
				document.body.appendChild(layer);

			}

		}

		var navegaCapas = function () {

			/*  */

		}

		return {
	        mostraCapas: mostraCapas,
	        buscaCapas: buscaCapas,
	        navegaCapas: navegaCapas,
	        principalCapa: principalCapa,
	        shareCapas: shareCapas,
	        shareCapasFacebook: shareCapasFacebook,
	        shareCapasTwitter: shareCapasTwitter,
	        shareCapasWhatsapp: shareCapasWhatsapp
	    }

	})();