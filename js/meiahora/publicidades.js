var adsMeiahora = (function() {

    var meiahoraNav,
    meiahoraEnvEdicao,
    meiahoraEnvPreview,
    meiahoraEnvProd, 
    nomeSecao, 
    dfpSite = 'meiahora', 
    dfpId = '1009826';

    meiahoraNav = window.location.href;
    meiahoraEnvPreview = 'http://cms.ejesa.igcorp.com.br/edicaoHomes/preview/';
    meiahoraEnvProd = 'http://meiahora.ig.com.br/';

    var meiahoraAds = (meiahoraNav.match(meiahoraEnvPreview) || meiahoraNav.match(meiahoraEnvProd)) ? true : false;
    var debugOdia = (window.location.search.indexOf('debug') > -1) ? true : false;

    var dfpBlocosdeanuncio = [
        {dfpVar: 'adFullbanner',      dfpLabel: '_fullbanner',   dfpSize: '[468, 60]',   dfpContainer: 'fullbanner'},
        {dfpVar: 'adRetmedio1',       dfpLabel: '_retmedio1',    dfpSize: '[300, 250]',  dfpContainer: 'retangulo_medio_1'},
        {dfpVar: 'adRetmedio2',       dfpLabel: '_retmedio2',    dfpSize: '[300, 250]',  dfpContainer: 'retangulo_medio_2'},
        {dfpVar: 'adRetmedio3',       dfpLabel: '_retmedio3',    dfpSize: '[300, 250]',  dfpContainer: 'retangulo_medio_3'},
        {dfpVar: 'adRetmedio4',       dfpLabel: '_retmedio4',    dfpSize: '[300, 250]',  dfpContainer: 'retangulo_medio_4'},
        {dfpVar: 'adSuperbanner',     dfpLabel: '_superbanner',  dfpSize: '[728, 90]',   dfpContainer: 'superbanner'},
        {dfpVar: 'adTopClick',        dfpLabel: '_topclick',     dfpSize: '[940, 300]',  dfpContainer: 'topclick'},
        {dfpVar: 'adDhtml',           dfpLabel: '_dhtml',        dfpSize: '[400, 400]',  dfpContainer: 'pub-dhtml'},
        {dfpVar: 'adIntervencao',     dfpLabel: '_intervencao',  dfpSize: '[940, 465]',  dfpContainer: 'pub-intervencao'},
        {dfpVar: 'adRodapeRetraido',  dfpLabel: '_expand',       dfpSize: '[940, 75]',   dfpContainer: 'retraido'},
        {dfpVar: 'adRodapeExpandido', dfpLabel: '_expand',       dfpSize: '[940, 300]',  dfpContainer: 'expandido'},
        {dfpVar: 'adSuperVideo',      dfpLabel: '_video',        dfpSize: '[1, 1]',      dfpContainer: 'video-area'},
        {dfpVar: 'adTextLink',        dfpLabel: '_textlink',     dfpSize: '[610, 270]',  dfpContainer: 'text_link'}
    ];

    var getSectionName = function () {

        switch (true) {

            case /^http:\/\/meiahora.ig.com.br\/gata-da-hora\//.test(window.location.href):
                nomeSecao = 'gatadahora';
            break;

            case /^http:\/\/meiahora.ig.com.br\/policia\//.test(window.location.href):
                nomeSecao = 'policia';
            break;

            case /^http:\/\/meiahora.ig.com.br\/galerias\//.test(window.location.href):
                nomeSecao = 'galerias';
            break;

            case /^http:\/\/meiahora.ig.com.br\/capas\//.test(window.location.href):
                nomeSecao = 'capasdojornal';
            break;

            case /^http:\/\/meiahora.ig.com.br\/noticias\//.test(window.location.href):
                nomeSecao = 'noticias';
            break;

            case /^http:\/\/meiahora.ig.com.br\/busca\//.test(window.location.href):
                nomeSecao = 'busca';
            break;

            case /^http:\/\/meiahora.ig.com.br\/contato\//.test(window.location.href):
                nomeSecao = 'contato';
            break;

            case /^http:\/\/meiahora.ig.com.br\/notfound\//.test(window.location.href):
                nomeSecao = 'notfound';
            break;

            case /^http:\/\/meiahora.ig.com.br\/expediente\//.test(window.location.href):
                nomeSecao = 'expediente';
            break;

            case /^http:\/\/meiahora.ig.com.br\/esporte\//.test(window.location.href):
                nomeSecao = 'esporte';
            break;

            case /^http:\/\/meiahora.ig.com.br\/se-liga\//.test(window.location.href):
                nomeSecao = 'seliga';
            break;

            case /^http:\/\/meiahora.ig.com.br\//.test(window.location.href):
                nomeSecao = 'home';
            break;

            case /^http:\/\/localhost:8080\//.test(window.location.href):
                nomeSecao = 'home';
            break;

            case /^http:\/\/localhost\//.test(window.location.href):
                nomeSecao = 'home';
            break;

        }

        if(typeof nomeSecao === 'undefined' && debugOdia === true){
            console.error('************************************');
            console.error('* nomeSecao is undefined.  Fix it! *');
            console.error('************************************');
        }

        return nomeSecao;

    }

    var setDFPSlots = function () {

        getSectionName();

        googletag.cmd.push(function() {

            // googletag.pubads().enableSingleRequest();
            googletag.pubads().enableAsyncRendering();
            googletag.pubads().disableInitialLoad();
            googletag.pubads().collapseEmptyDivs();
            googletag.enableServices();

            var mapLeader = googletag.sizeMapping().
                addSize([320, 400], [320, 50]).
                addSize([320, 700], [300, 75]).
                addSize([480, 200], [468, 60]).
                addSize([768, 200], [728, 90]).
                addSize([1000, 200], [728,90]).
                build();

            /*
            adBlocks = [];
            for (var i=0; i < dfpBlocosdeanuncio.length; ++i){
                adBlocks[i] = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao + dfpBlocosdeanuncio[i]['dfpLabel'], [dfpBlocosdeanuncio[i]['dfpSize']], dfpBlocosdeanuncio[i]['dfpContainer']).addService(googletag.pubads());
            }
            */
            
            adFullbanner        = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_fullbanner',  [468, 60],  'fullbanner').addService(googletag.pubads());
            adRetmedio1         = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_retmedio1',   [300, 250], 'retangulo_medio_1').addService(googletag.pubads());
            adRetmedio2         = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_retmedio2',   [300, 250], 'retangulo_medio_2').addService(googletag.pubads());
            adRetmedio3         = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_retmedio3',   [300, 250], 'retangulo_medio_3').addService(googletag.pubads());
            adRetmedio4         = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_retmedio4',   [300, 250], 'retangulo_medio_4').addService(googletag.pubads());
            adSuperbanner       = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_superbanner', [728, 90],  'superbanner').defineSizeMapping(mapLeader).addService(googletag.pubads());

            adTopClick          = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_topclick',    [940, 300], 'topclick').addService(googletag.pubads());        
            adDhtml             = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_dhtml',       [400, 400], 'pub-dhtml').addService(googletag.pubads());
            adIntervencao       = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_intervencao', [940, 465], 'pub-intervencao').addService(googletag.pubads());
            adRodapeRetraido    = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_expand',      [940, 75],  'retraido').addService(googletag.pubads());
            adRodapeExpandido   = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_expand',      [940, 300], 'expandido').addService(googletag.pubads());
            adSuperVideo        = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_video',       [1, 1],     'video-area').addService(googletag.pubads());
            adTextLink          = googletag.defineSlot('/' + dfpId + '/' + dfpSite + '_'+ nomeSecao +'_textlink',    [610, 270], 'text_link').addService(googletag.pubads());

            googletag.pubads().enableSingleRequest();
            googletag.enableServices();

            /*
            for (var i=0; i < dfpBlocosdeanuncio.length; ++i){
                googletag.display(adBlocks[i]);
            }
            */

            googletag.display('fullbanner');
            googletag.display('retangulo_medio_1');
            googletag.display('retangulo_medio_2');
            googletag.display('retangulo_medio_3');
            googletag.display('retangulo_medio_4');
            googletag.display('superbanner');

            googletag.display('topclick');
            googletag.display('pub-dhtml');
            googletag.display('pub-intervencao');
            googletag.display('retraido');
            googletag.display('expandido');
            googletag.display('video-area');
            googletag.display('text_link');

            googletag.pubads().addEventListener('slotRenderEnded', function(event) {

                if (event.slot === adTopClick) {
                    console.info('event.slot === adTopClick ', event.slot);
                    var contemTopClick = !event.isEmpty;
                    if(contemTopClick === false) {
                        console.info('contemTopClick === false ', contemTopClick);
                        verificarIntervencao();
                    }else{
                        console.info('contemTopClick === true ', contemTopClick);
                    }
                }

                if (event.slot === adDhtml) {
                    var contemDhtml = !event.isEmpty;
                    if(contemDhtml === true) {
                        adicionarDHTML();
                    } 
                }

                if (event.slot === adRodapeRetraido || event.slot === adRodapeExpandido) {
                    var contemRodape = !event.isEmpty;
                    if(contemRodape === true) {
                        addRodapeExpansivo();
                    } 
                } 

                if (event.slot === adSuperVideo) {
                    var contemSuperVideo = !event.isEmpty;
                    if(contemSuperVideo === true) {
                        setTimeout(function() {
                            adicionarSuperAdVideo();
                        },1000);
                    } 
                } 

            });

            googletag.pubads().refresh([adFullbanner]);
            googletag.pubads().refresh([adRetmedio1]);
            googletag.pubads().refresh([adRetmedio2]);
            googletag.pubads().refresh([adRetmedio3]);
            googletag.pubads().refresh([adRetmedio4]);
            googletag.pubads().refresh([adSuperbanner]);

            googletag.pubads().refresh([adDhtml]);
            googletag.pubads().refresh([adTopClick]);
            googletag.pubads().refresh([adRodapeRetraido]);
            googletag.pubads().refresh([adRodapeExpandido]);
            googletag.pubads().refresh([adSuperVideo]);
            googletag.pubads().refresh([adTextLink]);

            if(debugOdia === true) {
                googletag.pubads().addEventListener('slotRenderEnded', function(event) {
                    console.info('Creative with id: ' + event.creativeId + '\n' +
                        'Creative serviceName: ' + event.serviceName + '\n' +
                        'Creative lineItemId: ' + event.lineItemId + '\n' +
                        'Creative isEmpty: ' + event.isEmpty + '\n' +
                        ' is rendered to slot of size: ' + event.size[0] + 'x' + event.size[1] + '\n\n');
                });
            }

        });

    }

    var verificarIntervencao = function () {
        console.info('verificarIntervencao');
        setTimeout(function() {
            refreshIntervencao();
            setTimeout(function() {
                console.info('verificarIntervencao - setTimeout 2');
                timerIntervencaoRefresh = setInterval(refreshIntervencao, 100);
                adicionarIntervencao();
            },500);
        }, 500);
    }

    var refreshIntervencao = function () {
        googletag.cmd.push(function() {
            googletag.pubads().addEventListener('slotRenderEnded', function(event) {
                if (event.slot === adIntervencao) {
                    contemIntervencao = !event.isEmpty;
                    if(contemIntervencao === true) {
                        intervencao = 1;
                        clearInterval(timerIntervencaoRefresh);
                        retornoIntervencao = retornoDFP(contemIntervencao)
                    }
                }
            });
            if(loadIntervencao === 0) {
                loadIntervencao = 1;
                googletag.pubads().refresh([adIntervencao]);
            }
        });
    }

    var retornoDFP = function (contem) {
        if(contem === false) {
            valor = false;
        } else {
            valor = true;
        }
        return valor;
    }

    var pegarFilme = function (movieName) {
        if (navigator.appName.indexOf("Microsoft") != -1) {
            return document.querySelectorAll(".topclick iframe")[0].contentWindow[movieName]
        } else {
            return document.querySelectorAll(".topclick iframe")[0].contentDocument[movieName]
        }
    }

    var acoesTopClick = function (valor) {
        var objeto = document.querySelectorAll(".topclick iframe")[0];
        var atributoID;
        if (objeto.contentDocument.getElementsByTagName('object').length > 0) {
            atributoID = objeto.contentDocument.getElementsByTagName('object')[0].getAttribute("id");
            if (valor == 'ver') {
                pegarFilme(atributoID).ver();
            } else {
                pegarFilme(atributoID).fechar();
            }
        } else {
            if (valor == 'ver') {
                if (typeof objeto.contentWindow.gwd == 'object') {

                    contador = setInterval(CloseTopClick, 16000);
                    gwd = 1;
                } else {
                    objeto.contentWindow.stage.setFlashVars('acao=ver');
                }
            } else {
                if (typeof objeto.contentWindow.gwd == 'object') {
                    clearInterval(contador);
                } else {
                    objeto.contentWindow.stage.setFlashVars('acao=fechar');
                }
            }
        }
    }


    var addRodapeExpansivo = function () {

        setTimeout(function() {

            var cssRodape = "display:block;width:940px;height:75px;bottom:0px;left:50%;margin-left:-470px;position:fixed;z-index:9999999999999;";
            document.getElementById("bottomclick").style.cssText = cssRodape;


            var htmlExpansao = '<div class="bt-expansao" style="position:absolute;z-index:2;cursor:pointer;right:15px;top:26px;">';
                htmlExpansao += '<a href="javascript:void(0);" onclick="adsMeiahora.expandirBottomClick();">';
                htmlExpansao += '<img src="http://s0.ejesa.ig.com.br/img/pub/arrow_expansao.png">';
                htmlExpansao += '</a></div>';

                var tempdiv = document.createElement('div');
                tempdiv.innerHTML = htmlExpansao;
                document.getElementById('retraido').appendChild(tempdiv.firstChild);

            var htmlRetracao = '<div class="bt-retracao" style="position:absolute;z-index:2;cursor:pointer;right:15px;top:15px;">';
                htmlRetracao += '<a href="javascript:void(0);" onclick="adsMeiahora.retrairBottomClick();">';
                htmlRetracao += '<img src="http://s0.ejesa.ig.com.br/img/pub/arrow_retracao.png">';
                htmlRetracao += '</a></div>';

                var tempdiv = document.createElement('div');
                tempdiv.innerHTML = htmlRetracao;
                document.getElementById('expandido').appendChild(tempdiv.firstChild);

        }, 1000);

    }

    var expandirBottomClick = function() {
        $("#bottomclick #retraido").stop().animate({ top: "+=75" }, 500, function () { 
            $("#bottomclick #expandido").stop().animate({  top: "-=300" }, 500)
        });
    }

    var retrairBottomClick = function () {
        $("#bottomclick #expandido").stop().animate({ top: "+=300" }, 500, function () { 
            $("#bottomclick #retraido").stop().animate({  top: "-=75" }, 500)
        });
    }

    var OpenTopClick = function () {

        topclick = 1;

        if(btnsTopclickLoaded == 0) {
            addBotoesTopClick();
        }

        function addBotoesTopClick() {

            btnsTopclickLoaded = 1;
            var btnsTopclick = '<div><div class="btn-fechar" style="display:none;position:absolute;z-index:2;cursor:pointer;right:5px;top:5px;"><a href="javascript:void(0);" onclick="adsMeiahora.acoesTopClick(\'fechar\');"><img src="http://s0.ejesa.ig.com.br/img/pub/btn_fecha_interv.png"></a></div>';
            btnsTopclick += '<div class="ver-novamente" style="display:none; text-align:right;position:absolute;bottom:0px;z-index:1;width:100%;"><a href="javascript:void(0);" style="font-size:10px;font-weight:bold;color:#333;" onclick="adsMeiahora.acoesTopClick(\'ver\');">VER NOVAMENTE</a></div></div>';
            var tempdiv = document.createElement('div');
            tempdiv.innerHTML = btnsTopclick;
            document.getElementsByClassName("topclick")[0].appendChild(tempdiv.firstChild);
        }

        document.getElementsByTagName("body")[0].setAttribute('class', 'com-topclick');
        var elementosTopClick = document.querySelectorAll('.topclick, .topclick .btn-fechar');
        var obj = document.getElementsByClassName('topclick');
        setTimeout(function() {
            for (var i = 0; i < elementosTopClick.length; i++) {
                elementosTopClick[i].style.display = "block";
            }
        }, 2000);

        var bt_novamente = document.querySelectorAll('.topclick .ver-novamente');
        for (var i = 0; i < bt_novamente.length; i++) {
            bt_novamente[i].style.display = "none";
        }

        for (var i = 0; i < obj.length; i++) {
            obj[i].style.height = '335px';
            if (gwd == 1) {
                document.querySelectorAll(".topclick iframe")[0].contentWindow.gwd.handleExpand_buttonAction();
            }
        }
    }

    var CloseTopClick = function () {

        topclick = 0;

        document.getElementsByTagName("body")[0].removeAttribute("class");

        if (typeof document.querySelectorAll(".topclick iframe")[0].contentWindow.gwd == 'object') {
            clearInterval(contador);
            document.querySelectorAll(".topclick iframe")[0].contentWindow.gwd.handleClose_buttonAction();
        }

        var elementosTopClick = document.querySelectorAll('.topclick .btn-fechar');
        for (var i = 0; i < elementosTopClick.length; i++) {
            elementosTopClick[i].style.display = "none";
        }
        var obj = document.getElementsByClassName('topclick');
        for (var i = 0; i < obj.length; i++) {
            obj[i].style.height = '88px';
            if (gwd == 1) {
                document.querySelectorAll(".topclick iframe")[0].contentWindow.gwd.handleExpand_buttonAction();
            }
        }
        setTimeout(function() {
            var bt_novamente = document.querySelectorAll('.topclick .ver-novamente');
            for (var i = 0; i < bt_novamente.length; i++) {
                bt_novamente[i].style.display = "block";
            }
        }, 1000);

        adicionarIntervencao();
    }


    var adicionarIntervencao = function () {

        if (intervencao == 0) {

            if (document.getElementsByTagName("body")[0].classList.contains("com-topclick") == false) {

                if (topclick == 0) {

                    var cssString = "z-index:9999999999;top:390px;display:none!important;position:absolute;left:50%;margin-left:-468px;overflow:hidden;";
                    document.getElementById("pub-intervencao").style.cssText = cssString;

                    googletag.cmd.push(function() {
                        googletag.pubads().refresh([adIntervencao]);
                    });

                    timerIntervencao = setInterval(CloseIntervencao, 15000);

                    var btnFechar = '<div class="btn-fechar-intervencao" style="position:absolute;z-index:2;cursor:pointer;right:8px;top:5px;display:block;">';
                    btnFechar += '<a href="javascript:void(0);" onclick="adsMeiahora.CloseIntervencao();">';
                    btnFechar += '<img src="http://s0.ejesa.ig.com.br/img/pub/btn_fecha_interv.png">';
                    btnFechar += '</a></div>';

                    var tempdiv = document.createElement('div');
                    tempdiv.innerHTML = btnFechar;
                    document.getElementById('pub-intervencao').appendChild(tempdiv.firstChild);

                }
            }

        } else if (intervencao == 1) {

            var cssString = "z-index:9999999999;top:390px;display:block!important;position:absolute;left:50%;margin-left:-468px;overflow:hidden;";
            document.getElementById("pub-intervencao").style.cssText = cssString;

            timerIntervencao = setInterval(CloseIntervencao, 15000);

            var btnFechar = '<div class="btn-fechar-intervencao" style="position:absolute;z-index:2;cursor:pointer;right:8px;top:5px;display:block;">';
            btnFechar += '<a href="javascript:void(0);" onclick="adsMeiahora.CloseIntervencao();">';
            btnFechar += '<img src="http://s0.ejesa.ig.com.br/img/pub/btn_fecha_interv.png">';
            btnFechar += '</a></div>';

            var tempdiv = document.createElement('div');
            tempdiv.innerHTML = btnFechar;
            document.getElementById('pub-intervencao').appendChild(tempdiv.firstChild);
        }
    }

    var CloseIntervencao = function () {
        intervencao = 1;
        clearInterval(timerIntervencao);
        $("#pub-intervencao").remove();
    }

    var adicionarDHTML = function () {
        var cssString = "z-index: 9999999999;display:block;position:absolute;left:50%;margin-left:-200px;top:390px;overflow:hidden;width:400px;height:400px;";
        document.getElementById("pub-dhtml").style.cssText = cssString;

        timerDHTML = setInterval(CloseDHTML, 13000);

        var btFecharDhtml = '<div class="btn-fechar-intervencao" style="position:absolute;z-index:2;cursor:pointer;right:5px;top:5px;">';
        btFecharDhtml += '<a href="javascript:void(0);" onclick="adsMeiahora.CloseDHTML();">';
        btFecharDhtml += '<img src="http://s0.ejesa.ig.com.br/img/pub/btn_fecha_interv.png">';
        btFecharDhtml += '</a></div>';

        var tempdiv = document.createElement('div');
        tempdiv.innerHTML = btFecharDhtml;
        document.getElementById('pub-dhtml').appendChild(tempdiv.firstChild);
    }


    var CloseDHTML = function () {
        dhtml = 1;
        clearInterval(timerDHTML);
        document.getElementById("pub-dhtml").remove();
    }

    var adicionarSuperAdVideo = function () {

        /* CRIACAO DAS DIVS DINAMICAMENTE */
        var divAreaVideo = document.getElementById("video-area");

        var slotSuperVideo = document.createElement('div');
        slotSuperVideo.className = 'video ad-video-container';
        divAreaVideo.appendChild(slotSuperVideo);
        var cssString = "left: 0px; position:absolute; display:none;";
        document.getElementsByClassName("video ad-video-container")[0].style.cssText = cssString;

        var slotPlayer = document.createElement('div');
        slotPlayer.id = 'player';
        slotSuperVideo.appendChild(slotPlayer);

        /* CRIACAO DO PLAYER E BIBLIOTECA DO YOUTUBE */
        var tag = document.createElement('script');
        tag.src = "//www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        /* PEGA O ID DO YOUTUBE */
        var URLyoutube = document.querySelectorAll("#video-area iframe")[0].contentDocument.getElementsByTagName('body')[0].getElementsByClassName("youtube")[0].innerHTML;
        var IDyoutube = URLyoutube.split('?v=');

        window.onYouTubeIframeAPIReady = function() {
            player = new YT.Player('player', {
                height: '100%',
                width: '100%',
                playerVars: {
                    'autoplay': 0,
                    'controls': 0,
                    'modestbranding': 1,
                    'autohide': 2,
                    'wmode': 'opaque',
                    'rel': 0,
                    'enablejsapi': 1,
                    'theme': 'dark',
                    'showinfo': 0,
                    'cc_load_policy': 0,
                    'html5': 1,
                    'disablekb': 1,
                    'iv_load_policy': 3,
                    'loop': 1,
                    'origin': 'http://localhost/'

                },
                videoId: IDyoutube[1],
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
        }

        function onPlayerReady(event) {

            player.setPlaybackQuality('hd720');

            $(document).ready(function() {
                document.getElementsByTagName("body")[0].setAttribute('class', 'super-ad-video');
                setTimeout(function() {
                    $('body, html').animate({
                        scrollTop: 256
                    }, '1000', 'linear', function() {

                        var cssStringContainer = 'height:' + alturaWindow + 'px;width:100%';
                        document.getElementsByClassName("ad-video-container")[0].style.cssText = cssStringContainer;
                        var elementosSuperVideo = document.querySelectorAll('.video-area .btn-fechar, .video-area');
                        for (var i = 0; i < elementosSuperVideo.length; i++) {
                            elementosSuperVideo[i].style.display = "block";
                        }
                        document.getElementById("content").style.marginTop = alturaWindow + 256 + 'px';
                        event.target.playVideo();
                        setTimeout(function() {
                            var btnsVideo = '<div><div class="btn-volume" style="display:block; position:absolute;z-index:10;cursor:pointer;left:30px;bottom:50px;"><a href="javascript:void(0);" onclick="adsMeiahora.muteSuperAdVideo();"><img src="http://s0.ejesa.ig.com.br/img/pub/bt_audio_muted_ad.png"></a></div>';
                            btnsVideo += '<div class="btn-veja-noticias" style="display:block; position:absolute;z-index:9;cursor:pointer;left:50%;bottom:45px;margin-left:-47px;"><a href="javascript:void(0);" onclick="adsMeiahora.scrollSuperAdVideo();"><img src="http://s0.ejesa.ig.com.br/img/pub/bt_veja_noticias.png"></a></div></div>';
                            var tempdiv = document.createElement('div');
                            tempdiv.innerHTML = btnsVideo;
                            document.getElementsByClassName("ad-video-container")[0].appendChild(tempdiv.firstChild);
                        }, 600);

                    });
                }, 1000);
            });

            event.target.mute();
        }

        var done = false;
        function onPlayerStateChange(event) {
            if (event.data == YT.PlayerState.ENDED && !done) {
                /*event.target.playVideo();*/
                muteSuperAdVideo();
                scrollSuperAdVideo();

                ga('Publicidade', 'Click', 'SuperVideo', 'Replay');
            }
        }

        window.muteVideo = function() {
            if (player.isMuted()) {
                player.unMute();
                $('.ad-video-container .btn-volume img').attr('src', 'http://s0.ejesa.ig.com.br/img/pub/bt_audio_ad.png');
                ga('Publicidade', 'Click', 'SuperVideo', 'unMute');
            } else {
                player.mute();
                $('.ad-video-container .btn-volume img').attr('src', 'http://s0.ejesa.ig.com.br/img/pub/bt_audio_muted_ad.png');
                ga('Publicidade', 'Click', 'SuperVideo', 'Mute');
            }
        }

        window.pauseVideo = function() {
            player.pauseVideo();
        }
        window.playVideo = function() {
            player.playVideo();
        }
        window.stopVideo = function() {
            player.stopVideo();
        }
        window.setVolume = function() {
            player.stopVideo();
            $('#content').css('margin-top', '0');
        }

        var divs = $('#video-area');
        var limit = $(window).height();
        $(window).on('scroll', function() {
            var offSetWindow = $(window).scrollTop();
            var alturaWindowScroll = $(window).height() + 256;
            if ($("body, html").hasClass("super-ad-video")) {
                var st = $(this).scrollTop() - 256;
                if (st <= limit) {
                    divs.css({ 'opacity' : (1 - st/limit) });
                }
                if (offSetWindow >= alturaWindowScroll) {
                    pausarVideo();
                    $('.layer-video a, .btn-volume, btn-veja-noticias').css('display', 'none');
                } else {
                    playVideo();
                    $('.layer-video a, .btn-volume, btn-veja-noticias').css('display', 'block');
                }

            }
        });


        /* PEGA O HREF DO DFP */
        var URLhref = document.querySelectorAll("#video-area iframe")[0].contentDocument.getElementsByTagName('body')[0].getElementsByClassName("click")[0].getElementsByTagName('a')[0].href;
        if (URLhref) {

            var layerVideo = document.createElement('div');
            layerVideo.innerHTML = '<a href="' + URLhref + '" target="_blank" style="display:block;width:100%;height:100%;"></a>';
            layerVideo.className = 'layer-video';

            var childNode = document.getElementsByClassName("ad-video-container")[0];
            var parentDiv = childNode.parentNode;
            parentDiv.insertBefore(layerVideo, childNode);

            var cssStringLayerVideo = "width:100%; height:100%; z-index:2; position: absolute;";
            document.getElementsByClassName("layer-video")[0].style.cssText = cssStringLayerVideo;
        }

    }

    var muteSuperAdVideo = function () {
        muteVideo();
    }

    var pausarVideo = function () {
        pauseVideo();
    }

    var playVideo = function () {
        playVideo();
    }

    var scrollSuperAdVideo = function () {
        $('body, html').stop().animate({
            scrollTop: alturaWindow + 256
        }, '1000', 'linear', function() {
            pausarVideo();
            if (!player.isMuted()) {
                muteSuperAdVideo();
            }
        });
    }

    var resizeVideoAd = function () {
        var height = $(window).height();
        var cssStringContainer = 'height:' + height + 'px;width:100%;';
        document.getElementsByClassName("ad-video-container")[0].style.cssText = cssStringContainer;
    }

    return {
        setDFPSlots: setDFPSlots
    }


})();

adsMeiahora.setDFPSlots();