var MEIAHORA = MEIAHORA || {};
MEIAHORA.criaNS = function (namespace) {
	var nsparts = namespace.split(".");
	var parent = MEIAHORA;
	if (nsparts[0] === "MEIAHORA") {
		nsparts = nsparts.slice(1);
	}
	for (var i = 0; i < nsparts.length; i++) {
		var partname = nsparts[i];
		if (typeof parent[partname] === "undefined") {
			parent[partname] = {};
		}
		parent = parent[partname];
	}
	return parent;
};

MEIAHORA.criaNS("MEIAHORA.PUBLICIDADE");
MEIAHORA.PUBLICIDADE = (function () {
	'use strict';

	var ws_pecas = {};
	var webspectatorInsertPub, 
	webspectator, 
	abrirDhtml, 
	fecharDhtml, 
	abreTopClick, 
	fechaTopClick, 
	abrirIntervencao,
	fecharIntervencao, 
	addIntervencao;

    webspectatorInsertPub = function (pub_position) {
        if (ws_pecas[pub_position]) {
            return ws_pecas[pub_position].replace(/\s+/g, '');
        }
        return webspectatorInsertPub;
    }

    webspectator = function () {

    	var p;

        var webspectator_pubs = document.getElementsByName("pubs-web-spectator")[0].getAttribute("content") || "",
            webspectator_pubs = webspectator_pubs.toLowerCase();

        if (webspectator_pubs.length > 0) {
           var ws_pubs = webspectator_pubs.split(",");
           for (p in ws_pubs) {
               if (typeof ws_pubs[p] === "string") {
                    var peca = ws_pubs[p].split(":"),
                        peca_key = peca[0],
                        peca_val = peca[1];
                        ws_pecas[peca_key] = peca_val;
               }
           }
        }

        return webspectator;

    }

    abrirDhtml = function () {
        document.getElementById("dhtml").style.display = "block";
    }

    fecharDhtml = function () {
        document.getElementById("dhtml").style.display = "none";
    }

    abreTopClick = function() {

        $("#topclick").css("display", "block");
        $("#topclick").animate({
            height: 335
        }, 500)
    }

    fechaTopClick = function() {

        $("#content").animate({
            top: 20
        }, 500);

        $("#topclick").animate({
            height: 90
        }, 500);

        addIntervencao();
    }

    abrirIntervencao = function() {
        document.getElementById("intervencao").setAttribute("style", "z-index:9999999999; display: block; position: absolute; left:50%; margin-left:-468px; top:100px; overflow:hidden;");                          
    }

    fecharIntervencao = function() {
        document.getElementById("intervencao").setAttribute("style", "z-index:9999999999; display: none !important; position: absolute; left:50%; margin-left:-468px; top:100px; overflow:hidden;");        
    }

    addIntervencao = function() {

        var iframe = document.createElement('iframe');
        iframe.setAttribute("style", "border-width:0px");
        iframe.style.display = "none";
        iframe.style.margin = '0px auto';
        iframe.src = "about:blank";
        document.getElementById('intervencao').appendChild(iframe);
        document.getElementById("intervencao").style.display = "block";

        var frameDoc = iframe.contentDocument ? iframe.contentDocument : iframe.contentWindow ? iframe.contentWindow.document : iframe.document;
        frameDoc.open('text/html', 'replace');
        frameDoc.write("<html><head></head><body style='margin:0px;overflow:hidden;'><scr"+"ipt>var ws_unit = {'id': '"+EJESA.WSPublicidades.webspectatorInsertPub('intervencao')+"', 'dynamic':1}</scr"+"ipt>"+"<scr"+"ipt type='text/javascript' src ='\/\/wfpscripts.webspectator.com/ws-ad.js'></scr"+"ipt></body></html>");
        frameDoc.close();

    }

    return {
        webspectator: webspectator,
        webspectatorInsertPub: webspectatorInsertPub,
        abrirIntervencao: abrirIntervencao,
        fecharIntervencao: fecharIntervencao,
        abreTopClick: abreTopClick,
        fechaTopClick: fechaTopClick,
        abrirDhtml: abrirDhtml,
        fecharDhtml: fecharDhtml,
        addIntervencao: addIntervencao
    };

}());

MEIAHORA.criaNS("MEIAHORA.UTEIS");
MEIAHORA.UTEIS = (function () {

	var fazBusca, 
		getURLParameter, 
		getMetaName, 
		getLinkRel, 
		autoResize, 
		addListener, 
		eventoGa,
		numpagina;

	
	fazBusca = function (limite) {
		/* document.getElementById("bt-mais-noticias").classList.toggle('btn-ajax-loading'); */
		QTDNEWS = QTDNEWS + limite;
		var queries = getURLParameter('q');
		
		var alvoLoading = 'resultadoBusca';
		if(QTDNEWS==25){
			alvoLoading = alvoLoading;
			document.getElementById('resultadoBusca').innerHTML = '<span style="display:block;width:30px;height:30px;padding:15px;background:url(http://s0.ejesa.ig.com.br/img/meiahora/loading-cinza.gif);background-repeat:no-repeat;background-position:45% 50%;"></span>';
		}else{
			alvoLoading = 'carregando-pos';
			document.getElementById('mais-resultados').innerHTML = '<div class="mais-resultados" style="width:250px;background:#da251d;"><span style="display:block;color:#fff;text-decoration:none;font-size:16px;padding:6px 15px 6px 15px;"><img src="http://s0.ejesa.ig.com.br/img/meiahora/loading-DA251D.gif"></span></div>';
		}
		executeAjax('GET', 'http://meiahora.ig.com.br/_indice/noticias/select?start=0&size='+ QTDNEWS +'&site=meiahora&comb_termos="'+ queries +'"&wt=json', 'busca');
	}

	getURLParameter = function (parametro) {
		return decodeURIComponent((new RegExp('[?|&]' + parametro + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
	}

	getMetaName = function (name) {
		var i, x, y, att, m = document.getElementsByTagName('meta');
		for (i = 0; i < m.length; i = i + 1) {
			x = m[i].attributes;
			for (y = 0; y < x.length; y = y + 1) {
				att = x[y];
				if (att.name === 'name' && att.value === name) {
					return m[i].content;
				}
			}
		}
		return getMetaName;
	}

	getLinkRel = function (rel) {
		var i, x, y, att, m = document.getElementsByTagName('link');
		for (i = 0; i < m.length; i = i + 1) {
			x = m[i].attributes;
			for (y = 0; y < x.length; y = y + 1) {
				att = x[y];
				if (att.name === 'rel' && att.value === rel) {
					return m[i].content;
				}
			}
		}
		return getLinkRel;
	}

	autoResize = function (id) {
	    var newheight;
	    var newwidth;

	    if(document.getElementById){
	        newheight=document.getElementById(id).contentWindow.document .body.scrollHeight;
	        newwidth=document.getElementById(id).contentWindow.document .body.scrollWidth;
	    }

	    document.getElementById(id).height= (newheight) + "px";
	    document.getElementById(id).width= (newwidth) + "px";
	}

	addListener = function (element, type, callback) {
		if (element.addEventListener) element.addEventListener(type, callback);
		else if (element.attachEvent) element.attachEvent('on' + type, callback);
	}

	eventoGa = function (category, action, label, value) {
		if(getMetaName('odia-iframe-home') != 'true'){
			ga('send', 'event', category, action, label, value);
		}
	}

	var QTDNEWS = 5,
		SITE = 'meiahora',
		SOLR = 'http://meiahora.ig.com.br/_indice/noticias/select?',
		FORMAT = 'json',
		TIPOCONTEUDO = 'texto',
		start,
		renderiza,
		page = 1,
		pageIdentify,
		makeAddress,
		query,
		executeSearch,
		pageMarked,
		nextPage,
		prevPage,
		resetPagination,
		executeAjax,
		renderizaCapa,
		renderizaPassafoto,
		paginacaoNumero, 
		section_id = getMetaName('mh-section-id');

	ajax = function () {
		var xhr;

        if (window.XMLHttpRequest) {
          xhr = new XMLHttpRequest();
        }
        else {
            xhr = new ActiveXObject("Microsoft.XMLHTTP");
        }
		return xhr;
	};

	makeAddress = function (start) {

		var address = SOLR;

		address += 'start=';
		address += start;
		address += '&size=';
		address += QTDNEWS;
		address += '&site=';
		address += SITE;
		address += '&secoes_EH=';
		address += section_id;
		address += '&wt=';
		address += FORMAT;
		address += '&tipoConteudo=';
		address += TIPOCONTEUDO;

		return address;
	};

	executeSearch = function (address,container) {

		var i, xhr;

		xhr = ajax();

		xhr.onreadystatechange = function () {
			if (xhr.readyState !== 4) {
				return false;
			}
			if (xhr.status !== 200) {
				console.log("Error, status code: " + xhr.status);
				return false;
			}
			if(xhr.readyState === 4 && xhr.status === 200) {
				renderiza(xhr.responseText, container);
			}
		};

		xhr.open("GET", address, true);
		xhr.send("");
	};

	// Função que consulta json e atribui valor ao array de objetos
	executeAjax = function (protocolo,address,tipo) {

		var i, xhr;

		xhr = ajax();

		xhr.onreadystatechange = function () {
			if (xhr.readyState !== 4) {
				return false;
			}
			if (xhr.status !== 200) {
				console.log("Error, status code: " + xhr.status);
				return false;
			}
			if(xhr.readyState === 4 && xhr.status === 200) {
				if(tipo=="busca"){
					renderizaBusca(xhr.responseText);
				}else if(tipo=="gatadahora"){
					renderizaBuscaGatas(xhr.responseText);
				}else if(tipo=="supergaleria"){
					renderizaBuscaGaleria(xhr.responseText);
				}else{
					renderizaPassafoto(xhr.responseText);
				}
			}
		};

		xhr.open(protocolo, address, true);
		xhr.send("");
	};

	renderiza_2 = function (data, indice) {

		/*
			var xml;
	        if (window.XMLHttpRequest) {
	          xml = new XMLHttpRequest();
	        }else{
	            xml = new ActiveXObject("Microsoft.XMLHTTP");
	        }
	        xml.onreadystatechange = function(){
	            if (xml.readyState !== 4) {
	                return false;
	            }
	            if (xml.status !== 200) {
	                console.log("Error, status code: " + xml.status);
	                return false;
	            }
	            renderiza_2(xml.responseText,i);
	        }
	        xml.open("GET",novaUrl,true);
	        xml.send("");
		*/

		var obj = JSON.parse(data);

		docs = obj.response.docs;
        if(typeof obj.imagens != "undefined"){
			$("#EJELIA01204HL li .EJEHLE02401 .headline .headline-img .span-border-hover a img").eq(indice).attr("src",obj.imagens[0].cortes["146x101"].imagem_url)
		}

	};

	renderizaBuscaGaleria = function (dados) {

		console.log('Entrou renderizaBuscaGaleria '+dados);

		// http://odia.ig.com.br/diversao/celebridades/famosos-movimentam-o-aeroporto/562fb5362f2df0ce440021e0_1.json
		var obj = JSON.parse(dados),
			termos, 
			quantidadeNoticias, 
			myRegexp, 
			match, 
			obj_termos, 
			paginacaoHtml, 
			numPagLista, 
			docs,
			i,
			date_news,
			sections,
			mysection,
			tit,
			novaUrl, 
			html = '';

		termos = obj.responseHeader.params.q;
		myRegexp = /[\"](.*?)[\"]/g;
		match = myRegexp.exec(termos);

		quantidadeNoticias = obj.response.numFound;
		docs = obj.response.docs;

		for (i = 0; i < docs.length; i = i + 1) {

			html += '<li style="list-style:none">';
			html += '	<div class="EJEHLE02401">';
			html += '		<div class="headline">';
			html += '			<span class="headline-span"></span>';
			html += '			<div class="headline-img">';
			html += '				<span class="span-border-hover">';
			html += '					<a href="' + docs[i].url + '">';
			html += '						<span class="active"></span>';


				/*
					renderiza_2 = function (data, indice) {
						var obj = JSON.parse(data);
						montaGalerias(obj, indice);
					};
				*/


				novaUrl = docs[i].url;
				novaUrl = novaUrl.substring(0,(novaUrl.length - 5));
				novaUrl = novaUrl+"_1.json";
				// faz novo ajax


			html += '						<img src="" width="140px" height="95px">';
			html += '					</a>';
			html += '				</span>';
			html += '			</div>';
			html += '			<div class="headline-title">';
			html += '				<a href="' + docs[i].url + '">';
			html += 					docs[i].titulo;
			html += '				</a>';
			html += '			</div>';
			html += '		</div>'; 
			html += '	</div>'; 
			html += '</li>';

			

		}

		console.log('Preparando para montar html');

		document.getElementById('lista-empilhamento-galeria').innerHTML = html;

	};

	renderizaBusca = function (dados) {
			
		var obj = JSON.parse(dados),
			termos, 
			quantidadeNoticias, 
			myRegexp, 
			match, 
			obj_termos, 
			paginacaoHtml, 
			numPagLista, 
			docs,
			i,
			date_news,
			sections,
			mysection,
			tit,
			html = '';

		termos = obj.responseHeader.params.q;
		myRegexp = /[\"](.*?)[\"]/g;
		match = myRegexp.exec(termos);

		quantidadeNoticias = obj.response.numFound;
		docs = obj.response.docs;

		for (i = 0; i < docs.length; i = i + 1) {
			html += '<div class="result">';
			html += '	<div class="title-result"><a href="' + docs[i].url + '"></a></div>';
			html += '	<div class="detail">';
			if(docs[i].urlImgEmp_140x90 != undefined){
				html += '		<div class="thumbnail"><a href="' + docs[i].url + '"><img src="'+ docs[i].urlImgEmp_140x90 +'"></a></div>';
			}
			html += '		<div class="texto">';
			html += '			<a href="' + docs[i].url + '"><strong>'+ docs[i].titulo +'</strong></a><br>';
			html += '			<a href="' + docs[i].url + '">'+ docs[i].olho +'</a>';
			html += '		</div>';
			html += '	</div>';
			html += '</div>';

		}

		numPagLista = Math.floor(quantidadeNoticias / 20);

		/*
		if(numPagLista > 10){
			numPagLista = 10;
		}else{
			numPagLista = numPagLista;
		}
		for (x=1; x<=numPagLista; x++) {
			paginacaoHtml += '	<li><a href="javascript:MEIAHORA.UTEIS.fazBusca();">'+x+'</a></li>';
		}
		if(numPagLista > 10){
			paginacaoHtml += '	<li class="next">';
			paginacaoHtml += '		<a href="javascript:MEIAHORA.UTEIS.fazBusca();">Próximo</a>';
			paginacaoHtml += '	</li>';
		}
		*/

		document.getElementById('palavraPesquisa').innerHTML = match[1];
		document.getElementById('resultadoBuscaQtd').innerHTML = quantidadeNoticias;
		document.getElementById('resultadoBusca').innerHTML = html;

		if(quantidadeNoticias > 25){
			document.getElementById('mais-resultados').innerHTML = '<div class="mais-resultados" style="width:250px;background:#da251d;"><a href="javascript:MEIAHORA.UTEIS.fazBusca(20);" style="display:block;color:#fff;text-decoration:none;font-size:16px;padding:15px;">Mais resultados</a></div>';
		}
		
		
	};

	renderizaBuscaGatas = function (dados) {
			
		var obj = JSON.parse(dados), 
			docs, 
			quantidadeNoticias, 
			html = '';

		quantidadeNoticias = obj.count;
		docs = obj.results;

		for (i = 0; i < docs.length; i = i + 1) {
			html += '<li class="gd4 fl bordadefault">';
			html += '	<a href="' + docs[i].url + '">';
			html += '		<img src="' + docs[i].foto_1 + '" width="204" height="153" />';
			html += '		<h4>' + docs[i].titulo + '</h4>';
			html += '	</a>';
			html += '</li>';
		}

		document.getElementById('lista-gatas-ge').innerHTML = html;

		if(quantidadeNoticias > 12){
			document.getElementById('mais-resultados').innerHTML = '<div class="mais-resultados" style="width:250px;background:#da251d;"><a href="javascript:MEIAHORA.UTEIS.fazBusca(20);" style="display:block;color:#fff;text-decoration:none;font-size:16px;padding:15px;">Mais resultados</a></div>';
		}
		
	};

	filtraGatas = function (formKey, nome, data_dia, data_mes, data_ano) {
		/*
			Exemplos: 
				http://meiahora.ig.com.br/bancos_editoriais_search/560e92b7a0a848f46d000001/_find?criteria={"data_da_postagem":{"$regex":"^14/10/.*?","$options":"i"}}&order={"data_do_evento":1}&limit=3
				http://meiahora.ig.com.br/bancos_editoriais_search/560e92b7a0a848f46d000001/_find?criteria={"data_da_postagem":{"$regex":"^13/.*?/.*?","$options":"i"}}&order={"data_do_evento":1}&limit=3
				http://meiahora.ig.com.br/bancos_editoriais_search/560e92b7a0a848f46d000001/_find?criteria={"nome":{"$regex":"Evellin","$options":"i"}}
		*/

		url = '/bancos_editoriais_search/'+formId+'/_find?criteria={'+
			'"data_do_evento":{"$regex":"^'+dataFull.data.dia+'/10/.*? '+dataFull.data.hora+'","$options":"i"}'+
			'}'+
			'&order={"data_do_evento":1}'+ 
			'&limit=3';
	};

	pageIdentify = function () {
		var start = (page - 1) * QTDNEWS;
		return start;
	};

	renderiza = function (dados, container) {

		var obj = JSON.parse(dados),
			docs,
			i,
			limit,
			date_news,
			sections,
			mysection,
			tit,
			html = '';

		docs = obj.response.docs;

		for (i = 0; i < docs.length; i = i + 1) {

			html += '<li>';
			html += '<a href="' + docs[i].url + '">';

			limit = 0;

			if (docs[i].urlImgEmp_idCorteImagem !== undefined) {
				html += '<img src="' + docs[i].urlImgEmp_80x60 + '">';
				limit = 90;
			} else {
				limit = 180;
			}

			tit = '';
			if (docs[i].titulo.length > limit) {
				tit = docs[i].titulo.substring(0, limit) + "...";
			} else {
				tit = docs[i].titulo;
			}


			html += '       <h4>'+ tit +'</h4>';
			html += '   </a>';

			html += '</li>';
		}

		document.getElementById(container).innerHTML = html;
	};

	renderizaCapa = function (dados) {

		var obj = JSON.parse(dados);

		$.getJSON("http://s0.ejesa.ig.com.br/img/meiahora/capa/info.json",function(result){
			$.each(result, function(i, field){
				$( ".galeria-de-fotos img" ).attr("src", "http://s0.ejesa.ig.com.br/img/meiahora/capa/"+obj.paper_date+"652x740.jpg");
			});
		});
	};

	downloadPdfCapa = function(dados){

		var obj = JSON.parse(dados);

		$.getJSON("http://s0.ejesa.ig.com.br/img/meiahora/capa/info.json",function(result){
			$.each(result, function(i, field){
				$( ".share-download a:nth-child(1)").attr("href", "http://s0.ejesa.ig.com.br/img/meiahora/capa/"+obj.paper_date+"652x740.jpg").attr("download",field+"652x740.jpg");
			});
		});
	};

	getScript = function (source, callback) {
	    var script = document.createElement('script');
	    var prior = document.getElementsByTagName('script')[0];
	    script.async = 1;
	    prior.parentNode.insertBefore(script, prior);

	    script.onload = script.onreadystatechange = function( _, isAbort ) {
	        if(isAbort || !script.readyState || /loaded|complete/.test(script.readyState) ) {
	            script.onload = script.onreadystatechange = null;
	            script = undefined;

	            if(!isAbort) { if(callback) callback(); }
	        }
	    };

	    script.src = source;
	};

	start = function (container) {

		var start,
			address;

		start = pageIdentify();
		address = makeAddress(start);
		executeSearch(address, container);

	};

	maisNoticias = function (quantidade, container) {

		QTDNEWS = QTDNEWS + quantidade;
		paginacaoNumero = QTDNEWS / 5;
		/* loga evento a partir da segunda paginação */
		document.getElementById("bt-mais-noticias").setAttribute("data-paginacao", paginacaoNumero);
		start(container);

	};

	nextPage = function (quantidade) {

		page = page + 1;
		if (page > quantidade) {
			page = 1;
		}
		start();

	};

	prevPage = function (quantidade) {

		page = page - 1;
		if (page < 1) {
			page = quantidade;
		}
		start();

	};

	toggle_visibility = function (id) {
	   var e = document.getElementById(id);
	   if(e.style.display == 'block'){
		  e.style.display = 'none';
	   }
	   else{
		  e.style.display = 'block';
	   }
	   return toggle_visibility;
	};


	scrollWin = function () {
		window.scrollTo(0,0);
	};


	getParametros = function (palavra) {
	    palavra = palavra.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + palavra + "=([^&#]*)"),
	    resultados = regex.exec(location.search);
	    return resultados === null ? "" : decodeURIComponent(resultados[1].replace(/\+/g, " "));
	};

	getSolR = function () {

		var section_id = document.querySelector("meta[name=mh-section-id]").content;
        var urlS = '';
            
        if( $("meta[name='mh-section-name']").attr("content") != 'Home' ){
            urlS = 'http://meiahora.ig.com.br/_indice/noticias/select?start=0&size=20&site=meiahora&secoes_EH='+ section_id +'&wt=json';
        }else{
            urlS = 'http://meiahora.ig.com.br/_indice/noticias/select?start=0&size=20&site=meiahora&wt=json';
        }
        
        $.getJSON(urlS, function( data ) {
                
            $.each( data.response.docs, function(key, val) {

                if(typeof val.urlImgEmp_316x198 != 'undefined'){
                    imgEmp = val.urlImgEmp_316x198;
                }else{
                    imgEmp = 'http://s0.ejesa.ig.com.br/img/meiahora/placeholder_meiahora.jpg';
                }

                $('div.container ul.noticias').append('<li class="materia noticia-mais-lidas com-img noticia">'+ 
                    ' <div class="container">'+
                    '  <div class="content">'+
                    '   <figure>'+
                    '    <a href="' + val.url + '" title="' + val.titulo + '">'+
                    '     <img class="imagem-noticia" src="' + imgEmp + '" width="316" height="198" alt="' + val.titulo + '">'+
                    '    </a>'+
                    '   </figure>'+
                    '   <div class="noticia-headline"><span class="chapeu">'+ val.secaoBreadcrumb +'</span><a class="titulo" href="' + val.url + '">' + val.titulo + '</a></div>'+
                    '  </div>'+
                    ' </div>'+
                    '</li>');

            });

        });
	
	};

	return {
		fazBusca: fazBusca,
		getURLParameter: getURLParameter,
		getMetaName: getMetaName,
		getLinkRel: getLinkRel,
		autoResize: autoResize,
		eventoGa: eventoGa,
		start: start,
		toggle_visibility: toggle_visibility,
		renderizaCapa: renderizaCapa,
		executeAjax: executeAjax,
		nextPage: nextPage,
		prevPage: prevPage,
		maisNoticias: maisNoticias,
		getParametros: getParametros,
		renderizaBuscaGatas: renderizaBusca,
		filtraGatas: filtraGatas,
		getSolR: getSolR
	};

}());

MEIAHORA.PUBLICIDADE.webspectator();

document.addEventListener("DOMContentLoaded", function(event) {
	if(MEIAHORA.UTEIS.getMetaName('mh-section-name') == 'Capas'){
		MEIAHORA.UTEIS.executeAjax('GET', 'http://s0.ejesa.ig.com.br/img/meiahora/capa/info.json', 'capa');
	}
});