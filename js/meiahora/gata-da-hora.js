var ngAppGatasdahora = angular.module('ngAppGatasdahora', []);
	ngAppGatasdahora.controller('GatasdahoraController', function ($scope, $http){

		$http.get('http://meiahora.ig.com.br/bancos_editoriais_search/560e92b7a0a848f46d000001/_find?').success(function(data) {

			$scope.gatas = data.results;

		});

		$scope.eventDateFilter = function() { 

			var ano = $scope.dataField.ano;
			var mes = $scope.dataField.mes;
			var dia = $scope.dataField.dia;

			if(typeof ano == 'undefined'){
				ano = '.*';
			}else{
				ano = ano;
			}

			if(typeof mes == 'undefined'){
				mes = '.*';
			}else{
				mes = mes;
			}

			if(typeof dia == 'undefined'){
				dia = '^.{2}';
			}else{
				dia = dia;
			}

			$http.get('http://meiahora.ig.com.br/bancos_editoriais_search/560e92b7a0a848f46d000001/_find?criteria={'+
				'"data_da_postagem":{"$regex":"'+dia+'/'+mes+'/'+ano+'","$options":"i"}'+
					'}').success(function(data) {

				$scope.gatas = data.results;

			});

		}

	});