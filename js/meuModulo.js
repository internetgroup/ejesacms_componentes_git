// antes de subir:
// 1.retirar chamada require
// 2.retirar console.og
// 3.alterar variavel query do módulo EJELIA00602


var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

var EJESA = EJESA || {};
var SOLR_NOTICIAS = 'http://odia.ig.com.br/_indice/noticias/';


EJESA.namespace = function (ns_string) {
	'use strict';
	var parts = ns_string.split('.'),
		parent = EJESA,
		i;

	if (parts[0] === "EJESA") {
		parts = parts.slice(1);
	}

	for (i = 0; i < parts.lenght; i += 1) {
		if (typeof parent[parts[i]] === 'undefined') {
			parent[parts[i]] = {};
		}
		parent = parent[parts[i]];
	}
	return parent;
};

//declarando os módulos
EJESA.namespace('EJESA.Util');
EJESA.namespace('EJESA.EJELIA00602');
EJESA.namespace('EJESA.EJELIA00106');

//implementando módulo útil
EJESA.Util = (function () {
	'use strict';
	//propriedades privadas
	//métodos privados
	var zeroPad = function (num) {
			if (num < 10) {
				num = '0' + num;
			}
			return num;
		},
		dateSolr = function (data, formato) {
			var ext = /^(\d+)-(\d+)-(\d+)T(\d+):(\d+):(\d+)Z$/.exec(data),
				utc = new Date(),
				mon,
				day,
				hour,
				min,
				local;

			utc.setUTCFullYear(ext[1]);
			utc.setUTCMonth(parseInt(ext[2], 10) - 1);
			utc.setUTCDate(ext[3]);
			utc.setUTCHours(ext[4]);
			utc.setUTCMinutes(ext[5]);
			mon = zeroPad(parseInt(utc.getMonth(), 10) + 1);
			day = zeroPad(utc.getDate());
			hour = zeroPad(utc.getHours());
			min = zeroPad(utc.getMinutes());
			local = utc.getFullYear() + '-' + mon + '-' + day + '-' + hour + '-' + min;
			return local.replace(/^(\d+)-(\d+)-(\d+)-(\d+)-(\d+)$/, formato);
		},
		getXHR = function () {
			var i,
				xhr,
				activexIds = [
			        'MSXML2.XMLHTTP.3.0',
			        'MSXML2.XMLHTTP',
			        'Microsoft.XMLHTTP'
				];
		    if (typeof XMLHttpRequest === "function") { // native XHR
		        xhr =  new XMLHttpRequest();
		    } else { // IE before 7
		        for (i = 0; i < activexIds.length; i += 1) {
		            try {
		                xhr = new ActiveXObject(activexIds[i]);
		                break;
		            } catch (e) {}
		        }
		    }
		    return xhr;
		},
		trim = function (str) {
			return str.replace(/^\s+|\s+$/g, "");
		},
		getMetaContent = function (mn) {
			var i, m = document.getElementsByTagName('meta');
			for (i in m) {
				if (m[i][i].name === mn) {
					return m[i].content;
				}
			}
		};
		//fim da declaracao var

	//revelando API pública
	return {
		dateSolr: dateSolr,
		getXHR: getXHR,
		trim: trim,
		getMetaContent: getMetaContent
	};
}());

console.log(EJESA.Util.dateSolr('2013-05-15T10:45:37Z', '$4:$5'));

//implementando módulo útil
EJESA.EJELIA00602 = (function () {
	'use strict';
	//propriedades privadas
	var html = '',
		init,
		renderiza,
		//section_id = $("meta[name='odia-section-id']").attr("content"),
		section_id = '51278b4256efd04740000053',
		sections = '&secoes_EH=' + section_id,
		query =  SOLR_NOTICIAS + 'select?start=0&size=5&site=odia';

	query += sections;
	query += '&wt=json';
	//fim da declaracao var

	//métodos privados
	init = function () {

		var i, xhr;

		xhr = EJESA.Util.getXHR();

	    xhr.onreadystatechange = function () {
	        if (xhr.readyState !== 4) {
	            return false;
	        }
	        if (xhr.status !== 200) {
	            console.log("Error, status code: " + xhr.status);
	            return false;
	        }
	        renderiza(xhr.responseText);
	    };

	    xhr.open("GET", query, true);
	    xhr.send("");

	};

	renderiza = function (data) {

		var obj = JSON.parse(data),
			docs,
			i,
			limit,
			date_news,
			sections,
			mysection,
			tit;

		docs = obj.response.docs;

		for (i = 0; i < docs.length; i = i + 1) {

			html += "<li>";

			html += "<span class='headline-item'><p>";

			limit = 0;
			if (docs[i].urlImgEmp_idCorteImagem !== undefined) {

				html += "<a href='" + docs[i].url + "'>";
				html += "<img src='" + docs[i].urlImgEmp_80x60 + "'>";
				html += "</a>";
				limit = 90;
			} else {
				limit = 180;
			}

			date_news = EJESA.Util.dateSolr(docs[i].startDate, '$4:$5');

			html += "<span class='headline-item-eye'>";
			html += date_news;
			html += " - ";

			sections = docs[i].secaoBreadcrumb.split('›');
			mysection = sections[sections.length - 1];
			mysection = EJESA.Util.trim(mysection);
			html += mysection;

			html += "</br></span>";
			html += "<span class='headline-item-title'>";
			html += "<a href='" + docs[i].url + "'>";

			tit = '';
			if (docs[i].titulo.length > limit) {
				tit = docs[i].titulo.substring(0, limit) + "...";
			} else {
				tit = docs[i].titulo;
			}

			html += tit;
			html += "</a>";
			html += "</span>";
			html += "</p></span>";

			html += "</li>";
		}

		console.log(html);
		//$('#EJELIA00602HL').html(html);
	};

	//revelando API pública
	return {
		init: init
	};
}());

EJESA.EJELIA00602.init();

EJESA.EJELIA00106 = (function () {
	'use strict';
	var QTDNEWS = 6,
		SITE = 'odia',
		SOLR = 'http://odia.ig.com.br/_indice/noticias/select?',
		FORMAT = 'json',
		HEADLINES = '#EJELIA00106HL',
		SECTIONFORM = '#EJELIA00106FORM',
		NOTICIA_ID = '512788b109f9c44c46000020',
		DIVERSAO_ID = '51278b0656efd04740000051',
		ESPORTE_ID = '51278b7e09f9c44c46000029',
		FLAMENGO_ID = '51278b9509f9c44c4600002b',
		BOTAFOGO_ID = '51278c0a56efd04740000059',
		FLUMINENSE_ID = '51278bdb09f9c44c4600002d',
		VASCO_ID = '51278bb956efd04740000057',
		CARNAVAL_ID = '51278b2709f9c44c46000027',
		CELEBRIDADES_ID = '51278b4256efd04740000053',
		TELEVISAO_ID = '51278b5f56efd04740000055',
		RIO_ID = '5127897256efd04740000043',
		ECONOMIA_ID = '512789c456efd04740000046',
		BRASIL_ID = '512789e656efd04740000048',
		MUNDO_ID = '51278a0609f9c44c46000023',
		EDUCACAO_ID = '51278a7f56efd0474000004a',
		OPINIAO_ID = '51278a9409f9c44c46000025',
		AUTOMANIA_ID = '51278abd56efd0474000004c',
		IMOVEIS_ID = '51278aec56efd0474000004f',
		getIDS,
		sectionIdentify,
		pageIdentify,
		makeAddress,
		displaySearch,
		init,
		renderiza,
		executeSearch,
		displayForm,
		validateChecked,
		sumChecked;

	getIDS = function (section) {
		var sections = '';

		if (section === 'noticia') {
			sections += NOTICIA_ID;
			sections += '%20';
			sections += RIO_ID;
			sections += '%20';
			sections += ECONOMIA_ID;
			sections += '%20';
			sections += BRASIL_ID;
			sections += '%20';
			sections += MUNDO_ID;
			sections += '%20';
			sections += EDUCACAO_ID;
			sections += '%20';
			sections += OPINIAO_ID;
			sections += '%20';
			sections += AUTOMANIA_ID;
			sections += '%20';
			sections += IMOVEIS_ID;
		}

		if (section === 'diversao') {
			sections += DIVERSAO_ID;
			sections += '%20';
			sections += CARNAVAL_ID;
			sections += '%20';
			sections += CELEBRIDADES_ID;
			sections += '%20';
			sections += TELEVISAO_ID;
		}

		if (section === 'esporte') {
			sections += ESPORTE_ID;
			sections += '%20';
			sections += FLAMENGO_ID;
			sections += '%20';
			sections += FLUMINENSE_ID;
			sections += '%20';
			sections += BOTAFOGO_ID;
			sections += '%20';
			sections += VASCO_ID;

		}

		if (section === 'flamengo') {
			sections += FLAMENGO_ID;
		}

		if (section === 'fluminense') {
			sections += FLUMINENSE_ID;
		}

		if (section === 'botafogo') {
			sections += BOTAFOGO_ID;
		}

		if (section === 'vasco') {
			sections += VASCO_ID;
		}

		if (section === 'carnaval') {
			sections += CARNAVAL_ID;
		}

		if (section === 'celebridades') {
			sections += CELEBRIDADES_ID;
		}

		if (section === 'televisao') {
			sections += TELEVISAO_ID;
		}

		if (section === 'rio') {
			sections += RIO_ID;
		}

		if (section === 'economia') {
			sections += ECONOMIA_ID;
		}

		if (section === 'brasil') {
			sections += BRASIL_ID;
		}

		if (section === 'mundoeciencia') {
			sections += MUNDO_ID;
		}

		if (section === 'educacao') {
			sections += EDUCACAO_ID;
		}

		if (section === 'opiniao') {
			sections += OPINIAO_ID;
		}

		if (section === 'automania') {
			sections += AUTOMANIA_ID;
		}

		if (section === 'imoveis') {
			sections += IMOVEIS_ID;
		}

		return sections;
	};

	sectionIdentify = function (metaurl) {

		var section = '';

		if (metaurl.match(/\/noticia\//)) {
			section += 'noticia';
		}

		if (metaurl.match(/\/diversao\//)) {
			section += 'diversao';
		}

		if (metaurl.match(/\/esporte\//)) {
			section += 'esporte';
		}

		return section;
	};

	pageIdentify = function (page) {
		var start = (page - 1) * QTDNEWS;
		return start;
	};

	makeAddress = function (start, section) {

		var address = SOLR;

		address += 'start=';
		address += start;
		address += '&size=';
		address += QTDNEWS;
		address += '&site=';
		address += SITE;
		address += '&secoes_EH=';
		address += section;
		address += '&wt=';
		address += FORMAT;

		return address;
	};

	displaySearch = function (search) {
		document.getElementById('HEADLINES').innerHTML(search);
	};

	//métodos privados
	executeSearch = function (address) {

		var i, xhr;

		xhr = EJESA.Util.getXHR();

	    xhr.onreadystatechange = function () {
	        if (xhr.readyState !== 4) {
	            return false;
	        }
	        if (xhr.status !== 200) {
	            console.log("Error, status code: " + xhr.status);
	            return false;
	        }
	        renderiza(xhr.responseText);
	    };

	    xhr.open("GET", address, true);
	    xhr.send("");

	};

	renderiza = function (data) {
		var html = '',
			obj = JSON.parse(data),
			i,
			y,
			sections,
			mysection,
			tit,
			eye,
			date_news,
			docs;

		docs = obj.response.docs;

		for (y = 1; y <= docs.length; y = y + 1) {

			i = y - 1;
			html += "<li>";
			if (y % 3 === 0) {
				html += "<div class='headline smr'>";
			} else {
				html += "<div class='headline cmr'>";
			}
			html += "<span class='headline-span'></span>";
			html += "<div class='headline-filter'>";

			sections = docs[i].secaoBreadcrumb.split('›');
			mysection = sections[sections.length - 1];
			mysection = EJESA.Util.trim(mysection);

			html += mysection;
			html += "</div>";

			if (docs[i].urlImgEmp_idCorteImagem !== undefined) {

				html += "<div class='headline-img'>";
				html += "<span class='span-border-hover'>";
				html += "<span class='active'></span>";
				html += "<a href='" + docs[i].url + "'>";
				html += "<img src='" + docs[i].urlImgEmp_300x120 + "'>";
				html += "</a>";
				html += "</span>";
				html += "</div>";

			} else {

				html += "<div class='headline-block'></div>";

			}

			tit = '';
			if (docs[i].titulo.length > 60) {
				tit = docs[i].titulo.substring(0, 60) + "...";
			} else {
				tit = docs[i].titulo;
			}


			date_news = EJESA.Util.dateSolr(docs[i].startDate, '$3/$2/$1 - $4:$5');

			html += "<div class='headline-date'>";
			html += date_news;
			html += "</div>";
			html += "<div class='headline-title'>";
			html += "<a href='" + docs[i].url + "'>";
			html += tit;
			html += "</a>";
			html += "</div>";

			if (docs[i].urlImgEmp_idCorteImagem === undefined) {

				eye = '';
				if (docs[i].olho.length > 80) {
					eye = docs[i].olho.substring(0, 80) + "...";
				} else {
					eye = docs[i].olho;
				}

				html += "<div class='headline-eye'>";
				html += "<a href='" + docs[i].url + "'>";
				html += eye;
				html += "</a>";
				html += "</div>";

			}

			html += "</li>";
		}

		displaySearch(html);

	};

	displayForm = function (section) {

		var inputform = "<span>filtrar:</span>";

		if (section === 'noticia') {

			inputform += "<input type='checkbox' name='nops' class='snews' value='noticia' checked />";
			inputform += " Tudo";
			inputform += "<input type='checkbox' name='nops' class='snews' value='rio' />";
			inputform += " Rio";
			inputform += "<input type='checkbox' name='nops' class='snews' value='brasil' />";
			inputform += " Brasil";
			inputform += "<input type='checkbox' name='nops' class='snews' value='economia' />";
			inputform += " Economia";
			inputform += "<input type='checkbox' name='nops' class='snews' value='mundoeciencia' />";
			inputform += " Mundo e Ciência";
			inputform += "<input type='checkbox' name='nops' class='snews' value='educacao' />";
			inputform += " Educação";
			inputform += "<input type='checkbox' name='nops' class='snews' value='opiniao' />";
			inputform += " Opinião";
			inputform += "<input type='checkbox' name='nops' class='snews' value='automania' />";
			inputform += " Automania";
			inputform += "<input type='checkbox' name='nops' class='snews' value='imoveis' />";
			inputform += " Imóveis";

		}

		if (section === 'diversao') {

			inputform += "<input type='checkbox' name='dops' class='snews' value='diversao' checked/>";
			inputform += " Tudo";
			inputform += "<input type='checkbox' name='dops' class='snews' value='celebridades' />";
			inputform += " Celebridades";
			inputform += "<input type='checkbox' name='dops' class='snews' value='televisao' />";
			inputform += " Televisão";
			inputform += "<input type='checkbox' name='dops' class='snews' value='carnaval' />";
			inputform += " O Dia na Folia";

		}

		if (section === 'esporte') {

			inputform += "<input type='checkbox' name='eops' class='snews' value='esporte' checked/>";
			inputform += " Tudo";
			inputform += "<input type='checkbox' name='eops' class='snews' value='botafogo' />";
			inputform += " Botafogo";
			inputform += "<input type='checkbox' name='eops' class='snews' value='flamengo' />";
			inputform += " Flamengo";
			inputform += "<input type='checkbox' name='eops' class='snews' value='fluminense' />";
			inputform += " Fluminense";
			inputform += "<input type='checkbox' name='eops' class='snews' value='vasco' />";
			inputform += " Vasco";

		}

		document.getElementById('SECTIONFORM').innerHTML(inputform);
	};

	validateChecked = function () {
		var sections = $("input.snews:checked"),
			i;

		for (i = 0; i < sections.length; i = i + 1) {

			if (sections[i].value === 'esporte') {
				$('.snews').removeAttr('checked');
				$("input[value='esporte']").prop("checked");
				break;
			}

			if (sections[i].value === 'diversao') {
				$('.snews').removeAttr('checked');
				$("input[value='diversao']").prop("checked");
				break;
			}

			if (sections[i].value === 'noticia') {
				$('.snews').removeAttr('checked');
				$("input[value='noticia']").prop("checked");
				break;
			}

		}

		return sections.length;

	};

	sumChecked = function () {
		var sections = $("input.snews:checked"),
			param = '',
			i;

		for (i = 0; i < sections.length; i = i + 1) {
			param += getIDS(sections[i].value);
			if (i !== (sections.length - 1)) {
				param += '%20';
			}
		}

		return param;
	};


	init = function () {

		var section = sectionIdentify($("meta[property='og:url']").attr("content")),
			secoes_EH,
			start,
			address,
			search,
			options_checked;


		displayForm(section);

		secoes_EH = getIDS(section);

		start = pageIdentify(1);

		address = makeAddress(start, secoes_EH);

		search = executeSearch(address);

		$('.snews').click(function () {

			options_checked = validateChecked();
			if (options_checked > 0) {

				secoes_EH = sumChecked();

				start = pageIdentify(1);

				address = makeAddress(start, secoes_EH);

				search = executeSearch(address);

			}

		});

	};

EJESA.EJELIA00106.init();

}());

