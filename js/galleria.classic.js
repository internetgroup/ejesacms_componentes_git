/*
 * Galleria Classic Theme v. 1.5 2010-10-28
 * http://galleria.aino.se
 *
 * Copyright (c) 2010, Aino
 * Licensed under the MIT license.
 */

(function($) {

Galleria.addTheme({
    name: 'classic',
    author: 'Galleria',
    version: '1.5',
    css: '../../css/odia/galleria.odia.classic.css?v=0.10',
    defaults: {
        transition: 'slide',
        thumb_crop: 'height',
        
		// set this to false if you want to show the caption all the time:
        _toggle_info: true
    },
    init: function(options) {
        
        // cache some stuff
       var toggle   = this.$('image-nav-left,image-nav-right,counter'),
		 info     = this.$('info-link,info-close'),
		// info     = this.$('info-link,info-close,info-text'),
           click    = Galleria.TOUCH ? 'touchstart' : 'click';
        
        // show loader & counter with opacity
        this.$('loader').show().css('opacity',.4)

        // some stuff for non-touch browsers
        if (! Galleria.TOUCH ) {
            
            // fade thumbnails
            this.$('thumbnails').children().hover(function() {
                $(this).not('.active').children().stop().fadeTo(100, 1);
            }, function() {
                $(this).not('.active').children().stop().fadeTo(400, .6);
            });
            
            if(this.$("container").width() >= 510 ){
            	this.addIdleState( this.get('image-nav-left'), { left:-494 });
                this.addIdleState( this.get('image-nav-right'), { right:-494 });
            }else{
            	this.addIdleState( this.get('image-nav-left'), { left:-326 });
                this.addIdleState( this.get('image-nav-right'), { right:-326 });
            }
            
//            this.addIdleState( this.get('counter'), { opacity:0 });
        }
        
        
        // bind some stuff
        this.bind(Galleria.THUMBNAIL, function(e) {
            $(e.thumbTarget).parent(':not(.active)').children().css('opacity',.6)
        });
        
        this.bind(Galleria.LOADSTART, function(e) {
            if (!e.cached) {
                this.$('loader').show().fadeTo(200, .4);
            }
            
            this.$('info').toggle( this.hasInfo() );
            
            $(e.thumbTarget).css('opacity',1).parent().siblings().children().css('opacity',.6);
        });
        
        this.bind(Galleria.LOADFINISH, function(e) {
            this.$('loader').fadeOut(200);
        });
    }
});

})(jQuery);
