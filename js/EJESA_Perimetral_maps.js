	var primeiravez = 1;
	var directionsDisplay;
	var directionsService = new google.maps.DirectionsService();
	var map;

	function initialize(inicioTrajeto, destinoTrajeto, trajetoMapa, trajetoSel) {

		directionsDisplay = new google.maps.DirectionsRenderer();
		var riodejaneiro = new google.maps.LatLng(-22.89329, -43.18968);
		var mapOptions = {
			zoom: 14,
			center: riodejaneiro
		}
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		directionsDisplay.setMap(map);

		directionsDisplay.setPanel(document.getElementById("directions_panel"));

	google.maps.event.addListener(directionsDisplay, 'directions_changed',
	  function() {
	      if (currentDirections) {
	          oldDirections.push(currentDirections);
	      }
	  currentDirections = directionsDisplay.getDirections();
	});

		calcRoute(inicioTrajeto, destinoTrajeto, trajetoSel);

	}

	function calcRoute(inicioTrajeto, destinoTrajeto, trajetoSel) {

		var start = inicioTrajeto;
		var end = destinoTrajeto;
		var waypts = [];
		var desvios = [];
		var qtdParadas = $('#EJETOA03506 .trajetos').find('li:nth('+trajetoSel+')').find('span').length;

		for(i = 0; i < qtdParadas; i++) {
				desvios[i] = $('#EJETOA03506 .trajetos').find('li:nth('+trajetoSel+')').find('span:nth('+i+')').text();
				waypts.push({
						location:desvios[i],
						stopover:true});
		}

		var request = {
				origin: start,
				destination: end,
				waypoints: waypts,
				optimizeWaypoints: true,
				travelMode: google.maps.TravelMode.DRIVING
		};


		directionsService.route(request, function(response, status) {
			if (status == google.maps.DirectionsStatus.OK) {
			  directionsDisplay.setDirections(response);
			}
		});
	}

	$('#EJETOA03506 .trajetos').find('li').each(function() {
		$('#EJETOA03506 .trajetos').find('li').click(function(){
			trajetoSel = $(this).index();
			trajetoMapa = $(this).text();
            inicioTrajeto = $(this).attr('data-inicio');
            destinoTrajeto = $(this).attr('data-destino');
            initialize(inicioTrajeto, destinoTrajeto, trajetoMapa, trajetoSel);
		})
	});


	$(document).ready(function() {
		indexTrajeto = $('#EJETOA03506 .trajetos').find('li:nth(1)').index();
		startTrajeto = $('#EJETOA03506 .trajetos').find('li:nth(1)').find('.nometrajeto').text();
		startInicio = $('#EJETOA03506 .trajetos').find('li:nth(1)').attr('data-inicio');
		startDestino = $('#EJETOA03506 .trajetos').find('li:nth(1)').attr('data-destino');

		google.maps.event.addDomListener(window, 'load', initialize(startInicio, startDestino, startTrajeto, indexTrajeto));
		setInterval("aplicaScrollMapa()",2000);

		function aplicaScrollMapa() {
		    $('.adp').slimscroll({
		        height: '400px',
		        width: '250px',
		        color: '#298eff',
		        opacity: '.5',
		        alwaysVisible: true
		    });
		    $('.adp').css('width','230px');
		}

	});
