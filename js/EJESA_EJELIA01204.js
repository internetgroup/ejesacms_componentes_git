/*global document: false */
/*global console*/

var EJESA_2 = EJESA || {};

/** declarando os mÃ³dulos **/
EJESA.namespace('EJESA.EJELIA01204'); /** Lista de Ãºltimas do Canal **/

/** implementando mÃ³dulo **/
/** Lista de Ãºltimas do Canal **/
EJESA.EJELIA01204 = (function () {
	'use strict';
	var inicio = 0,
		QTDNEWS = 12,
		numNoticias = 1,
		SITE_2 = 'odia',
		SOLR_2 = 'http://odia.ig.com.br/_indice/noticias/select?',
		FORMAT_2 = 'json',
		HEADLINES_2 = 'EJELIA01204HL',
		SECTIONFORM_2 = 'EJELIA01204FORM',
		NOTICIA_ID_2 = '512788b109f9c44c46000020',
		DIVERSAO_ID_2 = '51278b0656efd04740000051',
		ESPORTE_ID_2 = '51278b7e09f9c44c46000029',
		FLAMENGO_ID_2 = '51278b9509f9c44c4600002b',
		BOTAFOGO_ID_2 = '51278c0a56efd04740000059',
		FLUMINENSE_ID_2 = '51278bdb09f9c44c4600002d',
		VASCO_ID_2 = '51278bb956efd04740000057',
		CARNAVAL_ID_2 = '51278b2709f9c44c46000027',
		CELEBRIDADES_ID_2 = '51278b4256efd04740000053',
		TELEVISAO_ID_2 = '51278b5f56efd04740000055',
		RIO_ID_2 = '5127897256efd04740000043',
		ECONOMIA_ID_2 = '512789c456efd04740000046',
		BRASIL_ID_2 = '512789e656efd04740000048',
		MUNDO_ID_2 = '51278a0609f9c44c46000023',
		EDUCACAO_ID_2 = '51278a7f56efd0474000004a',
		OPINIAO_ID_2 = '51278a9409f9c44c46000025',
		AUTOMANIA_ID_2 = '51278abd56efd0474000004c',
		IMOVEIS_ID_2 = '51278aec56efd0474000004f',
		getIDS_2,
		sectionIdentify,
		pageIdentify,
		page = 1,
		makeaddress,
		displaysearch,
		start,
		renderiza_2,
		executeSearch,
		displayForm_2,
		validateChecked,
		sumChecked_2,
		removeChecked_2,
		contentFilter,
		nextPage,
		prevPage,
		mudaPagina,
		resetPagination_2,
		pageMarked_2,
		secoes_EH,
		address,
		search,
		start,
		start,
		passaPagina = 'next',
		terminou = false,
		posicao_2 = 0,
		fotoGaleria,
		montaGalerias,
		j = 1,
		docs = "",
		totalNoticias,
		tagsNoticia;

	getIDS_2 = function (section_2) {
		var sections_2 = '';

		if (section_2 === 'noticia') {
			sections_2 += NOTICIA_ID_2;
			sections_2 += '%20';
			sections_2 += RIO_ID_2;
			sections_2 += '%20';
			sections_2 += ECONOMIA_ID_2;
			sections_2 += '%20';
			sections_2 += BRASIL_ID_2;
			sections_2 += '%20';
			sections_2 += MUNDO_ID_2;
			sections_2 += '%20';
			sections_2 += EDUCACAO_ID_2;
			sections_2 += '%20';
			sections_2 += OPINIAO_ID_2;
			sections_2 += '%20';
			sections_2 += AUTOMANIA_ID_2;
			sections_2 += '%20';
			sections_2 += IMOVEIS_ID_2;
		}

		if (section_2 === 'galerias') {
	/*		sections += NOTICIA_ID;
			sections += '%20';
			sections += RIO_ID;
			sections += '%20';
			sections += ECONOMIA_ID;
			sections += '%20';
			sections += BRASIL_ID;
			sections += '%20';
			sections += MUNDO_ID;
			sections += '%20';
			sections += EDUCACAO_ID;
			sections += '%20';
			sections += OPINIAO_ID;
			sections += '%20';
			sections += AUTOMANIA_ID;
			sections += '%20';
			sections += IMOVEIS_ID;
			sections += '%20';
			sections += DIVERSAO_ID;
			sections += '%20';
			sections += CARNAVAL_ID;
			sections += '%20';
			sections += CELEBRIDADES_ID;
			sections += '%20';
			sections += TELEVISAO_ID;
			sections += '%20';
			sections += ESPORTE_ID;
			sections += '%20';
			sections += FLAMENGO_ID;
			sections += '%20';
			sections += FLUMINENSE_ID;
			sections += '%20';
			sections += BOTAFOGO_ID;
			sections += '%20';
			sections += VASCO_ID;
	*/
		}

		if (section_2 === 'diversao') {
			sections_2 += DIVERSAO_ID_2;
			sections_2 += '%20';
			sections_2 += CARNAVAL_ID_2;
			sections_2 += '%20';
			sections_2 += CELEBRIDADES_ID_2;
			sections_2 += '%20';
			sections_2 += TELEVISAO_ID_2;
		}

		if (section_2 === 'esporte') {
			sections_2 += ESPORTE_ID_2;
			sections_2 += '%20';
			sections_2 += FLAMENGO_ID_2;
			sections_2 += '%20';
			sections_2 += FLUMINENSE_ID_2;
			sections_2 += '%20';
			sections_2 += BOTAFOGO_ID_2;
			sections_2 += '%20';
			sections_2 += VASCO_ID_2;

		}

		if (section_2 === 'flamengo') {
			sections_2 += FLAMENGO_ID_2;
		}

		if (section_2 === 'fluminense') {
			sections_2 += FLUMINENSE_ID_2;
		}

		if (section_2 === 'botafogo') {
			sections_2 += BOTAFOGO_ID_2;
		}

		if (section_2 === 'vasco') {
			sections_2 += VASCO_ID_2;
		}

		if (section_2 === 'carnaval') {
			sections_2 += CARNAVAL_ID_2;
		}

		if (section_2 === 'celebridades') {
			sections_2 += CELEBRIDADES_ID_2;
		}

		if (section_2 === 'televisao') {
			sections_2 += TELEVISAO_ID_2;
		}

		if (section_2 === 'rio') {
			sections_2 += RIO_ID_2;
		}

		if (section_2 === 'economia') {
			sections_2 += ECONOMIA_ID_2;
		}

		if (section_2 === 'brasil') {
			sections_2 += BRASIL_ID_2;
		}

		if (section_2 === 'mundoeciencia') {
			sections_2 += MUNDO_ID_2;
		}

		if (section_2 === 'educacao') {
			sections_2 += EDUCACAO_ID_2;
		}

		if (section_2 === 'opiniao') {
			sections_2 += OPINIAO_ID_2;
		}

		if (section_2 === 'automania') {
			sections_2 += AUTOMANIA_ID_2;
		}

		if (section_2 === 'imoveis') {
			sections_2 += IMOVEIS_ID_2;
		}

		return sections_2;
	};

	sectionIdentify = function (metaurl_2) {

		var section_2 = '';

		if (metaurl_2.match(/\/noticia\//)) {
			section_2 += 'noticia';
		}

		if (metaurl_2.match(/\/diversao\//)) {
			section_2 += 'diversao';
		}

		if (metaurl_2.match(/\/esporte\//)) {
			section_2 += 'esporte';
		}

		return section_2;
	};

	pageIdentify = function () {
		var start = (page - 1) * inicio;
		return start;
	};

	parseTag = function (tags) {
		var mytag = '',
			tag,
			mytags,
			i;
		mytags = tags.split('%20');
		for (i = 0; i < mytags.length; i += 1) {
			tag = "\"";
			tag += mytags[i].replace(/_/g, ' ');
			tag += "\"";
			mytag += tag;
		}
		return mytag;
	};

	makeaddress = function (inicio, section_2, QTDNEWS, tagsNoticia) {

		var address = SOLR_2;

		address += 'start=';
		address += inicio;
		address += '&size=';
		address += QTDNEWS;
		address += '&site=*';
		if(tagsNoticia!=''){
			address += '&comb_termos="'+parseTag(tagsNoticia)+'"';
		}
		address += '&wt=';
		address += FORMAT_2;
		address += '&tipoConteudo=supergaleria';

		return address;
	};

	displaysearch = function (search) {
		document.getElementById(HEADLINES_2).innerHTML = search;
	};

	//mÃ©todos privados
	executeSearch = function (address, indice) {

        var xml;
        
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
          xml = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xml = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        xml.onreadystatechange = function(){
            if (xml.readyState !== 4) {
                return false;
            }
            if (xml.status !== 200) {
                console.log("Error, status code: " + xml.status);
                return false;
            }
            renderiza_2(xml.responseText,indice);
                
        }
        xml.open("GET",address,true);
        xml.send("");

	};
	renderiza_2 = function (data, indice) {

		var obj = JSON.parse(data);

		montaGalerias(obj, indice);

	};
	fotoGaleria = function(url, indice){

		executeSearch(url, indice);

	};

	montaGalerias = function(obj, indice){
		var html = '',
		i,
		novaUrl,
		htmlFoto='',
		tit,
		numPaginas = 1;


	
		if(typeof obj.response == "object" && terminou == false && numNoticias < 13){

			if(parseInt(obj.response["numFound"]) >= 12){
				totalNoticias = 13;
			
				numPaginas += parseInt(totalNoticias/12);
			
 				for(var k = 1; k <= numPaginas ; k++){
					document.getElementById('EJELIA01204-hn_pos_'+k).style.display = "block";	
				}
			}
			else{
				totalNoticias = parseInt(obj.response["numFound"]);

				numPaginas = 0;

				document.getElementById('EJELIA01204-arrow-headline-nav-back').style.display = "none";
				document.getElementById('EJELIA01204-arrow-headline-nav-next').style.display = "none";

			}


			docs = obj.response.docs;

			for (i = 0; i < totalNoticias ; i ++){

				if(typeof docs[i] != "undefined"){

					if(i%4 == 0){
						html += "<div style='border-bottom:1px dotted #ccc; width:600px;height:180px; margin-bottom:10px; padding-bottom:20px;float:left'>";
					}

					html += "<li style='list-style:none'>";
					html += "	<div class='EJEHLE02401'>";
					html += "		<div class='headline'>";
					html += "			<span class='headline-span'></span>";
					html += "			<div class='headline-img'>";
					html += "				<span class='span-border-hover'>";
					html += "					<a href='" + docs[i].url + "'>";
					html += "						<span class='active'></span>";
					html += "						<img src='' width='140px' height='95px'>";
					html += "					</a>";
					html += "				</span>";
					html += "			</div>";

					tit = '';
					if (docs[i].titulo.length > 45) {
						tit = docs[i].titulo.substring(0, 45) + " ...";
					} else {
						tit = docs[i].titulo;
					}

					html += "		<div class='headline-title'>";
					html += "			<a href='" + docs[i].url + "'>";
					html += 				tit;
					html += "			</a>";
					html += "		</div>";
					html += "	</div>";
					html += "</li>";	


					if( (i+1)%4 == 0){
						html += "</div>"
					}

					numNoticias = numNoticias+1;

					novaUrl = docs[i].url;

					novaUrl = novaUrl.substring(0,(novaUrl.length - 5));
					novaUrl = novaUrl+"_1.json"


					fotoGaleria(novaUrl, i); 
				}
			}
		}
		else{
			if(typeof obj.imagens != "undefined"){
				$("#EJELIA01204HL li .EJEHLE02401 .headline .headline-img .span-border-hover a img").eq(indice).attr("src",obj.imagens[0].cortes["146x101"].imagem_url)
			}
		}
		if(numNoticias  == (totalNoticias+1)){
			if(numNoticias == 13){
				page = page+1;
			}
			terminou = true;
		}

	
	
		document.getElementById("EJELIA01204HL").innerHTML += html;
			
	};

	displayForm_2 = function (section_2) {

		var	inputform_2 = '<input type="text" placeholder="Filtrar galerias por..."  style="display:none">';

	/*	if (section_2 === 'noticia') {

			inputform_2 += "<form name='secOptions'  style='display:none'>";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='noticia' checked />";
			inputform_2 += " Tudo";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='rio' />";
			inputform_2 += " Rio";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='brasil' />";
			inputform_2 += " Brasil";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='economia' />";
			inputform_2 += " Economia";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='mundoeciencia' />";
			inputform_2 += " Mundo e CiÃªncia";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='educacao' />";
			inputform_2 += " EducaÃ§Ã£o";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='opiniao' />";
			inputform_2 += " OpiniÃ£o";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='automania' />";
			inputform_2 += " Automania";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='imoveis' />";
			inputform_2 += " ImÃ³veis";
			inputform_2 += "</form>";

		}

		if (section_2 === 'diversao') {

			inputform_2 += "<form name='secOptions'  style='display:none'>";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' ";
			inputform_2 += "onClick='javascript:EJESA.EJELIA01204.contentFilter()' ";
			inputform_2 += "value='diversao' checked/>";
			inputform_2 += " Tudo";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='celebridades' />";
			inputform_2 += " Celebridades";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='televisao' />";
			inputform_2 += " TelevisÃ£o";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='carnaval' />";
			inputform_2 += " O Dia na Folia";
			inputform_2 += "</form>";

		}

		if (section_2 === 'esporte') {

			inputform_2 += "<form name='secOptions' style='display:none'>";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='esporte' checked/>";
			inputform_2 += " Tudo";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='botafogo' />";
			inputform_2 += " Botafogo";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='flamengo' />";
			inputform_2 += " Flamengo";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='fluminense' />";
			inputform_2 += " Fluminense";
			inputform_2 += "<input type='checkbox' name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='vasco' />";
			inputform_2 += " Vasco";
			inputform_2 += "</form>";

		}
		else{
*/
			inputform_2 += "<form name='secOptions'  style='display:none'>";
			inputform_2 += "<select id='selectOpt'>";
			inputform_2 += "<option name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='galerias' selected></option>";
			inputform_2 += "<option name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='noticia' ></option>";
			inputform_2 += "<option name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='diversao'></option>";
			inputform_2 += "<option name='dops' class='snews' onClick='javascript:EJESA.EJELIA01204.contentFilter()' value='esporte' ></option>";
			inputform_2 += "</select>";
			inputform_2 += "</form>";

//	}

		inputform_2 += '<input type="submit" value="Submit"  style="display:none">';
		document.getElementById(SECTIONFORM_2).innerHTML = inputform_2;
	};

	removeChecked_2= function (fields_2) {
		var i;
		for (i = 0; i < fields_2.length; i = i + 1) {
			fields_2[i].checked = false;
		}
	};

	validateChecked = function () {
		var fields_2 = document.getElementById("selectOpt").options,
			sections_2 = EJESA.Util.fieldsMarked(fields_2),
			i,
			sections_2 = [];

		
		for (i = 0; i < fields_2.length; i = i+ 1) {

			if (fields_2[i].selected === true) {
				sections_2.push(fields_2[i].value);
			}

			if (fields_2[i].value === 'diversao' ||
					fields_2[i].value === 'esporte' ||
					fields_2[i].value === 'diversao' ||
					fields_2[i].value === 'galerias' 
					) {

				if (fields_2[i].checked === true) {
					removeChecked_2(fields_2);
					fields_2[i].checked = true;
				}
			}
		}

		return sections_2;

	};

	sumChecked_2 = function (sections_2) {
		var param_2 = '',
			i;

		for (i = 0; i < sections_2.length; i = i + 1) {
			param_2 += getIDS_2(sections_2[i]);

			if (i !== (sections_2.length - 1)) {
				param_2 += '%20';
			}
		}

		return param_2;
	};


	start = function () {

		var section_2 = sectionIdentify(EJESA.Util.getMetaProperty("og:url")),
			start,
			search,
			options_checked_2,
			snews_2;


		displayForm_2(section_2);


		secoes_EH = getIDS_2(section_2);

		start = pageIdentify(1);

		address = makeaddress(inicio, secoes_EH, QTDNEWS);

		search = executeSearch(address,"");


		document.getElementById('EJELIA01204-hn_pos_1').style.display = "none";
		document.getElementById('EJELIA01204-hn_pos_2').style.display = "none";
		document.getElementById('EJELIA01204-hn_pos_3').style.display = "none";
		document.getElementById('EJELIA01204-hn_pos_4').style.display = "none";
		document.getElementById('EJELIA01204-hn_pos_5').style.display = "none";
		document.getElementById('EJELIA01204-arrow-headline-nav-back').style.display = "block";
		document.getElementById('EJELIA01204-arrow-headline-nav-next').style.display = "block";

		contentFilter();


	};

	pageMarked_2 = function (page) {
		var mp_2,
			range_2,
			pos0_2 = 0,
			pos1_2 = 0,
			pos2_2 = 0,
			pos3_2 = 0,
			pos4_2 = 0,
			pos5_2 = 0,
			posid_2;

			range_2 = parseInt(page/5);

		if (page > 5) {
			if(page %5 == 1){
				pos1_2 = page;
				pos2_2 = page + 1;
				pos3_2 = page + 2;
				pos4_2 = page + 3;
				pos5_2 = page + 4;	
				posid_2 = page % 5;
	
			}
			else{
				if(page %5 != 0){
					pos1_2 = (range_2*5) + 1;
					pos2_2 = (range_2*5) + 2;
					pos3_2 = (range_2*5) + 3;
					pos4_2 = (range_2*5) + 4;
					pos5_2 = (range_2*5) + 5;	
					posid_2 = page % 5;
				}
				else{
					pos1_2 = (range_2*5) - 4;
					pos2_2 = (range_2*5) - 3;
					pos3_2 = (range_2*5) - 2;
					pos4_2 = (range_2*5) - 1;
					pos5_2 = (range_2*5) ;	

					posid_2 = 5;

				}
				
			}


			mp_2 = 'EJELIA01204-hn_pos_' + posid_2;
		}
		else{
		
			pos1_2 = 1;
			pos2_2 = pos1_2 + 1;
			pos3_2 = pos1_2 + 2;
			pos4_2 = pos1_2 + 3;
			pos5_2 = pos1_2 + 4;

			mp_2 = 'EJELIA01204-hn_pos_' + page;
		}

		document.getElementById('EJELIA01204-hn_pos_1').innerHTML = pos1_2;
		document.getElementById('EJELIA01204-link_1').rel = pos1_2;

		document.getElementById('EJELIA01204-hn_pos_2').innerHTML = pos2_2;
		document.getElementById('EJELIA01204-link_2').rel = pos2_2;

		document.getElementById('EJELIA01204-hn_pos_3').innerHTML = pos3_2;
		document.getElementById('EJELIA01204-link_3').rel = pos3_2;

		document.getElementById('EJELIA01204-hn_pos_4').innerHTML = pos4_2;
		document.getElementById('EJELIA01204-link_4').rel = pos4_2;

		document.getElementById('EJELIA01204-hn_pos_5').innerHTML = pos5_2;
		document.getElementById('EJELIA01204-link_5').rel = pos5_2;


		EJESA.Util.removeClassByID(mp_2, 'headline_nav_off');
		EJESA.Util.addClassByID(mp_2, 'headline_nav_on', true);


	};

	resetPagination_2 = function () {
		EJESA.Util.removeClassByID('EJELIA01204-hn_pos_1', 'headline_nav_on');
		EJESA.Util.removeClassByID('EJELIA01204-hn_pos_2', 'headline_nav_on');
		EJESA.Util.removeClassByID('EJELIA01204-hn_pos_3', 'headline_nav_on');
		EJESA.Util.removeClassByID('EJELIA01204-hn_pos_4', 'headline_nav_on');
		EJESA.Util.removeClassByID('EJELIA01204-hn_pos_5', 'headline_nav_on');

		EJESA.Util.addClassByID('EJELIA01204-hn_pos_1', 'headline_nav_off');
		EJESA.Util.addClassByID('EJELIA01204-hn_pos_2', 'headline_nav_off');
		EJESA.Util.addClassByID('EJELIA01204-hn_pos_3', 'headline_nav_off');
		EJESA.Util.addClassByID('EJELIA01204-hn_pos_4', 'headline_nav_off');
		EJESA.Util.addClassByID('EJELIA01204-hn_pos_5', 'headline_nav_off');
	};


	contentFilter = function () {

		//snews = document.form.input.name('.snews');
		var snews_2 = document.secOptions.dops,
			sections_2;

		sections_2 = validateChecked();

 		if (sections_2.length > 0) {

 			terminou = false;
 			numNoticias = 1;

			document.getElementById("EJELIA01204HL").innerHTML = '';

			secoes_EH = sumChecked_2(sections_2);

			start = pageIdentify();

			resetPagination_2();

			pageMarked_2(page);


			address = makeaddress(inicio, secoes_EH, QTDNEWS);
			search = executeSearch(address, "");

		}

	};

	contentFilterTags = function (tags) {

		//snews = document.form.input.name('.snews');
		var snews_2 = document.secOptions.dops,
			sections_2;

		sections_2 = validateChecked();

 		if (sections_2.length > 0) {

 			terminou = false;
 			numNoticias = 1;

			document.getElementById("EJELIA01204HL").innerHTML = '';

			secoes_EH = sumChecked_2(sections_2);

			start = pageIdentify();

			resetPagination_2();

			pageMarked_2(page);


			address = makeaddress(inicio, secoes_EH, QTDNEWS, tags);
			search = executeSearch(address, "");

		}

	};


	nextPage = function () {

		if(page <=  parseInt(totalNoticias/12)){
			page = page + 1;
			passaPagina = "next";

			inicio = inicio + 12;

			contentFilter();
		}
		
	};

	prevPage = function () {

		if(page != 1){
			page = page - 1;
			passaPagina = "prev";
			inicio = inicio - 12;

			contentFilter();
		}

	};

	mudaPagina = function(rel){

		if(rel > page){
			nextPage();
		}
		else{
			prevPage();
		}
	};

	window.onload = function () {
		resetPagination_2();

		start = pageIdentify();

		address = makeaddress("0", secoes_EH, QTDNEWS);
		search = executeSearch(address,"");

	};

	//revelando API pÃºblica
	return {
		start: start,
		contentFilter: contentFilter,
		contentFilterTags: contentFilterTags,
		nextPage: nextPage,
		prevPage: prevPage,
		mudaPagina: mudaPagina

	};

}());