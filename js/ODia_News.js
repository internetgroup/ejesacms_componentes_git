var size = 14;
function changeFont(action) {
	'use strict';
	if (action === "plus") {
		if (size < 26) {
			size += 1;
		}
	} else {
		if (size > 14) {
			size -= 1;
		}
	}
	document.getElementById('noticia').style.fontSize = size + 'px';
	document.getElementById('noticia').style.lineHeight = (size + 4) + 'px';
}

$(function () {
	'use strict';
	$('.botaoFacebook').attr("href", $('head link[rel=canonical]').attr('href'));
});

function replaceAll(str, de, para){
    var pos = str.indexOf(de);
    while (pos > -1){
		str = str.replace(de, para);
		pos = str.indexOf(de);
	}
    return (str);
}

function openPrint(){
    var urlPagina = window.location.href;
	var n = urlPagina.lastIndexOf("/");

	var novaUrl = urlPagina.replace(urlPagina.substring((n-10),n),replaceAll(urlPagina.substring((n-10),n),'-','/'));
	novaUrl = novaUrl.replace("html", "print");
	
	window.open(novaUrl);
	
}