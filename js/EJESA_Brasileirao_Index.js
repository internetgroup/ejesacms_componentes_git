/*global document */
/*global XMLHttpRequest*/
/*global ActiveXObject*/

var EJESA = EJESA || {},
//https://www.ejesa.com.br/dataodia/listajogosdecampeonato.xml
	CLASSIFICACAO = 'http://s0.ejesa.ig.com.br/js/odia/EJESA_Classificacao.js?v=0.3',
	JOGOS = 'http://s0.ejesa.ig.com.br/js/odia/EJESA_Jogos.js?v=0.1';

EJESA.namespace = function (ns_string) {
	'use strict';
	var parts = ns_string.split('.'),
		parent = EJESA,
		i;

	if (parts[0] === "EJESA") {
		parts = parts.slice(1);
	}

	for (i = 0; i < parts.lenght; i += 1) {
		if (typeof parent[parts[i]] === 'undefined') {
			parent[parts[i]] = {};
		}
		parent = parent[parts[i]];
	}
	return parent;
};

/** declarando os módulos **/
EJESA.namespace('EJESA.BRASILEIRAO2013');
EJESA.namespace('EJESA.BRASILEIRAO2013.Util');
EJESA.namespace('EJESA.BRASILEIRAO2013.CALL_Classificacao'); /** Tabela de classificação **/
EJESA.namespace('EJESA.BRASILEIRAO2013.CALL_Jogos'); /** Lista de Jogos **/

EJESA.BRASILEIRAO2013 = {};

/** implementando módulo **/
EJESA.BRASILEIRAO2013.Util = (function () {
	'use strict';
	//propriedades privadas
	//métodos privados

	var	getXHR = function () {
			var i,
				xhr,
				activexIds = [
			        'MSXML2.XMLHTTP.3.0',
			        'MSXML2.XMLHTTP',
			        'Microsoft.XMLHTTP'
				];
		    if (typeof XMLHttpRequest === "function") { // native XHR
		        xhr =  new XMLHttpRequest();
		    } else { // IE before 7
		        for (i = 0; i < activexIds.length; i += 1) {
		            try {
		                xhr = new ActiveXObject(activexIds[i]);
		                break;
		            } catch (e) {}
		        }
		    }
		    return xhr;
		},

		trim = function (str) {
			return str.replace(/^\s+|\s+$/g, "");
		},

		arraySort = function (list) {

			list.sort(function (a, b) {
			    return (a.rodada.nodeValue - b.rodada.nodeValue);
			});

			return list;
		},

		loadScript = function (url, callback) {

			var head, script;
			head = document.getElementsByTagName('head')[0];

			script = document.createElement('script');
			script.type = "text/javascript";
			if (script.readyState) {
				script.onreadystatechange = function () {
					if (script.readyState === "loaded" || script.readyState === "complete") {
						script.onreadystatechange = null;
						callback();
					}
				};
			} else {
				script.onload = function () {
					callback();
				};
			}
			script.src = url;

			head.appendChild(script);
		};

	//revelando API pública
	return {
		getXHR: getXHR,
		trim: trim,
		loadScript: loadScript,
		arraySort: arraySort
	};
}());

/** implementando módulo **/
/** Classificação do campeonato **/
EJESA.BRASILEIRAO2013.CALL_Classificacao = (function () {
	'use strict';
	//métodos privados
	var callInit, init;

	callInit = function () {
		EJESA.BRASILEIRAO2013.Classificacao.start();
	};

	init = function () {
		EJESA.BRASILEIRAO2013.Util.loadScript(CLASSIFICACAO, callInit);
	};

	//revelando API pública
	return {
		init: init
	};
}());

/** implementando módulo **/
/** Lista de Jogos **/
EJESA.BRASILEIRAO2013.CALL_Jogos = (function () {
	'use strict';
	//métodos privados
	var callInit, init;

	callInit = function () {
		EJESA.BRASILEIRAO2013.Jogos.start();
	};

	init = function () {
		EJESA.BRASILEIRAO2013.Util.loadScript(JOGOS, callInit);
	};

	//revelando API pública
	return {
		init: init
	};
}());
