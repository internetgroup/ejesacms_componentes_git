/*
 * Arquivo que contÃ©m utilitÃ¡rios usados em vÃ¡rios lugares do cÃ³digo.
 */

// A simple logging statement that works in all browsers.
var log = function() {
    try {
        console.log.apply(console, arguments);
    } catch(e) {
        try {
            opera.postError.apply(opera, arguments);
        } catch(e){
            alert(Array.prototype.join.call(arguments, " "));
        }
    }
};

function listHash(){
    var listaDeParametros = [];
    var urlHash = parent.location.hash;
    listaDeParametros = urlHash.split('#');
    listaDeParametros.shift();
    var lista = {};
    for(dt in listaDeParametros){
                   var temp = listaDeParametros[dt].split('=');
                   if(temp[0] == "query"){
                           lista[temp[0]] = replaceAll(temp[1], "_", " ");
                   }else{
                           lista[temp[0]] = temp[1];
                   }
    }
    return lista;
};

function updateHashObject(criterios){
        $.each( criterios, function(key, value){
                if(key != null && value != null){
                        updateHash(key, value);
                }
        });
}

function replaceAll(string, token, newtoken) {
        string += "";  
        while (string.indexOf(token) != -1) {
                string = string.replace(token, newtoken);
        }
        return string;
}

function updateHash(key, value){
    obj = listHash();
    value = replaceAll(value, " ", "_");
    if(obj.query != undefined && obj.query != null && obj.query != ''){
        obj.query = replaceAll(obj.query, " ", "_");
    }
    
    for(i in obj){
       if(i == key){
           parent.location.href = parent.location.href.replace(key + '=' + obj[i], key + '=' + value);
           return;
       }             
    }
    parent.location.href = parent.location.href + "#" + key + "=" + value;
};



function Network() {

};

var proteger = function(callbackErro) {
        var method = this;

        if (this.protegida) {
                return this;
        } 

        return function() {
                // TODO Mudar para que seja o Ãºltimo parÃ¢metro que seja funÃ§Ã£o.
                var callbackErroFuncao = callbackErro || arguments[arguments.length -1];

                try {
                        method.apply(this, arguments);
                } catch(erro){
                        log("Aconteceu um erro:");
                        log(erro.stack || erro);
                        if (callbackErroFuncao != null && typeof callbackErroFuncao == 'function') {
                                callbackErroFuncao(erro);
                        }
                        throw erro;
                }
        };
};

Function.prototype.proteger  = proteger;
Function.prototype.protegida = false;

Network.ajax = function(url, metodo, dados, tipo, sucesso, erro, timeout) {
        if (ehExecucaoBackground()) {
                Network.ajaxNodeJs(url, metodo, dados, tipo, sucesso, erro, timeout);
        } else {
                Network.ajaxJQuery(url, metodo, dados, tipo, sucesso, erro, timeout);
        }
};

Network.ajaxNodeJs = function(url, metodo, dados, tipo, sucesso, erro, timeout) {
        var siteRequisicao = null;
        var requisicao = null;

        /* As requisiÃ§Ãµes do catÃ¡logo serÃ£o tratadas como acesso Ã  disco, se houver configurada
         * a pasta do catÃ¡logo.
         */
        if (ehRequisicaoCatalogo(url) && document._parentWindow 
                        && document._parentWindow.propriedades_servidor
                        && document._parentWindow.propriedades_servidor["renderizacao.pasta.catalogo"]) {
                var pastaCatalogo = 
                        document._parentWindow.propriedades_servidor["renderizacao.pasta.catalogo"];

                tratarRequisicaoLocalCatalogo(url, tipo, sucesso, erro,pastaCatalogo, 
                                document._parentWindow.fs);
                return;
        }

        // TODO SÃ³ estÃ¡ funcionando para GETS:

        if (Network.http) {
                siteRequisicao = Network.http.createClient(
                                Network.propriedades_servidor["renderizacao.porta"],
                                Network.propriedades_servidor["renderizacao.host"]);;
                requisicao = siteRequisicao.request(metodo, url, 
                                {'host': Network.propriedades_servidor["renderizacao.host"]});
        } else {
                siteRequisicao = document._parentWindow.http.createClient(
                                document._parentWindow.porta, document._parentWindow.host);
                requisicao = siteRequisicao.request(metodo, url, 
                                {'host': document._parentWindow.host});
        }

        requisicao.on('response', function (resposta) {
                if (resposta.statusCode >= 400) {
                        objRequest = new Object();

                        objRequest.status = resposta.statusCode;
                        objRequest.statusText = 'Erro ao realizar o request.';
                        erro(objRequest, 'Erro');
                } else {
                        var conteudoResposta = '';

                        resposta.setEncoding('utf8');

                        resposta.on('data', function (chunk) {
                                conteudoResposta += chunk;
                         });
                        resposta.on('end', function (chunk) {
                                sucesso(tipo != null && tipo == 'json' ?  
                                                JSON.parse(conteudoResposta) : conteudoResposta);
                         });
                }
        });
        requisicao.end();
};

Network.ajaxJQuery = function(url, metodo, dados, tipo, sucesso, erro, timeout) {
    var settings = {
            async: true,
            url: url,
            type : metodo,
            timeout : timeout,
            beforeSend: function (request) {
                    request.setRequestHeader("ajax", 'true');
            },
            success : function(dadosRetorno, mensagem, xhr) {
                    var location = xhr.getResponseHeader('Location');

                    if (location != null) {

                            // Tratamento diferente para o chrome, por questÃµes de seguranÃ§a
                            if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {
                                    callbackSucessoLogin = function() {
                                            Network.ajax(url, metodo, dados, tipo, sucesso, erro, timeout);
                                    };

                                    window.open(location,'Login',
                                                    'height=500,width=987,toolbar=no,directories=no,status=no, menubar=no,scrollbars=no,resizable=nos,modal=yes');
                            } else {
                                    var iframeLogin = $("<iframe style='min-width:987px;' height='100%' name='iframeLogin' id='iframeLogin' >");

                                    iframeLogin.attr('src', location);
                                    iframeLogin.dialog({autoopen: true, modal: true, closeText : "fechar", width : 1000, height : 700});

                                    callbackSucessoLogin = function() {
                                            iframeLogin.dialog('close');
                                            Network.ajax(url, metodo, dados, tipo, sucesso, erro, timeout);
                                    };

                                    $('div.ui-dialog-titlebar', iframeLogin.parent()).css('display', 'none');

                            }
                    } else {
                            sucesso(dadosRetorno, mensagem, xhr);
                    }
            },
            error: erro
    };

    if (metodo.toLowerCase() == 'post' || 
            metodo.toLowerCase() == 'put' || 
            metodo.toLowerCase() == 'delete') {
        if (dados)
            settings.contentType = 'application/json';
    }

    if (dados) {
        settings.data = dados;
    }

    $.ajax(settings);
};

function tratarRequisicaoLocalCatalogo(url, tipo, sucesso, erro, caminhoBase, fs) {
        var caminhoArquivo = recuperarCaminhoArquivoCatalogo(url, caminhoBase);
        fs.readFile(caminhoArquivo, 'utf8', function (err, data) {
                if (err) {
                        erro(err);
                } else {
                        sucesso(tipo != null && tipo == 'json' ?  eval(data) : data);  
                }
        });
}


/**
 * Esta funÃ§Ã£o Ã© usada para indicar se Ã© execuÃ§Ã£o em background (atravÃ©s de um NodeJS da vida) ou 
 * foreground (atravÃ©s do browser).
 */
function ehExecucaoBackground() {
        return (typeof document == 'undefined') || document._parentWindow != null;
}

var expRegCaminho = /.*\/componentesCMS\/catalogo\/(.*)/;

function ehRequisicaoCatalogo(url) {
        return url.match(expRegCaminho) != null;
}

function recuperarCaminhoArquivoCatalogo(url, caminhoBase) {
        return caminhoBase + url.match(expRegCaminho)[1];
}


/*!
Math.uuid.js (v1.4)
http://www.broofa.com
mailto:robert@broofa.com

Copyright (c) 2010 Robert Kieffer
Dual licensed under the MIT and GPL licenses.
*/

/*
 * Generate a random uuid.
 *
 * USAGE: Math.uuid(length, radix)
 *   length - the desired number of characters
 *   radix  - the number of allowable values for each character.
 *
 * EXAMPLES:
 *   // No arguments  - returns RFC4122, version 4 ID
 *   >>> Math.uuid()
 *   "92329D39-6F5C-4520-ABFC-AAB64544E172"
 * 
 *   // One argument - returns ID of the specified length
 *   >>> Math.uuid(15)     // 15 character ID (default base=62)
 *   "VcydxgltxrVZSTV"
 *
 *   // Two arguments - returns ID of the specified length, and radix. (Radix must be <= 62)
 *   >>> Math.uuid(8, 2)  // 8 character ID (base=2)
 *   "01001010"
 *   >>> Math.uuid(8, 10) // 8 character ID (base=10)
 *   "47473046"
 *   >>> Math.uuid(8, 16) // 8 character ID (base=16)
 *   "098F4D35"
 */
  // Private array of chars to use
  var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split(''); 

  Math.uuid = function (len, radix) {
    var chars = CHARS, uuid = [];
    radix = radix || chars.length;

    if (len) {
      // Compact form
      for (var i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
    } else {
      // rfc4122, version 4 form
      var r;

      // rfc4122 requires these characters
      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
      uuid[14] = '4';

      // Fill in random data.  At i==19 set the high bits of clock sequence as
      // per rfc4122, sec. 4.1.5
      for (var i = 0; i < 36; i++) {
        if (!uuid[i]) {
          r = 0 | Math.random()*16;
          uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
        }
      }
    }

    return uuid.join('');
  };

  // A more performant, but slightly bulkier, RFC4122v4 solution.  We boost performance
  // by minimizing calls to random()
  Math.uuidFast = function() {
    var chars = CHARS, uuid = new Array(36), rnd=0, r;
    for (var i = 0; i < 36; i++) {
      if (i==8 || i==13 ||  i==18 || i==23) {
        uuid[i] = '-';
      } else if (i==14) {
        uuid[i] = '4';
      } else {
        if (rnd <= 0x02) rnd = 0x2000000 + (Math.random()*0x1000000)|0;
        r = rnd & 0xf;
        rnd = rnd >> 4;
        uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
      }
    }
    return uuid.join('');
  };

  // A more compact, but less performant, RFC4122v4 solution:
  Math.uuidCompact = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    }).toUpperCase();
  };

/**
 * Gera o caminho do arquivo a partir do contexto de renderizaÃ§Ã£o o qual foi usado e dos dados da 
 * pÃ¡gina.
 */
var gerarDiretorioArquivo = function(sitenav) {
        var caminhoArquivo = '';

        for (var i = 0; sitenav != null && i < sitenav.length; i++)  {

                // A seÃ§Ã£o home nÃ£o entra na montagem do diretÃ³rio do site.
                if (!ehSecaoHome(sitenav[i]) && !sitenav[i].secaoOrganizacao) {
                        caminhoArquivo += '/' + sitenav[i].nome;
                }
        }
        return caminhoArquivo; 
};


/**
 * Gera o caminho do arquivo a partir do contexto de renderizaÃ§Ã£o o qual foi usado e dos dados da 
 * pÃ¡gina.
 */
var gerarCaminhoArquivo = function(sitenav) {
        var caminhoArquivo = gerarDiretorioArquivo(sitenav);

        // Usaremos as informaÃ§Ãµes do Ãºltimo elemento do sitenav para descobrirmos o nome do arquivo.
//      if (secao.idFatwire != null) {
//              caminhoArquivo += '/p' + secao.idFatwire + '.html'; 
//      } else {
//              caminhoArquivo += '/p' + secao._id + '.html';
//      }

        caminhoArquivo += '/index.html';

        return caminhoArquivo; 
};


function escreverArvoreSecoesBuscaMulticheck(secoes, divMultiCheck, retornaArray){
        for (var i=0; i<secoes.length; i++) {
                var idSecaoInput = secoes[i]._id;
                var idSecao = secoes[i]._id + '|';
                if(secoes[i].idFatwire != null){
                        idSecao += secoes[i].idFatwire;
                        idSecaoInput += secoes[i].idFatwire;
                }

                // Secao Pai
                var secao = $('<div class="multicheck">');
                var secaoCheckBox = $('<input type="checkbox" id="'+ (retornaArray ? secoes[i]._id : idSecaoInput ) +'" name="secoes[]" value="'+ (retornaArray ? secoes[i]._id : idSecao )+'" /><label for="'+ (retornaArray ? secoes[i]._id : idSecaoInput ) +'"><strong>'+  secoes[i].titulo +'</strong></label>').appendTo(secao);

                secaoCheckBox.click(function(e){
                        $('#todassecoes').removeAttr("checked");
                });

                // Secao Filho
                if(secoes[i].filhos.length > 1){
                        if(secoes[i].titulo == "Home"){
                                escreverArvoreSecoesBuscaMulticheck(secoes[i].filhos, divMultiCheck, retornaArray);
                        }else{
                                secao.appendTo(divMultiCheck);
                                escreverArvoreSecoesBuscaMulticheckChild(secoes[i].filhos, secao, retornaArray);
                        };
                };
        };
};


function escreverArvoreSecoesBuscaMulticheckChild(secoes, divMultiCheck, retornaArray){
        for (var i=0; i<secoes.length; i++) {
                var idSecaoInput = secoes[i]._id;
                var idSecao = secoes[i]._id + '|';
                if(secoes[i].idFatwire != null){
                        idSecao += secoes[i].idFatwire;
                        idSecaoInput += secoes[i].idFatwire;
                }

                // Secao Pai
                secao = $('<div class="multicheck">');
                var secaoCheckBox = $('<input type="checkbox" class="outrasSecoes"  id="'+ (retornaArray ? secoes[i]._id : idSecaoInput ) +'" name="secoes[]" value="'+ (retornaArray ? secoes[i]._id : idSecao ) +'" /><label for="'+ (retornaArray ? secoes[i]._id : idSecaoInput ) +'">'+  secoes[i].titulo +'</label>').appendTo(secao);

                secaoCheckBox.click(function(e){
                        $('#todassecoes').removeAttr("checked");
                });

                secao.appendTo(divMultiCheck);

                // Secao Filho
                if(secoes[i].filhos.length > 1){
                        var childMultichecked = $('<div class="multicheck">').appendTo(secao);
                        escreverArvoreSecoesBuscaMulticheckChild(secoes[i].filhos, childMultichecked, retornaArray);
                };
        };
};

var gerarUrlCompleta  = function(sitenav, site) {
        var caminhoArquivo = gerarDiretorioArquivo(sitenav);

        caminhoArquivo += '/';
//      caminhoArquivo += '/index.html';

        return "http://" + site.nome + ".ig.com.br" + caminhoArquivo; 
};

var gerarUrlCompletaPath  = function(path, site) {
        var caminhoArquivo = (path && path != "" ? "/" + path : "");

        caminhoArquivo += '/index.html';
        return "http://" + site.nome + ".ig.com.br" + caminhoArquivo; 
};


var ehSecaoHome = function(secao) {
        return (secao.nome == 'home'); // Verificar a melhor forma de se fazer isso.
};

function escreverComboArvoreSecoes(secoes, opcoes, identacao){
        for (var i=0; i<secoes.length; i++) {
                // Secao Pai
                opcoes[opcoes.length] = new Option(identacao + " " + secoes[i].titulo,
                                secoes[i]._id, false, false);

                // Secao Filho
                if(secoes[i].filhos.length > 1){
                        escreverComboArvoreSecoes(secoes[i].filhos, opcoes, identacao + "--");
                }
        }
};

function escreverComboTemplatesNoticiaParaSite (callback, opcoes, callbackSucesso, callbackErro) {
        FachadaRepositorioSites.criarFachada().recuperar(
                                $.cookie('siteSelecionado'),
                                function(site) {
                                        if(site.layoutMestreNoticia != undefined && site.layoutMestreNoticia.length > 0) {                           
                                                        for (var i=0; i<site.layoutMestreNoticia.length; i++) {                   
                                                                opcoes[opcoes.length] = new Option(site.layoutMestreNoticia[i], site.layoutMestreNoticia[i], false, false);
                                                        };
                                        }
                                        if(callback == true){
                                                callbackSucesso(opcoes);
                                        }
                        }, 
                        callbackErro);
}

function escreverComboTemplatesNoticiaParaSecao (opcoes, jqCombo, idSecao, callbackSucesso, callbackErro) {
        if (idSecao) {
                FachadaRepositorioSecoes.criarFachada().recuperar(
                                idSecao,
                                function (secao) {
                                    if (!opcoes) opcoes = [];
                                        if (secao.layoutMestreNoticia && secao.layoutMestreNoticia.length > 0) {
                                                for (var i = 0; i < secao.layoutMestreNoticia.length; i++) {      
                                                        opcoes[opcoes.length] = new Option(secao.layoutMestreNoticia[i], secao.layoutMestreNoticia[i], false, false);
                                                }
                                                if (jqCombo) {
                                                jqCombo.html("");  
                                            jqCombo.append(opcoes);                                         
                                        }
                        callbackSucesso();
                                        } else {
                                                escreverComboTemplatesNoticiaParaSite(true, opcoes, function (opcoesSite) {
                                                        if (jqCombo) {
                                                        jqCombo.html("");  
                                                    jqCombo.append(opcoes);                                         
                                                }
                                                callbackSucesso();
                                                        }, callbackErro);                   
                                        }                                                   
                            }, 
                        callbackErro);
        } else {
            callbackSucesso();
        }
}

/* Augment existing class with a method from another class */
function augment(receivingClass, givingClass) {
    /* only provide certain methods */
    if (arguments[2]) {
        for (var i=0, len=arguments.length; i<len; i++) {
            receivingClass.prototype[arguments[i]] = givingClass.prototype[arguments[i]];
        }
    }
    /* provide all methods*/
    else {
        for (methodName in givingClass.prototype) {
            /* check to make sure the receiving class doesn't
               have a method of the same name as the one currently
               being processed */
            if (!receivingClass.prototype[methodName]) {
                receivingClass.prototype[methodName] = givingClass.prototype[methodName];
            }
        }
    }
};

var completarComZerosAEsquerda = function(numero, tamanho) {
    var  strNumero = numero + "";

   while (strNumero.length < tamanho) {
      strNumero = "0" + strNumero;
    }

    return strNumero;
};

var normalizar = function(str) {
        var de          = "ÃƒÃ€ÃÃ„Ã‚ÃˆÃ‰Ã‹ÃŠÃŒÃÃÃŽÃ’Ã“Ã–Ã”Ã™ÃšÃœÃ›Ã£Ã Ã¡Ã¤Ã¢Ã¨Ã©Ã«ÃªÃ¬Ã­Ã¯Ã®Ã²Ã³Ã¶Ã´ÃµÃ¹ÃºÃ¼Ã»Ã‘Ã±Ã‡Ã§",
        para    = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiiooooouuuunncc",
        mapping = {};
        for(var i=0; i<de.length; i++){
                mapping[de.charAt(i)] = para.charAt(i);
        }
        var ret = [];
        for(var i=0; i<str.length; i++) {
                var c = str.charAt(i);
                if(mapping.hasOwnProperty(str.charAt(i))){
                        ret.push(mapping[c]);
                }else{
                        ret.push(c);
                }
        }
        var retorna = ret.join('').replace(/[^-A-Za-z0-9]+/g, ' ').toLowerCase();

        return retorna;
};

if(typeof exports != 'undefined'){
        exports.normalizar = normalizar;
}

var parseDataDatePickerInicio = function(dateString){
        var date = obterDataDatePicker(dateString);
        date.setUTCHours(0);
        date.setUTCMinutes(0);
        date.setUTCSeconds(0);
        var dateFormated = obterDataFormatada(date);
        return dateFormated;
};

var parseDataDatePickerFinal = function(dateString){
        var date = obterDataDatePicker(dateString);
        date.setUTCHours(23);
        date.setUTCMinutes(59);
        date.setUTCSeconds(59);
        var dateFormated = obterDataFormatada(date);
        return dateFormated;
};


var obterDataDatePicker = function(dateString){
        var regExp = /(\d{2})\/(\d{2})\/(\d{4})/g;
        var dateItens = dateString.replace(regExp,"$1|$2|$3");
        var itens = dateItens.split('|');
        var date = new Date();
        date.setUTCFullYear(itens[2]);
        date.setUTCDate(itens[0]);
        date.setUTCMonth(itens[1]-1);
        return date;
};

var parseDateTimePicker = function(dateString){
    var dateFormated = obterDataFormatadaLocal(parseDateTimePickerToDate(dateString));
    return dateFormated;
};

var parseDateTimePickerToDate = function(dateString){
    var regExp = /(\d{2})\/(\d{2})\/(\d{4})\s(\d{2}):(\d{2}):(\d{2})/g;
    var dateItens = dateString.replace(regExp,"$1|$2|$3|$4|$5|$6");
    var itens = dateItens.split('|');
    var date = new Date();
    date.setFullYear(itens[2]);
    date.setDate(itens[0]);
    date.setMonth(itens[1]-1);
    date.setHours(itens[3]);
    date.setMinutes(itens[4]);
    date.setSeconds(itens[5]);
    return date;
};


//"2012-04-26T12:01:32Z"
var parseDate = function(dateString){
        var regExp = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})Z/g;
        var dateItens = dateString.replace(regExp,"$1|$2|$3|$4|$5|$6");
        var itens = dateItens.split('|');
        var date = new Date();
        date.setFullYear(itens[0]);
        date.setDate(itens[2]);
        date.setMonth(itens[1]-1);
        date.setHours(itens[3]);
        date.setMinutes(itens[4]);
        date.setSeconds(itens[5]);
        return date;
};

var parseUTCDate = function(dateString){
        var regExp = /(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})Z/g;
        var dateItens = dateString.replace(regExp,"$1|$2|$3|$4|$5|$6");
        var itens = dateItens.split('|');
        var date = new Date();
        date.setUTCFullYear(itens[0]);
        date.setUTCDate(itens[2]);
        date.setUTCMonth(itens[1]-1);
        date.setUTCHours(itens[3]);
        date.setUTCMinutes(itens[4]);
        date.setUTCSeconds(itens[5]);
        return date;
};

var parseSimpleDateTimePicker = function(dateString){
        var regExp = /(\d{2})\/(\d{2})\/(\d{4})/g;
        if(regExp.exec(dateString) != null){
                var dateItens = dateString.replace(regExp,"$1|$2|$3");
                var itens = dateItens.split('|');
                var date = new Date();
                date.setFullYear(itens[2]);
                date.setDate(itens[0]);
                date.setMonth(itens[1]-1);
                date.setHours(0);
                date.setMinutes(0);
                date.setSeconds(0);
                var dateFormated = obterDataFormatada(date);
                return dateFormated;
        }else{
                return null;
        }
};


var obterDataAtual = function() {
        dataAtual = new Date();
        ano    = dataAtual.getFullYear();
        mes    = completarComZerosAEsquerda(dataAtual.getMonth() + 1, 2);
        dia    = completarComZerosAEsquerda(dataAtual.getDate(), 2);
        hora   = completarComZerosAEsquerda(dataAtual.getHours(), 2);
        minuto = completarComZerosAEsquerda(dataAtual.getMinutes(), 2);
        return ano + '' + mes + '' + dia + '' + hora + '' + minuto;
};

var obterDataFormatada = function(data) {
        ano    = data.getUTCFullYear();
        mes    = completarComZerosAEsquerda(data.getUTCMonth() + 1, 2);
        dia    = completarComZerosAEsquerda(data.getUTCDate(), 2);
        hora   = completarComZerosAEsquerda(data.getUTCHours(), 2);
        minuto = completarComZerosAEsquerda(data.getUTCMinutes(), 2);
        segundos = completarComZerosAEsquerda(data.getUTCSeconds(), 2);
        return ano + '-' + mes + '-' + dia + 'T' + hora + ':' + minuto + ':'+ segundos+'Z';
};

var obterDataFormatadaLocal = function(data) {
        ano    = data.getFullYear();
        mes    = completarComZerosAEsquerda(data.getUTCMonth() + 1, 2);
        dia    = completarComZerosAEsquerda(data.getUTCDate(), 2);
        hora   = completarComZerosAEsquerda(data.getUTCHours(), 2);
        minuto = completarComZerosAEsquerda(data.getUTCMinutes(), 2);
        segundos = completarComZerosAEsquerda(data.getUTCSeconds(), 2);
        return ano + '-' + mes + '-' + dia + 'T' + hora + ':' + minuto + ':'+ segundos+'Z';
};

var dataFormatadaBusca = function(data) {
        ano    = ''+data.getFullYear();
        mes    = completarComZerosAEsquerda(data.getMonth() + 1, 2);
        dia    = completarComZerosAEsquerda(data.getDate(), 2);
        hora   = completarComZerosAEsquerda(data.getHours(), 2);
        minuto = completarComZerosAEsquerda(data.getMinutes(), 2);
        segundos = completarComZerosAEsquerda(data.getSeconds(), 2);
        return dia + '/' + mes + '/' + ano.substring(2, 4) + ' | ' + hora + ':' + minuto;
};

var isNotNull = function(variavel){
        return (variavel != undefined && variavel != null)?true:false;
};

var isNotEmpty = function(variavel){
        return (variavel != undefined && variavel != null && variavel != "")?true:false;
};

function trim (str) {
    return str.replace(/^\s+/, '').replace(/\s*$/, '');
}

function removerTags (str) {
    return str.replace(/<[^>]+>/g, '');
};

function escreverArvoreSecoes(secoes, opcoes, identacao){
        for (var i=0; i<secoes.length; i++) {
                // Secao Pai
                opcoes[opcoes.length] = new Option(identacao + " " + secoes[i].titulo,
                                secoes[i]._id, false, false);

                // Secao Filho
                if(secoes[i].filhos.length > 1){
                        escreverArvoreSecoes(secoes[i].filhos, opcoes, identacao + "--");
                }
        }
};

function escreverArvoreSecoesBusca(secoes, opcoes, identacao){
        for (var i=0; i<secoes.length; i++) {
                var idSecao = secoes[i]._id + '|';
                if(secoes[i].idFatwire != null){
                        idSecao += secoes[i].idFatwire;
                }
                // Secao Pai
                opcoes[opcoes.length] = new Option(identacao + " " + secoes[i].titulo,
                                idSecao, false, false);

                // Secao Filho
                if(secoes[i].filhos.length > 1){
                        escreverArvoreSecoes(secoes[i].filhos, opcoes, identacao + "--");
                }
        }
};

var normalize = (function() {
    var from = "ÃƒÃ€ÃÃ„Ã‚ÃˆÃ‰Ã‹ÃŠÃŒÃÃÃŽÃ’Ã“Ã–Ã”Ã™ÃšÃœÃ›Ã£Ã Ã¡Ã¤Ã¢Ã¨Ã©Ã«ÃªÃ¬Ã­Ã¯Ã®Ã²Ã³Ã¶Ã´ÃµÃ¹ÃºÃ¼Ã»Ã‘Ã±Ã‡Ã§", 
        to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiiooooouuuunncc",
        mapping = {};
    
    for(var i=0; i<from.length; i++)
        mapping[from.charAt(i)] = to.charAt(i);
    
    return function(str, separator) {
        var ret = [];
        for(var i=0; i<str.length; i++) {
            var c = str.charAt(i);
            if(mapping.hasOwnProperty(str.charAt(i)))
                ret.push(mapping[c]);
            else
                ret.push(c);
        }
        return ret.join('').replace(/[^-A-Za-z0-9]+/g, separator).toLowerCase();
    };


})();

/*
 * Recebe um codigo html e altera o type de todas as tags script, o
 * que previne que o codigo javascript seja executado em um ambiente errado (como
 * no servidor, por exemplo, durante a renderizacao)
 */
function desativarTagsScript (corpo) {
    var scriptOccurrence;
    var scriptPattern = /<script[^>]*/;
    var typePattern = / type="([^"]*)"/;
    while (scriptOccurrence = scriptPattern.exec(corpo)) {
        var found = scriptOccurrence[0];
        var replacement = scriptOccurrence[0];
        var typeOccurrence = typePattern.exec(scriptOccurrence[0]);
        if (!typeOccurrence) {
            replacement += ' type="text/javascript-frontend"';
                    //replacement = replacement.replace(/<script/, '<frontend:script');
        } else {
            if (!/^jsi/.test(typeOccurrence[1])) {
                replacement = replacement.replace(typePattern, ' type="' + typeOccurrence[1] + '-frontend"');
                        //replacement = replacement.replace(/<script/, '<frontend:script');
            } else {
                        //replacement = replacement.replace(/<script/, '<jsi:script');
                        }
        }
                /*
                 * Mesmo que seja um script jsi, todas as tags devem ter um prefixo
                 * adicionado a elas, para que esse loop nao seja infinito
                 */
                replacement = replacement.replace(/<script/, '<scanned:script');
        corpo = corpo.replace(scriptPattern, replacement);
    }
        corpo = corpo.replace(/<scanned:script/g, '<script'); 
        return corpo;
}

/*
 * Recebe um codigo HTML cujas tags script foram desativadas pela funcao
 * desativarTagsScript, e retorna o type de toas as tags script para
 * "text/javascript", o que deixa o codigo pronto para ser executado
 */
function reativarTagsScript (corpo) {
        corpo = corpo.replace(/(type="[^"]*)-frontend"/g, '$1"');
        // O jquery faz alguns escapamentos improprios do &
        // (por exemplo, no componente de embed na noticia)
        return corpo.replace(/&(amp;)+/g, '&amp;') 
}

if(typeof exports != 'undefined'){
        exports.reativarTagsScript = reativarTagsScript;
}

//TODO: Paleativo para corrigir dados que estao "encodados" no mongo
//Fazer script para arrumar no mongo tudo que tiver %NN
// Remover dos script e dos templates as chamadas para decodeURIComponentIG ou decodeURIComponent
function decodeURIComponentIG(str) {	
	if(typeof str != 'undefined') {	
		str = str.replace(/%(?![A-F0-9]{2})/gi, '%25');
	}
	
	return decodeURIComponent(str);
}

function obterUrlPaginacao(contexto, pagina, direcao) {
	var numeroDaPagina = pagina != null ? pagina : contexto.paginaAtual;
	var urlPagina      = contexto.url;
	
	if(direcao == '<') {
		if(numeroDaPagina > 1) {
			numeroDaPagina--;
		}
	} else if(direcao == '>') {
		if(numeroDaPagina < contexto.totalDePaginas) {
			numeroDaPagina++;
		}
	}
	
	if(typeof contexto.momentoPreview != 'undefined' && contexto.momentoPreview) {
		return obterUrlPreview(contexto.tipo, contexto._id, contexto.template) + '/' + numeroDaPagina;
	}
	
	if(numeroDaPagina == 1) {
		urlPagina = urlPagina.replace(/\-pagina[0-9]{1,}\.html/, '.html');
	} else {
		if(/\-pagina[0-9]{1,}\.html/.test(urlPagina)) {
			urlPagina = urlPagina.replace(/\-pagina[0-9]{1,}\.html/, '-pagina' + numeroDaPagina + '.html');
		} else {
			urlPagina = urlPagina.replace('.html', '-pagina' + numeroDaPagina + '.html');
		}
	}
	
	return urlPagina;
}

if(typeof exports != 'undefined'){
    exports.obterUrlPaginacao = obterUrlPaginacao;
}

function obterUrlPreview(tipo, id, template) {
	if(tipo == null || id == null) {
		console.log('Dados insuficientes para gerar a url do preview!');
		
		return '/edicaoHomes/';
	}
	
	return '/edicaoHomes/preview/' + tipo + '/' + id + (typeof template != 'undefined' ? '/' + template : '');
}

if(typeof exports != 'undefined'){
    exports.obterUrlPreview = obterUrlPreview;
}