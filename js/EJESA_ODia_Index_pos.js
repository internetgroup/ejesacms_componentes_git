        function fixaMenu() {
            var header = $('.main-header-fixo, .principal-nav');
            var menu = $('.principal-nav');
            var geral = $('#navigation');
            var offSetMenu = 60;
            $(window).scroll(function () {
                var offSetWindow = $(window).scrollTop();
                if (offSetWindow >= offSetMenu) {
                    header.addClass('menu-fixed');
                    /*header.show();*/
                    menu.addClass('t120');
                    /*geral.addClass('geral-absolute');*/
                } else {
                    header.removeClass('menu-fixed');
                    menu.removeClass('t120');
                    /*geral.removeClass('geral-absolute');*/
                    $('.main-header-fixo').hide();
                }
            });
        }

        function previsaoTempo() {
            var html = '';
            var d, day, month, year, myday, mesExtenso, diaSemana, dataCompleta, mn = false;

            d = new Date();
            day = d.getDate();
            month = d.getMonth();
            year = d.getFullYear();
            myday = (day <= 9) ? "0" + day : day;

            mesExtenso = [" de Janeiro de ", " de Fevereiro de ", " de Março de ", " de Abril de ", " de Maio de ", " de Junho de ", " de Julho de ", " de Agosto de ", " de Setembro de ", " de Outubro de ", " de Novembro de ", " de Dezembro de "];
            diaSemana = ["Domingo", "Segunda-feira", "Terça-feira", "Quarta-feira", "Quinta-feira", "Sexta-feira", "Sábado"];

            dataCompleta =  diaSemana[d.getDay()] + ', ' + myday + mesExtenso[month] + year;
            $('.data').html(dataCompleta);

            $.ajax({
                type: "GET",
                url : "http://odia.ig.com.br/tempo/tempo.json",
                dataType : "JSON",
                cache : true,
                success : function(response){
                    var auxiliar = response.noticias.tempo.especial.itens[0].componentes[0].dados.chamada;
                    $('#min-temp').html(auxiliar.tempmin);
                    $('#max-temp').html(auxiliar.tempmax);
                    /*$('.ico-tempo').attr('style','background: url(http://s0.ejesa.ig.com.br/img/odia/sprite-home.png?v=01) no-repeat scroll 0 '+auxiliar.imgtempo+' ')*/
                }
            });
        }


        var _secaoRio = 0;
        var _secaoDiversao = 0;
        var _secaoEconomia = 0;
        var _secaoEsporte = 0;
        function ajustaLoad(secao){
            $('.carregando-iframe').hide();
            $("#toastload").fadeOut();
            $('iframe#'+secao).slideDown();
        }
        function addSecao(secao,url,altura){
            var link = 'http://odia.ig.com.br/'+secao+'/'+url+'.html';
            var iframe = document.createElement('iframe');
            iframe.async = true;
            iframe.async = true;
            iframe.frameBorder=0;
            iframe.width='960px';
            iframe.height=parseInt(altura)+'px';
            iframe.id=secao;
            iframe.setAttribute('scrolling', 'no');
            iframe.setAttribute('src', link);
            iframe.setAttribute('style', 'margin:30px 0 0 -20px;padding:0;display:none;');
            iframe.setAttribute('onload', 'ajustaLoad("'+url+'")');
            iframe.setAttribute('id', url);

            $("#add-secoes").before("<img src='http://s0.ejesa.ig.com.br/img/odia/carregando-anima.gif?v=0.1' style='margin:20px 0 50px 300px;' class='carregando-iframe'>");
            $("#toastload").fadeIn();
            $('#add-secoes').before(iframe);
            switch(url){
                case 'rio':
                    _secaoRio = 1;
                break;
                case 'diversao':
                    _secaoDiversao = 1;
                break;
                case 'economia':
                    _secaoEconomia = 1;
                break;
                case 'esporte':
                    _secaoEsporte = 1;
                break;
            }
            /*_gaq.push (['_trackEvent', 'Home', 'scroll', url]);*/
        }

        function fixMenuOnScroll() {
            var header = $('.main-header-fixo, .principal-nav');
            var menu = $('.principal-nav');
            var geral = $('#navigation');
            var offSetMenu = 256;
            var alturaWindowScroll = $(window).height() + 256;
            
            $('#area10').hide();
            $('.a11rdp').hide();
            $('#footer-novo').hide();

            $(window).scroll(function () {

                var offSetWindow = $(window).scrollTop();

                if (offSetWindow >= offSetMenu) {

                    $("#main-nav").addClass("com-scroll");
            
                    if( $("body").hasClass("super-ad-video") ) {
                        $('.video-area').css({'position':'fixed', 'top':'-5px'});
                    }

                    $('#main-nav').addClass('com-scroll');
                    if($(window).scrollTop() >= 2400){
                        $('#nav-menu ul.main-cat li').removeClass('ativo');
                        if(_secaoRio == 0){
                            addSecao('rio-de-janeiro', 'rio', 1000);
                        }
                    }
                    if($(window).scrollTop() >= 3300){
                        $('#nav-menu ul.main-cat li').removeClass('ativo');
                        if(_secaoEsporte == 0){
                            addSecao('esporte', 'esporte', 1000);
                        }
                    }
                    if($(window).scrollTop() >= 4300){
                        $('#nav-menu ul.main-cat li').removeClass('ativo');
                        if(_secaoDiversao == 0){
                            addSecao('diversao', 'diversao', 980);
                        }
                    }
                    if($(window).scrollTop() >= 5300){
                        $('#nav-menu ul.main-cat li').removeClass('ativo');
                        if(_secaoEconomia == 0){
                            addSecao('economia', 'economia', 526); /* 970 */
                        }
                    }
                    if($(window).scrollTop() <= 1050){
                        $('#nav-menu ul.main-cat li').removeClass('ativo');
                    }
                    if($(window).scrollTop() >= 6100){
                        $('#nav-menu ul.main-cat li').removeClass('ativo');
                        $('#area10').show();
                        $('.a11rdp').show();
                        $('#footer-novo').show();
                    }
                }else{
                    $('#main-nav').removeClass('com-scroll');

                    if( $("body").hasClass("super-ad-video") ) {
                        $('.video-area').css({'position':'relative', 'top':'0'});
                    }
                }
            });
        }

        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=11&appId=241359022642830";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        window.___gcfg = {
            lang: 'en-US'
        };
        (function() {
            var po = document.createElement('script'); 
            po.type = 'text/javascript'; 
            po.async = true;
            po.src = 'https://apis.google.com/js/plusone.js';
            var s = document.getElementsByTagName('script')[1]; 
            s.parentNode.insertBefore(po, s);
        })();

        (function(d,s,id){
        var js,fjs=d.getElementsByTagName(s)[2];
            if(!d.getElementById(id)){
                js=d.createElement(s);
                js.id=id;
                js.src="https://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js,fjs);
            }
        }(document,"script","twitter-wjs"));

        /*
        $(document).ready(function(){

            var pos = $("#new-logo").position();
            var width = $("#new-logo").outerWidth();
            $("#inter-sky-right").css({
                position: "absolute",
                top: (pos.top - 60) + "px",
                left: (pos.left + width + 820) + "px"
            }).show();

        });
        */


        /* Faz a Chamada da intervenção */
        /*
        $(document).ready(function(){
            setTimeout(function() {
                EJESA.WSPublicidades.checarTopClick();
            }, 5000);
        });
        */