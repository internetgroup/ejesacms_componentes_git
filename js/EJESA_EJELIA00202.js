/*global document: false */
/*global console*/
/*global window */
/*global IBTX */

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.EJELIA00202'); /** Lista de últimas Home  **/

/** implementando módulo **/
/** Lista de últimas Home **/
EJESA.EJELIA00202 = (function () {

    'use strict';
    //propriedades privadas
    var QTDNEWS = 5,
        SITE = 'odia',
        SOLR = 'http://odia.ig.com.br/_indice/noticias/select?',
        FORMAT = 'json',
        TOP = 'http://s0.ejesa.ig.com.br/json/mostviewed/ga-odia-most-viewed-today.json',
        start,
        renderiza,
        newsMenuCommented,
        newsMenuLast,
        page = 1,
        pageIdentify,
        makeAddress,
        query,
        executeSearch,
        pageMarked,
        nextPage,
        prevPage,
        thisPage,
        resetPagination;
        //fim da declaracao var

    //métodos privados
    pageIdentify = function () {
        var start = (page - 1) * QTDNEWS;
        return start;
    };

    makeAddress = function (start) {

        var address = SOLR;

        address += 'start=';
        address += start;
        address += '&size=';
        address += QTDNEWS;
        address += '&site=';
        address += SITE;
        address += '&wt=';
        address += FORMAT;

        return address;
    };

    pageMarked = function () {
        var mp,
            posid = '';

        if (page > 5) {

            if (page % 5 === 1) {

                posid = page % 5;

            } else {
                posid = page % 5;
                if (posid === 0) {
                    posid = 5;
                }
            }

            mp = 'hn_pos_' + posid;

        } else {
            mp = 'hn_pos_' + page;
        }

        EJESA.Util.removeClassByID(mp, 'headline_nav_off');
        EJESA.Util.addClassByID(mp, 'headline_nav_on', true);
    };

    resetPagination = function () {
        EJESA.Util.removeClassByID('hn_pos_1', 'headline_nav_on');
        EJESA.Util.removeClassByID('hn_pos_2', 'headline_nav_on');
        EJESA.Util.removeClassByID('hn_pos_3', 'headline_nav_on');
        EJESA.Util.removeClassByID('hn_pos_4', 'headline_nav_on');
        EJESA.Util.removeClassByID('hn_pos_5', 'headline_nav_on');

        EJESA.Util.addClassByID('hn_pos_1', 'headline_nav_off');
        EJESA.Util.addClassByID('hn_pos_2', 'headline_nav_off');
        EJESA.Util.addClassByID('hn_pos_3', 'headline_nav_off');
        EJESA.Util.addClassByID('hn_pos_4', 'headline_nav_off');
        EJESA.Util.addClassByID('hn_pos_5', 'headline_nav_off');
    };

    start = function () {

        var start,
            address;

        //box de mais lidas
        //EJESA.Util.loadJS(TOP);

        //box de últimas
        start = pageIdentify();
        address = makeAddress(start);
        executeSearch(address);
        resetPagination();
        pageMarked();

    };

    executeSearch = function (query) {

        var xmlhttp;
        
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
          xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState !== 4) {
                return false;
            }
            if (xmlhttp.status !== 200) {
                console.log("Error, status code: " + xmlhttp.status);
                return false;
            }
            renderiza(xmlhttp.responseText);
                
        }
        xmlhttp.open("GET",query,true);
        xmlhttp.send("");
     
    };

    renderiza = function (data) {

        var obj = JSON.parse(data),
            docs,
            i,
            limit,
            date_news,
            sections,
            mysection,
            tit,
            html = '';


        docs = obj.response.docs;

        html += "<ul>";

        for (i = 0; i < docs.length; i = i + 1) {

            var buscaTags = docs[i].palavrasChaves;
            if(buscaTags.indexOf("chargedoaroeira") == -1){

                html += "<li>";

                html += "<span class='headline-item'><p>";

                limit = 0;
                if (docs[i].urlImgEmp_idCorteImagem !== undefined) {

                    html += "<a href='" + docs[i].url + "'>";
                    html += "<img src='" + docs[i].urlImgEmp_80x60 + "'>";
                    html += "</a>";
                    limit = 90;
                } else {
                    limit = 180;
                }

                date_news = EJESA.Util.dateSolr(docs[i].startDate, '$4:$5');

                html += "<span class='headline-item-eye'>";
                html += date_news;
                html += " - ";

                sections = docs[i].secaoBreadcrumb.split('›');
                mysection = sections[sections.length - 1];
                html += EJESA.Util.trim(mysection);

                html += "</br></span>";
                html += "<span class='headline-item-title'>";
                html += "<a href='" + docs[i].url + "'>";

                tit = '';
                if (docs[i].titulo.length > limit) {
                    tit = docs[i].titulo.substring(0, limit) + "...";
                } else {
                    tit = docs[i].titulo;
                }

                html += tit;
                html += "</a>";
                html += "</span>";
                html += "</p></span>";

                html += "</li>";

            }
        }

        html += "</ul>";

        document.getElementById('EJELIA00202HL').innerHTML = html;
    };

    thisPage = function (click) {

        page = click;
        start();
    };

    nextPage = function () {

        page = page + 1;
        if (page > 5) {
            page = 1;
        }
        start();
    };

    prevPage = function () {

        page = page - 1;
        if (page < 1) {
            page = 5;
        }
        start();
    };

    //procedimentos de inicialização
    //posicionando abas
    window.onload = function () {
        $('#news-item-commented').click(function() {
            $('#news-commented').css('display', 'block');
            $('#news-last').css('display', 'none');
            $('#news-menu-commented').css('color', '#fff');
            $('#news-item-commented').css('background', '#069');
            $('#news-menu-last').css('color', '#b2b2b2');
            $('#news-item-last').css('background', '#f0f0f0');
        });
        $('#news-item-last').click(function() {
            $('#news-commented').css('display', 'none');
            $('#news-last').css('display', 'block');
            $('#news-menu-last').css('color', '#fff');
            $('#news-item-last').css('background', '#069');
            $('#news-menu-commented').css('color', '#b2b2b2');
            $('#news-item-commented').css('background', '#f0f0f0');
        });
        $('#news-item-commented').click();
    };

    //revelando API pública
    return {
        start: start,
        nextPage: nextPage,
        prevPage: prevPage,
        thisPage: thisPage
    };
}());
