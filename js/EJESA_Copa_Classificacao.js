
/*global document: false */
/*global window: false */
/*global DOMParser: false */
/*global ActiveXObject: false */
/*global console*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.COPACONFEDERACOES.Classificacao'); /** Tabela de Classificação do campeonato **/


/** implementando módulo **/
/** Tabela de Classificação do campeonato **/
EJESA.COPACONFEDERACOES.Classificacao = (function () {
    'use strict';
    var start,
        getXML,
        path = "http://www.ongoing.com.br/dataodia/",
        xml =   [
            'condeferacoes_classificacao_671.xml',
            'condeferacoes_classificacao_587.xml'
        ],
        tabs = [
            "grupoa",
            "grupob"
        ],
        renderiza,
        mountTeam,
        mountHTML;

    start = function () {
        var i, url;
        for (i = 0; i < tabs.length; i += 1) {
            url = path + xml[i];
            getXML(url, tabs[i]);
        }
    };

    getXML = function (xml, tab) {
        var xhr;

        xhr = EJESA.COPACONFEDERACOES.Util.getXHR();

        xhr.onreadystatechange = function () {
            if (xhr.readyState !== 4) {
                return false;
            }
            if (xhr.status !== 200) {
                console.log("Error, status code: " + xhr.status);
                return false;
            }
            renderiza(xhr.responseText, tab);
        };

        xhr.open("GET", xml, true);
        xhr.send("");

    };

    renderiza = function (data, grupo) {

        var parser,
            xmlDoc,
            itens,
            item,
            i,
            teams = [];

        if (window.DOMParser) {
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(data, "text/xml");
        } else {
            xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
            xmlDoc.async = false;
            xmlDoc.loadXML(data);
        }

        itens = xmlDoc.getElementsByTagName('item');
        for (i = 0; i < itens.length; i = i + 1) {
            item = itens[i];
            teams.push(mountTeam(item));
        }
        mountHTML(grupo, teams);
    };

    mountHTML = function (grupo, teams) {

        var i,
            time,
            table,
            row,
            cellOrdem,
            cellTime,
            cellPontos,
            cellJogos,
            cellVitorias,
            tlength;


        table = document.getElementById(grupo);

        for (i = 0; i < teams.length; i += 1) {
            time = teams[i];

            tlength = table.rows.length;

            row = table.insertRow(tlength);
            cellOrdem = row.insertCell(0);
            cellTime = row.insertCell(1);
            cellPontos = row.insertCell(2);
            cellJogos = row.insertCell(3);
            cellVitorias = row.insertCell(4);

            cellOrdem.setAttribute("class", 'col1');
            cellTime.setAttribute("class", 'time');
            cellPontos.setAttribute("class", 'cinza');
            cellVitorias.setAttribute("class", 'cinza');

            cellOrdem.innerHTML = time[0][0].textContent;
            cellTime.innerHTML = time[1][0].textContent;
            cellPontos.innerHTML = '<strong>' + time[2][0].textContent + '</strong>';
            cellJogos.innerHTML = time[3][0].textContent;
            cellVitorias.innerHTML = time[4][0].textContent;
        }

    };

    mountTeam = function (team) {
        var ordem,
            time,
            pontos,
            jogos,
            vitorias,
            obj = [];

        ordem = team.getElementsByTagName('ordem');
        time = team.getElementsByTagName('time');
        pontos = team.getElementsByTagName('pg');
        jogos = team.getElementsByTagName('j');
        vitorias = team.getElementsByTagName('v');

        obj.push(ordem);
        obj.push(time);
        obj.push(pontos);
        obj.push(jogos);
        obj.push(vitorias);

        return obj;

    };


    /*revelando API pública*/
    return {
        start: start
    };

}());

