/*global COUNTAPI */
/*global XMLHttpRequest*/
/*global window*/
/*global ecount*/
/*global console*/
/*global ActiveXObject*/

function atualizaEnquete() {
	setInterval(function(){
		COUNTAPI.POLL.get();
	}, 60000);
}

var COUNTAPI = {};
var RET;
var COUNTURL = "http://count.ejesa.com.br/count/";
//var COUNTURL = "http://count-qa.ejesa.com.br:8080/count/";
//var COUNTURL = "http://localhost:8080/count/";

COUNTAPI.namespace = function (ns_string) {
	'use strict';
	var parts = ns_string.split('.'),
		parent = COUNTAPI,
		i;

	if (parts[0] === "COUNTAPI") {
		parts = parts.slice(1);
	}

	for (i = 0; i < parts.lenght; i += 1) {
		if (parent[parts[i]] === 'undefined') {
			parent[parts[i]] = {};
		}
		parent = parent[parts[i]];
	}
	return parent;
};

/** declarando os mÃ³dulos **/
COUNTAPI.namespace('COUNTAPI.XHR');
COUNTAPI.namespace('COUNTAPI.SEARCH');
COUNTAPI.namespace('COUNTAPI.RATE');
COUNTAPI.namespace('COUNTAPI.POLL');

/** implementando mÃ³dulo **/
COUNTAPI.XHR = (function () {
	'use strict';
	//propriedades privadas
	//mÃ©todos privados
	var getXHR = function () {
			var i,
				xhr,
				activexIds = [
					'MSXML2.XMLHTTP.3.0',
					'MSXML2.XMLHTTP',
					'Microsoft.XMLHTTP'
				];
			if (typeof XMLHttpRequest === "function") { // native XHR
				xhr =  new XMLHttpRequest();
			} else { // IE before 7
				for (i = 0; i < activexIds.length; i += 1) {
					try {
						xhr = new ActiveXObject(activexIds[i]);
						break;
					} catch (e) {
						console.log(e);
					}
				}
			}
			return xhr;
		};

	//fim da declaracao var

	//revelando API pÃºblica
	return {
		getXHR: getXHR
	};
}());

COUNTAPI.SEARCH = (function () {
	'use strict';
	var executeSearch = function (address) {

		var xhr;

		xhr = COUNTAPI.XHR.getXHR();

		xhr.onreadystatechange = function () {
			if (xhr.readyState !== 4) {
				return false;
			}
			if (xhr.status !== 200) {
				console.log("Error, status code: " + xhr.status);
				return false;
			}
			eval(xhr.responseText);

		};
		xhr.open("GET", address, true);
		xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8')
		xhr.send("");

	};

	return {
		executeSearch: executeSearch
	};

}());

COUNTAPI.RATE = (function () {
	'use strict';
	var vote = function (value) {

		//falta setar o cookie

		var vurl = COUNTURL;
		vurl += erate[2];
		vurl += "?cmd=";
		vurl += erate[2];
		vurl += "&app=";
		vurl += erate[0];
		vurl += "&value=";
		vurl += value;
		vurl += "&key=";
		vurl += window.location;
		vurl += "&callback=";
		vurl += erate[1];

		COUNTAPI.SEARCH.executeSearch(vurl);
	};

	return {
		vote: vote
	};

}());

COUNTAPI.POLL = (function () {
	'use strict';
	var vote = function (value) {
		var rcf = document.getElementById("recaptcha_challenge_field").value;
    	var rrf = document.getElementById("recaptcha_response_field").value;
		console.log("rcf: " + rcf);
        console.log("rrf: " + rrf);
        //falta setar o cookie
        console.log(epoll.toString());
		var vurl = COUNTURL;
		vurl += "poll?cmd=poll&app=";
		vurl += epoll[1];
		vurl += "&key=";
		vurl += value;
		vurl += "&callback=";
		vurl += epoll[0];
		vurl += "&rcf=";
		vurl += rcf;
		vurl += "&rrf=";
		vurl += rrf;

		COUNTAPI.SEARCH.executeSearch(vurl);
	};

	var get = function () {
		console.log("cheguei");
		//falta setar o cookie
		console.log(gpoll.toString());
		var vurl = COUNTURL;
		vurl += "poll?cmd=getp&app=";
		vurl += gpoll[1];
		vurl += "&callback=";
		vurl += gpoll[0];

		COUNTAPI.SEARCH.executeSearch(vurl);
	};

	return {
		vote: vote,
		get: get
	};

}());


if (ecount[2].toString() === "view") {

	var ecsurl = COUNTURL;
	ecsurl += ecount[2];
	ecsurl += "?cmd=";
	ecsurl += ecount[2];
	ecsurl += "&app=";
	ecsurl += ecount[0];
	ecsurl += "&key=";
	ecsurl += window.location;
	ecsurl += "&callback=";
	ecsurl += ecount[1];

	COUNTAPI.SEARCH.executeSearch(ecsurl);
}

//falta verificar se tem cookie
