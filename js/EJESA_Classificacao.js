/*global document: false */
/*global window: false */
/*global DOMParser: false */
/*global ActiveXObject: false */
/*global console*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.BRASILEIRAO2013.Classificacao'); /** Tabela de Classificação do campeonato **/


/** implementando módulo **/
/** Tabela de Classificação do campeonato **/
EJESA.BRASILEIRAO2013.Classificacao = (function () {
	'use strict';
	var start,
		xml = 'https://www.ejesa.com.br/dataodia/listatabelajogo.xml',
		renderiza,
		teams = [],
		mountTeam,
		mountHTML;


	start = function () {
		//pegar o xml
		var i, xhr;

		xhr = EJESA.BRASILEIRAO2013.Util.getXHR();

	    xhr.onreadystatechange = function () {
	        if (xhr.readyState !== 4) {
	            return false;
	        }
	        if (xhr.status !== 200) {
	            console.log("Error, status code: " + xhr.status);
	            return false;
	        }
	        renderiza(xhr.responseText);
	    };

	    xhr.open("GET", xml, true);
	    xhr.send("");
	};

	renderiza = function (data) {

		var html = '',
			parser,
			xmlDoc,
			itens,
			item,
			i,
			campeonato;

		if (window.DOMParser) {
			parser = new DOMParser();
			xmlDoc = parser.parseFromString(data, "text/xml");
		} else {
			xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
			xmlDoc.async = false;
			xmlDoc.loadXML(data);
		}

		itens = xmlDoc.getElementsByTagName('item');
		for (i = 0; i < itens.length; i = i + 1) {
			item = itens[i];
			mountTeam(item);
		}
		html = mountHTML();
	};

	mountHTML = function () {

		var i,
			html = '',
			time,
			table,
			row,
			cellTime,
			cellOrdem,
			cellPG,
			cellJ,
			cellV,
			cellE,
			cellD,
			cellGP,
			cellGC,
			cellSG,
			cellPercent,
			tlength;


		table = document.getElementById("tabJogos");

		for (i = 0; i < teams.length; i += 1) {
			time = teams[i];

			tlength = table.rows.length;

			row = table.insertRow(tlength);
			cellOrdem = row.insertCell(0);
			cellTime = row.insertCell(1);
			cellPG = row.insertCell(2);
			cellJ = row.insertCell(3);
			cellV = row.insertCell(4);
			cellE = row.insertCell(5);
			cellD = row.insertCell(6);
			cellGP = row.insertCell(7);
			cellGC = row.insertCell(8);
			cellSG = row.insertCell(9);
			cellPercent = row.insertCell(10);

			cellTime.setAttribute("class", 'times');
			cellPG.setAttribute("class", 'cinza');
			cellV.setAttribute("class", 'cinza');
			cellD.setAttribute("class", 'cinza');
			cellGC.setAttribute("class", 'cinza');
			cellPercent.setAttribute("class", 'cinza');

			if (i < 4) {
				row.setAttribute("class", 'primeiros');
			}

			if (i > 15) {
				row.setAttribute("class", 'ultimos');
			}

			cellOrdem.innerHTML = '<strong>' + time.ordem + '</strong>';
			cellTime.innerHTML = '<strong>' + time.time + '</strong>';
			cellPG.innerHTML = '<strong>' + time.pg + '</strong>';
			cellJ.innerHTML = time.j;
			cellV.innerHTML = time.v;
			cellE.innerHTML = time.e;
			cellD.innerHTML = time.d;
			cellGP.innerHTML = time.gp;
			cellGC.innerHTML = time.gc;
			cellSG.innerHTML = time.sg;
			cellPercent.innerHTML = '';
		}

		return html;

	};

	mountTeam = function (team) {
		var obj;

		obj = {
			'ordem' : team.getElementsByTagName('ordem')[0].childNodes[0].nodeValue,
			'id' : team.getElementsByTagName('id')[0].childNodes[0].nodeValue,
			'time' : team.getElementsByTagName('time')[0].childNodes[0].nodeValue,
			'sigla' : team.getElementsByTagName('sigla')[0].childNodes[0].nodeValue,
			'pg' : team.getElementsByTagName('pg')[0].childNodes[0].nodeValue,
			'j' : team.getElementsByTagName('j')[0].childNodes[0].nodeValue,
			'v' : team.getElementsByTagName('v')[0].childNodes[0].nodeValue,
			'e' : team.getElementsByTagName('e')[0].childNodes[0].nodeValue,
			'd' : team.getElementsByTagName('d')[0].childNodes[0].nodeValue,
			'gp' : team.getElementsByTagName('gp')[0].childNodes[0].nodeValue,
			'gc' : team.getElementsByTagName('gc')[0].childNodes[0].nodeValue,
			'sg' : team.getElementsByTagName('sg')[0].childNodes[0].nodeValue
		};

		teams.push(obj);

	};


	//revelando API pública
	return {
		start: start
	};

}());