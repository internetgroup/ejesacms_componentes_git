/*global document: false */
/*global console*/

var EJESA = EJESA || {};

/** declarando os módulos **/
EJESA.namespace('EJESA.BELIA00306'); /** Lista de noticias relacionadas **/


/** implementando módulo **/
/** Lista de noticias relacionadas **/
EJESA.BELIA00306 = (function () {

	'use strict';
	//propriedades privadas
	var QTDNEWS = 9,
		SITE = 'brasileconomico',
		SOLR ,
		FORMAT = 'json',
		start,
		videoChange,
		renderiza,
		makeAddress,
		exibeMais,
		address,
		inicio = 0,
		id = EJESA.Util.getMetaName('be-section-id'),
		idparent = EJESA.Util.getMetaName('be-section-parent'),

		executeSearch;
			//fim da declaracao var


       var site_enviroment,
            strUrl = ' ' + window.location + ' ',
            edition_prod = 'cms.ejesa.igcorp.com.br/edicao.html#start=0',
            prod_preview = 'cms.ejesa.igcorp.com.br/edicaoHomes/preview',
            edition_qa = 'cms-ejesa-qa.igcorp.com.br/edicao.html#start=0',
            qa_preview = 'cms-ejesa-qa.igcorp.com.br/edicaoHomes/preview',
            prod_site = 'brasileconomico.ig.com.br';

            site_enviroment = '';

            SOLR = 'http://brasileconomico.ig.com.br/_indice/noticias/select?';

            if (strUrl.match(edition_qa) ||strUrl.match(qa_preview) ){
                SOLR = 'http://cms-ejesa-qa.igcorp.com.br/solr/editorialStaging/select?sort=updatedDate%20desc&q=%2Bsite:brasileconomico%20%2B';
           }
	
            else if (strUrl.match(edition_prod) ||strUrl.match(prod_preview)  || strUrl.match(prod_site)){
                SOLR = 'http://tvig.ig.com.br/_search/?sort=maisrecente&xmlp=maisrecente&callback=maisrecente&name_categories=Brasil%20Econ%C3%B4mico';
            }





	makeAddress = function (inicio) {
		var address = SOLR;


		address += '&start=';
		address += inicio;
		address += '&size=';
		address += QTDNEWS;
//		address += '&comb_termos="video"%20"videos"'
//		address += '&site=';
//		address += SITE;
//		address += '&wt=';
//		address += FORMAT;


		console.log(address);
		return address;
	};


	executeSearch = function (query) {

		var i, xhr;

		xhr = EJESA.Util.getXHR();

	    xhr.onreadystatechange = function () {
	        if (xhr.readyState !== 4) {
	            return false;
	        }
	        if (xhr.status !== 200) {
	            console.log("Error, status code: " + xhr.status);
	            return false;
	        }
	        renderiza(xhr.responseText);
	    };

	    xhr.open("GET", query, true);
	    xhr.send("");
	};

	renderiza = function (data) {
	
		var obj = $.parseJSON(data),
			docs,
			i,
			limit,
			limittit,
			limiteye,
			date_news,
			hora_news,
			sections,
			mysection,
			tit,
			eye,
			html = '',
			eye,
			tamanho;

			console.log(obj)

			docs = obj.response.docs;
			
			limit = 45;
			limittit = 93;
			limiteye = 80;

        if(docs.length > 9){
        	tamanho = QTDNEWS;
        }
        else{
        	tamanho = docs.length;
        }

        for (i = 0; i < tamanho; i += 1) {


			if (docs[i].url_thumbnails == undefined) {

				tit = '';
				tit = docs[i].title;
			

				html += "<li class='col-md-12 artigo-sem-imagem'>";
				html += "	<a  data-toggle='modal' onClick='javascript:videoChange("+docs[i].file_urls[0]+")' title=''>";
				html += "		<h5>"+tit+"</h5>";
				html += "	</a>";
				html += "</li>";
			

				} 
			else {

				if(i== 0){
					tit = '';
					tit = docs[i].title;
					
					eye = '';
					eye = docs[i].short_description;
				

					html += "<div class='BETOE00306 col-md-12' id='33BDD558-0324-4AE9-B867-6322A825AD96'>        "              
					html += "	<div class='video-destaque'>"                        
					html += "		<div class='row'>"                        
					html += "			<div class='col-md-8 col-md-offset-2'>"                            
					html += "				<div class='video-container'>"                              
					if (docs[i].file_urls[0] !== undefined) {    
						html += "						<video id='video-play' autoplay='false' height='95%' width='100%' controls>"
						html += "							<source  src='"+docs[i].file_urls[0]+"'>"

					} 
					html += "							</video>"
					html += "				</div>"
					html += "			</div>"
					html += "            <div class='clearfix'></div>"
					html += "		</div>"
					html += "	</div>"
					html += "    <div class='row'>"   
					html += "    	<div class='col-md-12'>"   
					html += "			<a target='_blank' onClick='javascript:videoChange("+docs[i].file_urls[0]+")'>"          
					html += "    			<div class='legenda-video'>"                          
					html += "    				<div class='col-md-8 col-md-offset-2'>"                            
					html += "    					 <h2 id='eye-play'>"+eye+"</h2>"
					html += "            	    	<h3 id='title-play'>"+tit+"</h3>"
					html += "            	    	<div class='social-video'></div>"
					html += "            	    	<div class='BENEA00304'>"
					html += "            	    		<div class='toolsbar'>"
					html += "            	    			<div class='tools-twiter' style='float:left'>"
					html += "            	    				<a href='https://twitter.com/share' class='twitter-share-button' data-lang='en' data-url='' data-via='brasileconomico' data-lang='pt' data-related='brasileconomico'>Tweet</a>"
					html += "            	    			</div>"
					html += "            	    			<div class='tools-face' style='float:left'>"
					html += "            	    				<div class='fb-like' data-href='' data-send='false' data-layout='button_count' data-width='145' data-show-faces='true' data-font='lucida grande' data-action='recommend'></div>"
					html += "            	    			</div>"
					html += "            	    			<div class='tools-gplus' style='float:left'>"
					html += "            	    				<!--<g:plus action='share' annotation='bubble'></g:plus>-->"
					html += "            	    				<g:plusone size='medium' href=''></g:plusone>"
					html += "            	    			</div>"
					html += "            	    				<div class='middlebar'></div>"
				/*	html += "            	    			<div class='tools-print' style='float:right'>"
					html += "            	    				<a onClick='javascript:openPrint()''><img src='http://s0.ejesa.ig.com.br/img/be/impressora.png'></a>"
					html += "            	    			</div>"
					html += "            	    			<div class='tools-comments' style='float:right;margin-right:10px'>"
					html += "            	    				<a><img src='http://s0.ejesa.ig.com.br/img/be/chat.png'></a>"
					html += "            	    			</div>"
				*/
					html += "            	    		</div>"
					html += "            	    	</div>"
					html += "					</div>"
					html += "          	 		<div class='clearfix'></div>"
					html += "				</div>"
					html += "			</a>"
					html += "		</div>"
					html += "	</div>"
					html += "</div>"
				}

				else{
					if(i==1){
						html += "<div class='row titulo-separador'>"
						html += "	<div>" 
						html += "		<div >" 
						html += "			<h2> Veja também »</h2>"
						html += "		</div>"
						html += "	</div>"
						html += "</div>"		
					}

					tit = '';
					tit = docs[i].title;
				

					eye = '';
					eye = docs[i].short_description;



					html += "<li class='col-md-12 artigo-sem-imagem' style='float:left;height:300px'>";
					html += "	<a  data-toggle='modal' onClick=\"EJESA.BELIA00306.videoChange('" + docs[i].file_urls[0] + "', '" + eye + "', '" + tit + "');\" title=''>";
					if (docs[i].url_thumbnails !== undefined) {
				    	html += "<img width='150' height='124' src='" + docs[i].url_thumbnails[0] + "'>";
				    } 
					html += "		<h5>"+tit+"</h5>";
					html += "	</a>";
					html += "</li>";

				}

			

			}

         }


		document.getElementById('BELIA00306HL').insertAdjacentHTML("beforeend", html) ;

	};

	start = function () {

		var search;


		address = makeAddress(inicio);

		executeSearch(address);

	};

	exibeMais  = function (){

		$(".row.exibenoticia").hide();
		inicio = inicio + QTDNEWS;

		address = makeAddress(inicio);
		executeSearch(address);

	};


	videoChange = function (linkvideo, olho, titulo){
		console.log(linkvideo);
		$("#video-play").attr("src",linkvideo);
		$("#eye-play").html(olho);
		$("#title-play").html(titulo);


		$('html, body').animate({ scrollTop: 1 },'100');

	};

	//revelando API pública
	return {
		start: start,
		exibeMais: exibeMais,
		videoChange : videoChange
	};
}());
